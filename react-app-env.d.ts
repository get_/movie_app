declare module "material-react-table" {
  export interface MaterialReactTableProps<T extends object> {
    columns: any[];
    data: T[];
    getRowId?: (row: T) => string;
    isLoading?: boolean;
  }

  const MaterialReactTable: <T extends object = {}>(
    props: MaterialReactTableProps<T>
  ) => JSX.Element;
  export default MaterialReactTable;
}
