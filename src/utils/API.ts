import {createClient} from '@supabase/supabase-js'
import {ProgramDate} from "../models/Program";

const APP_SUPABASE_URL='https://smrgejlacnpeerpzxezv.supabase.co'
const SUPABASE_ANON_KEY='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InNtcmdlamxhY25wZWVycHp4ZXp2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDcxMjM5MzcsImV4cCI6MjAyMjY5OTkzN30.nacBmdMek4MeNVyLylb4MnmMztA6wIMdej_8PXULLU8'


export const supabaseUrl:string | undefined = APP_SUPABASE_URL
const supabaseAnonKey:string | undefined = SUPABASE_ANON_KEY
export const supabase = createClient(supabaseUrl!, supabaseAnonKey!)

export function getTimeWithAMPM(timeString:string ): string {
    const [hours, minutes, seconds] = timeString.split(':').map(Number);
    const period = hours >= 12 ? 'PM' : 'AM';
    const displayHours = hours % 12 || 12;
    return `${displayHours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')} ${period}`;
}

function formatDate(date:Date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
}

export function getNext7DaysWithDayNames():ProgramDate[]{
    const days:ProgramDate[] = [];
    const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const today = new Date();
    for (let i = 0; i < 7; i++) {
        const nextDay = new Date(today);
        nextDay.setDate(today.getDate() + i);
        const dayName = dayNames[nextDay.getDay()];
        const formattedDate = formatDate(nextDay);
        days.push({ date: formattedDate, dayName });
    }
    return days;
}
