import React from "react";
import {
  Tv,
  Report,
  Person,
  Message,
  AddCircleOutline,
  ListAlt,
  People,
  VerifiedUser,
  NewReleases,
  History,
  LocationCity,
  LocalMall,
  LocalMovies,
  TheaterComedy,
  Public,
  Dashboard,
} from "@mui/icons-material";

const menus = [
  {
    label: "Dashboard",
    path: "/dashboard",
    icon: <Dashboard />,
  },
  {
    label: "Movies",
    path: "/movies",
    icon: <TheaterComedy />,
    subMenu: [
      {
        label: "Add New Movie",
        path: "/add-movie",
        icon: <AddCircleOutline />,
      },
      { label: "Movie List", path: "/movies", icon: <ListAlt /> },
    ],
  },

  {
    label: "TV Schedules",
    icon: <Tv />,
    path: "/tv",
    subMenu: [
      {
        label: "Add New Channel",
        path: "/add-channel",
        icon: <AddCircleOutline />,
      },
      { label: "Channel List", path: "/channels", icon: <ListAlt /> },
    ],
  },
  {
    label: "Reports",
    icon: <Report />,
    path: "/reports",
    subMenu: [
      { label: "Public Reports", path: "/public-report", icon: <Public /> },
      {
        label: "TV Program Reports",
        path: "/tv-program-report",
        icon: <Tv />,
      },
      { label: "Deputy Reports", path: "/deputy-report", icon: <Report /> },
    ],
  },
  {
    label: "HR",
    path: "/hr",
    icon: <People />,
    subMenu: [
      { label: "Deputy Users", path: "/deputy-user", icon: <Person /> },
      { label: "Users", path: "/user", icon: <People /> },
      { label: "Verification Center", path: "/verify", icon: <VerifiedUser /> },
    ],
  },
  {
    label: "Messages",
    icon: <Message />,
    path: "/messages",
    subMenu: [
      { label: "New Message", path: "/new-message", icon: <NewReleases /> },
      { label: "Message History", path: "/message-history", icon: <History /> },
    ],
  },
  {
    label: "Cinema",
    path: "/cinemas",
    icon: <LocalMovies />,
    subMenu: [
      { label: "Cities", path: "/cities", icon: <LocationCity /> },
      { label: "Mall", path: "/malls", icon: <LocalMall /> },
      { label: "Cinemas", path: "/cinemas", icon: <LocalMovies /> },
      { label: "movies schedule", path: "/movies-schedule", icon: <LocalMovies /> },
    ],
  },
];

export default menus;
