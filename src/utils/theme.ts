import { createTheme, responsiveFontSizes } from "@mui/material/styles";

export const theme = responsiveFontSizes(
  createTheme({
    palette: {
      primary: {
        main: "#2196f3", // Example primary color
      },
      secondary: {
        main: "#f50057", // Example secondary color
      },
      // You can define other colors like error, warning, success, etc.
    },
    typography: {
      fontFamily: "Roboto, sans-serif",
      fontSize: 16, // Base font size in pixels
      h1: {
        fontSize: "6rem", // Example size for extra-large screens (xl)
        "@media (max-width:960px)": {
          fontSize: "4rem", // Adjust size for medium screens (md)
        },
        "@media (max-width:600px)": {
          fontSize: "3rem", // Adjust size for small screens (sm)
        },
      },
      // You can define other typography styles here...
    },
    spacing: 8, // Base spacing unit (8px by default)
    breakpoints: {
      values: {
        xs: 0,
        sm: 600,
        md: 960,
        lg: 1280,
        xl: 1920,
      },
    },
  })
);
