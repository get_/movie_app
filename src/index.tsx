import { Suspense } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";
import App from "./App";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { ToastContainer } from "react-toastify";
import { GloablThemeProvider } from "./layouts/global-theme";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { store } from "./store/app.store";
import { Provider } from "react-redux";
import "./App.css";
const root = ReactDOM.createRoot(document.getElementById("root")!);
root.render(
  <HelmetProvider>
    <Provider store={store}>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <BrowserRouter>
          <Suspense>
            <ToastContainer limit={3} theme="colored" />

            <GloablThemeProvider>
              <App />
            </GloablThemeProvider>
          </Suspense>
        </BrowserRouter>
      </LocalizationProvider>
    </Provider>
  </HelmetProvider>
);
