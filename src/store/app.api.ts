import { createApi } from "@reduxjs/toolkit/query/react";
import { axiosBaseQuery } from "../utils/axios-base-query";
export const apiSlice = createApi({
  reducerPath: "api",
  baseQuery: axiosBaseQuery(),
  tagTypes: [
    "movies",
    "Cinema",
    "users",
    "mall",
    "city",
    "channels",
    "reports",
    "messages",
  ],
  endpoints: () => ({}),
});
