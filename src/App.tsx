import React from "react";
import Router from "./routes/routes";
import UserContextProvider from "./context/UserContextProvider";
export default function App() {
  return (
    <>
      <UserContextProvider>
        <Router />
      </UserContextProvider>
    </>
  );
}
