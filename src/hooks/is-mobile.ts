import { useState, useEffect } from "react";

// Define your mobile breakpoint (for example, <= 600px)
const mobileBreakpoint = 600;

// Custom hook to check if the screen width matches mobile breakpoint
const useIsMobile = (): boolean => {
  const [isMobile, setIsMobile] = useState<boolean>(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= mobileBreakpoint);
    };

    // Call handleResize initially and add event listener for resize
    handleResize();
    window.addEventListener("resize", handleResize);

    // Clean up event listener on unmount
    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty dependency array ensures this effect runs only once

  return isMobile;
};

export default useIsMobile;
