import { Suspense, useContext } from "react";
import { Outlet, Navigate, useRoutes } from "react-router-dom";

// Import all component dependencies
import AddNewMovie from "../components/Movie/AddNewMovie";
import AddNewChannel from "../components/TVSchedule/AddNewChannel";
import ChannelList from "../components/TVSchedule/ChannelList";
import NewMessage from "../components/Message/NewMessage";
import MessageHistory from "../components/Message/MessageHistory";
import User from "../components/hr/User";
import DeputUser from "../components/hr/DeputyUser";
import VerificationCenter from "../components/hr/VerificationCenter";
import PublicReport from "../components/Report/PublicReport";
import DeputyReport from "../components/Report/DeputyReport";
import TVProgramReport from "../components/Report/TVProgramReport";
import CinemasList from "../components/cinemas/CinemasList";
import CinemaMovieList from "../components/cinemas/movies";
import AddNewCinema from "../components/cinemas/AddNewCinema";
import CinemaMovie from "../components/cinemas/CinemaMovie";
import AddNewCity from "../components/city/AddNewCity";
import CityList from "../components/city/CityList";
import AddNewMall from "../components/mall/AddNewMall";
import MallList from "../components/mall/MallList";
import NotFound from "../components/notfound/NotFound";
import Login from "../components/Auth/Login";
import Layout2 from "../layouts/layout2/main";
import Home from "../components/Home/Home"; // Assuming this exists for the home route
import UserContext from "../context/UserContext";
import MovieList from "../components/Movie/MoviewList";
import { MovieTest } from "../components/Movie/movie-test";
import ScheduleProgram from "../components/TVSchedule/ScheduleProgram";
import { Dashboard } from "@mui/icons-material";
import { MyDashboard } from "../components/dashboard/my-dashboard";

export default function Router() {
  const user = useContext(UserContext);
  const isAllowed = user?.user !== null || localStorage.getItem("access_token");

  const ProtectedRoute = ({ children }: any) =>
    isAllowed ? children : <Navigate to="/login" replace />;

  return useRoutes([
    {
      path: "/",
      element: (
        <Layout2>
          <Suspense fallback={<div>Loading...</div>}>
            <Outlet />
          </Suspense>
        </Layout2>
      ),

      children: [
        {
          index: true,
          element: (
            <ProtectedRoute>
              <MyDashboard />
            </ProtectedRoute>
          ),
        },
        { path: "dashboard", element: <MyDashboard /> },

        {
          path: "movies",
          element: (
            <ProtectedRoute>
              <Outlet />
            </ProtectedRoute>
          ),
          children: [
            { path: "movies", element: <MovieTest /> },
            { path: "add-movie", element: <AddNewMovie /> },
          ],
        },

        {
          path: "tv",
          element: (
            <ProtectedRoute>
              <Outlet />
            </ProtectedRoute>
          ),
          children: [
            { path: "add-channel", element: <AddNewChannel /> },
            { path: "channels", element: <ChannelList /> },
            { path: "tv-program", element: <ScheduleProgram /> },
          ],
        },
        {
          path: "reports",
          element: (
            <ProtectedRoute>
              <Outlet />
            </ProtectedRoute>
          ),
          children: [
            { path: "public-report", element: <PublicReport /> },
            { path: "deputy-report", element: <DeputyReport /> },
            { path: "tv-program-report", element: <TVProgramReport /> },
          ],
        },
        {
          path: "hr",
          element: (
            <ProtectedRoute>
              <Outlet />
            </ProtectedRoute>
          ),
          children: [
            { path: "deputy-user", element: <DeputUser /> },
            { path: "user", element: <User /> },
            { path: "verify", element: <VerificationCenter /> },
          ],
        },
        {
          path: "messages",
          element: (
            <ProtectedRoute>
              <Outlet />
            </ProtectedRoute>
          ),
          children: [
            { path: "new-message", element: <NewMessage /> },
            { path: "message-history", element: <MessageHistory /> },
          ],
        },
        {
          path: "cinemas",
          element: (
            <ProtectedRoute>
              <Outlet />
            </ProtectedRoute>
          ),
          children: [
            { path: "cities", element: <CityList /> },
            { path: "malls", element: <MallList /> },
            { path: "cinemas", element: <CinemasList /> },
            { path: "add-cinema", element: <AddNewCinema /> },
            { path: "movies-schedule", element: <CinemaMovieList /> },
            { path: "cinema-movie", element: <CinemaMovie /> },
          ],
        },
      ],
    },
    { path: "login", element: <Login /> },

    { path: "*", element: <NotFound /> },
  ]);
}
