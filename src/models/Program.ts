
export interface Program{
    id?: number;
    created_at?: string;
    program_name?: string;
    category_id?: number;
    start_time?: string;
    end_time?: string;
    season_episode?: string;
    tv_schedule_id?: number;
    program_date: Date;
    category_name?: string;
    channel_name?: string;
}
export interface ProgramDate{
    date?: string;
    dayName?: string;
}
