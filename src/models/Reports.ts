export interface PublicReport{
    id: number;
    created_at: Date;
    event_date: Date;
    time:string;
    place_type: string;
    report: string;
    photo_url?: string;
    user_id:string;
    phone_number: string;
    email?:string;
}
export interface TVChannelReport{
    id: number;
    created_at: Date;
    event_date: Date;
    time:string;
    place_type: string;
    report: string;
    photo_url?: string;
    user_id:string;
    phone_number: string;
    program:string;
    channel:string;
    email?:string;
}
export interface DeputyReport{
    id: number;
    created_at: Date;
    event_date: Date;
    time:string;
    place: string;
    report: string;
    photo_url?: string;
    user_id:string;
    phone_number: string;
    location:string;
    email?:string;
    tv_program_name:string;
    name:string;
}
