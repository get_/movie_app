import {Mall} from "./Mall";

export interface City{
    id?: number;
    name: string;
    short_code?:string;
    street?: string;
    state?: string;
    zip_code?:string;
    created_at?: Date;
    malls?:number[];
    cityMalls?:Mall[];
}
