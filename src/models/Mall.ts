import { Cinema } from "./Cinema";
import { City } from "./City";

export interface Mall {
  id?: number;
  name: string;
  created_at?: Date;
  city?: string;
  cinemas?: number[];
  mallCinemas?: Cinema[];
  mallCities?: City[];
}
