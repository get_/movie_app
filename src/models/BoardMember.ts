export  interface BoardMember {
    id: string;
    name: string;
    position: string;
    appointment_date?: Date;
    contact?: string;
    created_at: string;
    description?: string;
    profile_url?: string;
}
