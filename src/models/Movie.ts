import {BoardMember} from "./BoardMember";
import {Dayjs} from "dayjs";

export interface Movie {
    id?: number,
    title?: string,
    genre?: string,
    cover_img?: string,
    avatar_img?: string,
    category?: number,
    category_name?: string,
    applicant?: string,
    release_date?: Dayjs | null | undefined,
    permit_number?: string,
    reason?: string,
    premiere_date?: Date|null,
    created_at?: Date,
    rate?: number,
    committees?: number[],
    committee_members?: BoardMember[] | null,
}
export  interface Genre {
    id?: number,
    name: string,
    created_at: string
}



// <FormikProvider value={formik}>
// <Form autoComplete="off" noValidate onSubmit={handleSubmit}
//
//     >
//     <Grid container spacing={2}>
//
//
//     <Grid item xs={12} sm={3} md={3}>
// <Box className="flex items-center">
// <input
//     accept={'image/*'}
// style={{display: 'none'}}
// id="contained-button-file"
// multiple
// type="file"
// onChange={handleFileInputChange}
// />
// <label htmlFor="contained-button-file">
// <Button
//     sx={{width: 128, height: 140}}
// variant="outlined"
// component="span"
// tabIndex={3}
// startIcon={<CloudUploadIcon/>}
// >
// Upload File
// </Button>
// </label>
// {file && <span>{file.name}</span>}
// </Box>
//
// </Grid>
//
// <Grid item xs={12} sm={9} md={9} direction="column" container spacing={2}
//     style={{marginTop: 1}}>
//     <FormControl>
//         <TextField
//             sx={{width: '100%'}}
//     fullWidth
//     label="Title"
//     name="title"
//     value={formData.title}
//     onChange={handleInputChange}
//     variant="outlined"
//     required
//     type="text"
//     placeholder="Please enter the title"
//
//     />
//     </FormControl>
//     <FormControl>
//     <TextField
//         sx={{width: '100%', marginTop: 2}}
//     id="outlined-select-country"
//     select
//     label="Rating"
//         >
//         {categories.map((category) => (
//                 <MenuItem key={category.id} value={category.name}>
//             {category.name}
//             </MenuItem>
// ))}
//     </TextField>
//     </FormControl>
//     <FormControl>
//     <TextField
//         sx={{width: '100%', marginTop: 2}}
//     id="outlined-select-genre"
//     select
//     label="Genre"
//         >
//         {types.map((option) => (
//                 <MenuItem key={option.id} value={option.name}>
//             {option.name}
//             </MenuItem>
// ))}
//     </TextField>
//     </FormControl>
//
//     </Grid>
//
//     </Grid>
//
//
//     <br/>
//     <Grid container spacing={2}>
//
//     <Grid item xs={2} sm={12} md={12}
//     sx={{width: '100%',}}
// >
//
//     <Box sx={{
//     display: 'flex',
//         flexDirection: 'row',
//         alignItems: 'center',
//         justifyContent: 'space-between',
//         width: '100%',
// }}>
//     <LocalizationProvider
//         dateAdapter={AdapterDayjs}>
//     <DatePicker
//         label="Release Date"
//     value={value}
//     onChange={handleChange}
//     />
//     </LocalizationProvider>
//
//     <LocalizationProvider
//     dateAdapter={AdapterDayjs}>
//     <DatePicker
//         label="Premier Date"
//     value={value}
//     onChange={handleChange}
//     />
//     </LocalizationProvider>
//     </Box>
//     </Grid>
//
//     <Grid item xs={2} sm={12} md={12}>
// <TextField
//     sx={{width: '100%',}}
//     fullWidth
//     label="Applicant Name"
//     name="applicantName"
//     value={formData.title}
//     onChange={handleInputChange}
//     variant="outlined"
//     required
//     type="text"
//     placeholder="Please enter the applicant name"
//         />
//         </Grid>
//
//         <Grid item xs={2} sm={12} md={12}>
// <TextField
//     sx={{width: '100%',}}
//     fullWidth
//     label="Permit Number"
//     name="permitNumber"
//     value={formData.title}
//     onChange={handleInputChange}
//     variant="outlined"
//     required
//     type="text"
//     placeholder="Please enter the permit number"
//         />
//         </Grid>
//
//         <Grid item xs={2} sm={12} md={12}>
// <TextField
//     sx={{width: '100%',}}
//     fullWidth
//     label="Reason for Classification"
//     name="reasonForClassification"
//     value={formData.title}
//     onChange={handleInputChange}
//     variant="outlined"
//     required
//     type="text"
//     placeholder="Please enter the reason for classification"
//
//     />
//     </Grid>
//
//     </Grid>
//     <br/>
//     <Typography
//         sx={{width: '100%', color: 'gray',margin:1, fontSize: 20, fontWeight: 'bold'}}
// >Review Committe Members</Typography>
// <Autocomplete
//     multiple
//     id="user-chip-input"
//     options={users}
//     getOptionLabel={(user) => user.name}
//     value={selectedUsers}
//     onChange={handleUserChange}
//     renderInput={(params) => (
//     <TextField
//         {...params}
//     variant="outlined"
//     label="Select Users"
//     placeholder="Select Users"
//         />
// )}
//     renderTags={(value, getTagProps) =>
//     value.map((user, index) => (
//         // <Chip
//         //     // key={user.id}
//         //     label={user.name}
//         //     {...getTagProps({index})}
//         // />
//         <Chip
//             avatar={<Avatar alt="Natacha" src="/static/images/avatar/1.jpg" />}
//     label="Avatar"
//     variant="outlined"
//         />
// ))
// }
//     />
//
//     <Box sx={{
//     display: 'flex',
//         flexDirection: 'row',
//         alignItems: 'center',
//         width: '100%',
//         marginTop: 2,
//         justifyContent: 'center',
//
// }}>
//     <input
//         accept={'image/*'}
//     style={{display: 'none'}}
//     id="contained-button-file"
//     multiple
//     type="file"
//     onChange={handleFileInputChange}
//     />
//     <label htmlFor="contained-button-file">
// <Button
//     sx={{width: 128, height: 140}}
//     variant="outlined"
//     component="span"
//     tabIndex={3}
//     startIcon={<CloudUploadIcon/>}
// >
//     Upload File
// </Button>
// </label>
//     {file && <span>{file.name}</span>}
//     </Box>
//
//
//     <br/>
//     <Grid container spacing={2}>
//         <Grid item xs={2} sm={2} md={2}/>
//     <Grid item xs={8} sm={8} md={8}>
//     <Button
//         type="submit"
//         variant="contained"
//         color="primary"
//         className="bg-green-500 hover:bg-green-700 text-white px-4 py-2 rounded-md"
//         sx={{width: '100%', borderRadius: '20px'}}
//     >
//         Add New Films
//     </Button>
//     </Grid>
//     <Grid item xs={2} sm={2} md={2}/>
//     </Grid>
//
//     </Form>
//     </FormikProvider>
