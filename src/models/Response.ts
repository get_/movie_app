export interface DeputyReportResponse{
    id?: number;
    report_id: number;
    response: string;
    created_at?: string;
}
export interface PublicReportResponse{
    id?: number;
    report_id: number;
    response: string;
    created_at?: string;
}
export interface TvChannelReportResponse{
    id?: number;
    report_id: number;
    response: string;
    created_at?: string;
}
