import { Movie } from "./Movie";
import { Mall } from "./Mall";
import { City } from "./City";

export interface Cinema {
  id?: number;
  name: string;
  created_at?: Date;
  movies?: Movie[];
  cities?: City[];
  mallId?: number | null; // Assuming bigint is mapped to number in TypeScript
  cinemaImg?: string | null;
  qrValue?: string | null;
  malls?: Mall[];
}
export interface CinemaMovie {
  id?: number;
  cinema_id: number;
  movie_id: number;
  created_at?: Date;
}
