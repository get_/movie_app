
export interface Message {
    id?: number;
    message: string;
    open?: boolean|undefined;
    created_at?:Date;
    user_type?: string;

}
