import { date } from "yup";

export interface User {
  id: number;
  name: string;
  email: string;
  phone_number: string;
  profile_picture?: string;
  government_id?: string;
  gov_issued_ID_url?: string;
  role?: string;
  user_id?: string;
  password?: string;
  created_at?: Date;
  lastActivity?: Date;
}
export interface UserContextType {
  user: string | null;
  setUser: React.Dispatch<React.SetStateAction<string | null>>;
}
export interface Auth {
  access_token: string;
  success: boolean;
  user?: User;
  message?: string;
}
export interface UserActivity {
  id?: number;
  created_at?: Date;
  user_id: number;
  last_activity: Date;
}
export interface DeputyUser {
  id?: string;
  created_at: Date;
  email: string;
  password: string;
  phone_number: string;
  user_id: string; // Assuming UUID is represented as a string
  gov_issued_ID_url?: string | null; // Optional field
  role?: string | null; // Optional field
  profile_picture?: string | null; // Optional field
  name?: string | null; // Optional field
  status: string; // Assuming 'status' is always present and cannot be null
}
