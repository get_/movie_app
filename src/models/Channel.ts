
export interface Channel {
    id?: number,
    avatar_img?: string,
    title: string,
    file?: File | null
    open?: boolean|undefined;
}
export interface DeleteChannel {
    id?: number,
    avator?: string,
    title: string,
    open: boolean
}
