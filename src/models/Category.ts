export interface Category {
    id: number;
    name: string;
    color: string;
    type?: string;
    description?: string;
    created_at: Date;
  }

