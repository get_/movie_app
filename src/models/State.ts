import { SnackbarOrigin } from '@mui/material/Snackbar';


export interface State extends SnackbarOrigin {
    open: boolean|undefined;
}
export interface FilterModel {
    search:string,
    genreFilter:string,
    ratingFilter:string

}
