import { createContext } from "react";
import { User, UserContextType } from "../models/User";
import UserContextProvider from "./UserContextProvider";

const UserContext = createContext<UserContextType | null>(null);
export default UserContext;
