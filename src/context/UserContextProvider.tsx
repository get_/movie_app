import { ReactNode, useEffect, useState } from "react";
import UserContext from "./UserContext";
import { User, UserContextType } from "../models/User";
interface UserContextProviderProps {
  children: ReactNode;
}

const UserContextProvider: React.FC<UserContextProviderProps> = ({
  children,
}) => {
  const [user, setUser] = useState<UserContextType | null>(null);

  // useEffect(() => {
  //     const unsubscribe = auth.onAuthStateChanged((user) => {
  //         setUser(user);
  //         setLoading(false);
  //     });
  //
  //     return unsubscribe;
  // }, []);
  // const value:UserContextType = { user, setUser };
  return (
    <UserContext.Provider value={{ user, setUser } as UserContextType}>
      {children}
    </UserContext.Provider>
  );
};
export default UserContextProvider;
