import { Box, CircularProgress, Paper } from "@mui/material";
import { PropsWithChildren, ReactNode } from "react";
import {
  DefaultPageHeader,
  DefaultPageHeaderProps,
} from "./default-page-header";

type DefaultPageProps = {
  narrow?: boolean;
  otherComponet?: ReactNode;
  loading?: boolean;
} & DefaultPageHeaderProps;

export const DefaultPage = (
  props: PropsWithChildren<DefaultPageProps>
): JSX.Element => {
  return (
    <Box
      {...(props.narrow && {
        alignSelf: "center",
        width: "100%",
        maxWidth: "100%",
        marginBottom: 2,
      })}
    >
      <DefaultPageHeader {...props} />
      {props.otherComponet}
      <Paper
        component="div"
        sx={{
          width: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexGrow: 1,
          minHeight: 200,
          padding: props.narrow ? "25px" : "16px",
        }}
        {...(props.narrow && {
          style: {
            padding: 25,
          },
        })}
      >
        {props?.loading ? <CircularProgress size={40} /> : props.children}
      </Paper>
    </Box>
  );
};
