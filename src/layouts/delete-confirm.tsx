import React from "react";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
  IconButton,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";

interface DeleteConfirmationDialogProps {
  open: boolean;
  onClose: () => void;
  loading?: boolean;
  confirmMessage?: string;
  onConfirm: () => void;
  item: string; // Assuming 'item' is a string to be displayed
  itemName: string; // Name of the item to be deleted, also a string
}

// Reusable delete confirmation dialog with TypeScript
const DeleteConfirmationDialog: React.FC<DeleteConfirmationDialogProps> = ({
  open,
  onClose,
  onConfirm,
  item,
  confirmMessage,
  loading = false,
  itemName,
}) => {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      sx={{
        "& .MuiDialog-paper": {
          width: "30%", // Increased width
          maxWidth: "650px", // Optionally, ensure it doesn't get too large on bigger screens
          borderRadius: "15px",
          backgroundColor: "#F6F7FD",
        },
      }}
    >
      <DialogTitle id="alert-dialog-title">
        <Typography variant="body1" component="div" align="center">
          {`${confirmMessage ?? "Do You Want To Delete "} ${itemName}`}
        </Typography>
      </DialogTitle>
      <IconButton
        aria-label="close"
        onClick={onClose}
        sx={{
          position: "absolute",
          top: 8,
          right: 8,
          color: (theme) => theme.palette.error.light,
        }}
      >
        <img alt="Close icon" src="/x.png" />
      </IconButton>
      <DialogContent>
        <Typography sx={{ color: "black" }}>{item}</Typography>
      </DialogContent>
      <DialogActions>
        <LoadingButton
          onClick={onConfirm}
          color="warning" // Maintain the "error" color from the theme
          fullWidth
          loading={loading}
          sx={{
            textTransform: "none",
            borderRadius: "55px",
            margin: 2,
            backgroundColor: (theme) => theme.palette.grey[300], // Changed to grey theme
            "&:hover": { backgroundColor: (theme) => theme.palette.grey[400] }, // Lighter grey on hover
          }}
        >
          Confirm
        </LoadingButton>
        <Button
          onClick={onClose}
          color="secondary"
          fullWidth
          sx={{ textTransform: "none", borderRadius: "55px", margin: 2 }}
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteConfirmationDialog;
