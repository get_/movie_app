/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { ArrowBack } from "@mui/icons-material";
import { LoadingButton, LoadingButtonProps } from "@mui/lab";
import {
  Box,
  Grid,
  Typography,
  Button,
  Select,
  SelectProps,
} from "@mui/material";
import { PropsWithChildren } from "react";
import { useNavigate } from "react-router-dom";

export type DefaultPageHeaderProps = {
  title: string;
  subTitle?: React.ReactNode | string;
  primaryButtonProps?: DefaultPageHeaderButtonProps;
  outlinedButtonProps?: DefaultPageHeaderButtonProps;
  secondaryButtonProps?: DefaultPageHeaderButtonProps;
  secondarySelectProps?: DefaultPageHeaderSelectProps;
  backButtonLink?: string | any;
  backButtonTitle?: string;
  dropdownSelector?: React.ReactNode;
  showWarningColor?: boolean;
  showTotalEmployees?: boolean;
  total?: number;
  rightSideComponent?: React.ReactElement;
};

type DefaultPageHeaderButtonProps = PropsWithChildren<
  Omit<LoadingButtonProps, "size" | "variant">
> & {
  requiredPermissions?: string[];
};
type DefaultPageHeaderSelectProps = PropsWithChildren<SelectProps>;

export const DefaultPageHeader = (
  props: DefaultPageHeaderProps
): JSX.Element => {
  const navigate = useNavigate();

  return (
    <Box mx={1} mb={2}>
      <Grid
        container
        direction="row"
        justifyContent={"space-between"}
        flexDirection="row"
        alignItems="center"
        spacing={2}
        wrap="nowrap"
        justifyItems={"center"}
        justifySelf={"center"}
      >
        <Grid item>
          <Typography variant="h4" sx={{ fontWeight: 500, fontSize: "35px" }}>
            {props.title}
          </Typography>
        </Grid>
        <Grid
          item
          container
          direction="row"
          xs={"auto"}
          spacing={1}
        >
          {props.primaryButtonProps && (
            <Grid item>
              <LoadingButton
                style={{ textTransform: "none" }}
                size="large"
                variant="contained"
                {...props.primaryButtonProps}
              >
                {props.primaryButtonProps.children}
              </LoadingButton>
            </Grid>
          )}

          {props.rightSideComponent && (
            <Grid item>{props.rightSideComponent}</Grid>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};
