import React, { useState } from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import { useMediaQuery, useTheme } from "@mui/material";

import Main from "./main";
import Header from "../../components/Header/Header";
import LeftSideNav from "../../components/SideNav/LeftSideNav";
import Footer from "../../components/Footer/Footer";

interface DashboardLayoutProps {
  children: React.ReactNode;
}

export default function DashboardLayout({ children }: DashboardLayoutProps) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));
  const [sidebarOpen] = useState(!isMobile);
  return (
    <Box sx={{ display: "flex", flexDirection: "column", width: "100%" }}>
      <Header />
      <Box sx={{ display: "flex", flexGrow: 1, width: "100%" }}>
        {!isMobile && <LeftSideNav />}
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            transition: theme.transitions.create("margin", {
              easing: theme.transitions.easing.sharp,
              duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: `${sidebarOpen ? 40 : 0}px`, // Adjust this value based on your sidebar's width
            width: { sm: `calc(100% - ${sidebarOpen ? 40 : 0}px)` },
            overflowX: "hidden", // Prevent horizontal overflow
          }}
        >
          {children}
        </Box>
      </Box>
      {/*       <Footer />
       */}{" "}
    </Box>
  );
}

DashboardLayout.propTypes = {
  children: PropTypes.node,
};
