import { ReactNode } from 'react';
import PropTypes from 'prop-types';

import { BoxProps } from '@mui/material/Box';


interface MainProps extends BoxProps {
  children: ReactNode;
}

export default function Main({ children }: MainProps) {

  return (
    <div>
      {children}
    </div>
  );
}

Main.propTypes = {
  children: PropTypes.node,
  sx: PropTypes.object,
};
