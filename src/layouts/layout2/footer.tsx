// src/components/Footer.js
import { Box, Typography, useTheme, useMediaQuery } from "@mui/material";

function Footer() {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <Box
      component="footer"
      sx={{
        p: isMobile ? 1 : 3, // smaller padding on mobile
        textAlign: "center",
        mt: "auto",
        backgroundColor: "#f3f3f3",
      }}
    >
      <Typography variant={isMobile ? "caption" : "subtitle1"}>
        © 2024 MTRCB All Rights Reserved
      </Typography>
    </Box>
  );
}

export default Footer;
