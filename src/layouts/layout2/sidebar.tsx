import React, { useState } from "react";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  Box,
  Typography,
} from "@mui/material";
import { Link, useLocation } from "react-router-dom";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import menus from "../../utils/menu";

function SidebarItem({ menu }: any) {
  const location = useLocation();
  const [open, setOpen] = useState(location.pathname.includes(menu.path));

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <>
      <ListItem
        component={Link}
        to={menu.submenu ? undefined : menu.path}
        onClick={handleClick}
        className={
          (location.pathname === `${menu.path}` && `${menu.path}` !== "/") ||
          location.pathname === `${menu.path}`
            ? "active-sidebar-item"
            : ""
        }
        sx={{
          py: 1,
          pl: 2,
          cursor: "pointer",
          color: "inherit",
          textDecoration: "inherit",
        }}
      >
        <ListItemIcon>{menu.icon}</ListItemIcon>
        <ListItemText primary={menu.label} />

        {menu.subMenu ? open ? <ExpandLess /> : <ExpandMore /> : null}
      </ListItem>
      {menu.subMenu && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {menu.subMenu.map((subItem: any) => (
              <ListItem
                button
                key={subItem.path}
                component={Link}
                to={`${menu.path}${subItem.path}`}
                className={
                  (location.pathname === `${menu.path}${subItem.path}` &&
                    `${menu.path}${subItem.path}` !== "/") ||
                  location.pathname === `${menu.path}${subItem.path}`
                    ? "active-sidebar-item"
                    : ""
                }
                sx={{
                  py: 1,
                  pl: 4,
                  cursor: "pointer",
                  color: "inherit",
                  textDecoration: "inherit",
                }}
              >
                <ListItemIcon>{subItem.icon}</ListItemIcon>
                <ListItemText primary={subItem.label} />
              </ListItem>
            ))}
          </List>
        </Collapse>
      )}
    </>
  );
}

function Sidebar({ open, onClose }: any) {
  return (
    <Box sx={{ width: 250 }} role="presentation">
      <Typography variant="h6" sx={{ my: 2, textAlign: "center" }}></Typography>
      <List>
        {menus.map((menu, index) => (
          <SidebarItem key={index} menu={menu} />
        ))}
      </List>
    </Box>
  );
}

export default Sidebar;
