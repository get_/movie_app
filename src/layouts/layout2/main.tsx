// src/App.js
import React, { useState } from "react";
import {
  Box,
  CssBaseline,
  Drawer,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import Footer from "./footer";
import Header from "./header";
import Sidebar from "./sidebar";

function Layout2({ children }: any) {
  const [mobileOpen, setMobileOpen] = useState(false);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  const handleMenuClick = (menu: any) => {
    if (isMobile) {
      setMobileOpen(false); // Close the drawer when an item is clicked on mobile
    }
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = <Sidebar onMenuClick={handleMenuClick} />;
  const drawerStyles: React.CSSProperties = {
    width: 270,
    flexShrink: 0,
    borderRight: "none",
    marginTop: `${75}px`,
    borderTopRightRadius: "12px",
    height: "90%",
  };
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
      }}
    >
      <CssBaseline />
      <Header handleDrawerToggle={handleDrawerToggle} />
      <Box
        component="nav"
        sx={{ width: { sm: 240 }, marginX: 30, flexShrink: { sm: 0 } }}
      >
        <Drawer
          variant={isMobile ? "temporary" : "permanent"}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile
          }}
          sx={{
            "& .MuiDrawer-paper": {
              ...drawerStyles,
              boxSizing: "border-box",
              width: 270,
            },
          }}
        >
          {drawer}
        </Drawer>
      </Box>

      <Box
        component="main"
        sx={{
          flexGrow: 1,
          ml: 35,
          p: 2,
          display: "flex",
          flexDirection: "column",
          maxWidth: "100%",
        }}
      >
        {children}
      </Box>
      <Footer />
    </Box>
  );
}

export default Layout2;
