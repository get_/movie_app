// src/components/Header.js
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Avatar,
  useTheme,
  useMediaQuery,
  Box,
  Button,
  Menu,
  MenuItem,
  MenuList,
  ListItemIcon,
  ListItemText,
  CircularProgress,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useGetUserByEmailQuery } from "../../services/auth.query";
import { User } from "@supabase/supabase-js";
import LogoutIcon from "@mui/icons-material/Logout";
import { signOut } from "../../services/AuthService";
import { useNavigate } from "react-router";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { useState } from "react";
function Header({ handleDrawerToggle }: any) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const lUser: User = JSON.parse(localStorage.getItem("user") as string);
  const email = lUser?.email;
  const { data: userInfo, isLoading } = useGetUserByEmailQuery(email);
  const navigator = useNavigate();
  const [loading, setLoading] = useState(false);

  const handleLogout = async () => {
    setLoading(true);
    try {
      // Assuming signOut is an async function
      await signOut();
      navigator("/login");
    } catch (error) {
      console.error("Logout failed:", error);
    }
    setLoading(false);
  };

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleProfileMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <AppBar
        elevation={0}
        position="fixed"
        sx={{
          backgroundColor: "#2891FF",
          boxShadow: 1,
          zIndex: (theme) => theme.zIndex.drawer + 4,
          height: 65,
        }}
      >
        <Toolbar sx={{ marginX: isMobile ? 0 : 30 }}>
          {isMobile && (
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
          )}

          <Box sx={{ flexGrow: 1, display: "flex", alignItems: "center" }}>
            <img src="/logo.png" alt="Logo" style={{ height: "40px" }} />
          </Box>

          <Box display="flex" alignItems="center">
            <Button
              id="basic-button"
              aria-controls={open ? "basic-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              onClick={handleProfileMenu}
            >
              <Avatar
                src={userInfo?.profile_picture}
                alt={userInfo?.name}
                sx={{ width: 36, height: 36 }}
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "start",
                  marginLeft: "15px",
                }}
              >
                <Typography
                  variant="body1"
                  sx={{ textTransform: "capitalize", color: "white" }}
                >
                  {userInfo?.name}
                </Typography>
                <Box
                  sx={{
                    textTransform: "capitalize",
                    display: "flex",
                    color: "#ccc",
                    justifyContent: "center",
                  }}
                >
                  {userInfo?.role.toLowerCase()}
                  <ExpandMoreIcon color="inherit" sx={{ color: "white" }} />
                </Box>
              </Box>
            </Button>
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
            >
              <MenuList sx={{ minWidth: 100 }}>
                <MenuItem>
                  <ListItemText
                    primary={userInfo?.name}
                    secondary={userInfo?.role}
                  />
                </MenuItem>
                <MenuItem disabled={loading} onClick={handleLogout}>
                  <ListItemIcon>
                    {loading ? <CircularProgress size={24} /> : <LogoutIcon />}
                  </ListItemIcon>
                  Logout
                </MenuItem>
              </MenuList>
            </Menu>
          </Box>
        </Toolbar>
      </AppBar>
      <Toolbar />{" "}
    </>
  );
}
export default Header;
