import { Box, Grid, PaperProps, Stack, Theme, Typography } from "@mui/material";
import {
  ExpandedState,
  OnChangeFn,
  PaginationOptions,
} from "@tanstack/table-core";
import {
  MaterialReactTable,
  MRT_Row,
  MRT_RowSelectionState,
  MaterialReactTableProps,
} from "material-react-table";
import { ReactNode } from "react";

type RowClickCallback<T extends Record<string, any> = {}> = (row: T) => void;
type RenderRowActionsCallback<T extends Record<string, any> = {}> = (
  row: T,
  index: number
) => ReactNode;

type RenderBottomToolbarCustomActionsCallback<
  T extends Record<string, any> = {}
> = () => ReactNode;

type RenderDetailPanelCallback<T extends Record<string, any> = {}> = ({
  row,
}: {
  row: MRT_Row<T>;
}) => ReactNode;

type DataTableRecordsProps<T extends Record<string, any>> = {
  list: MaterialReactTableProps<T>["data"] | undefined;
  count: MaterialReactTableProps<T>["rowCount"];
} & Required<Pick<MaterialReactTableProps<T>, "columns" | "getRowId">>;

type DataTableOptionProps<T extends Record<string, any>> = Pick<
  MaterialReactTableProps<T>,
  | "enableBottomToolbar"
  | "enableRowSelection"
  | "onRowSelectionChange"
  | "renderRowActionMenuItems"
  | "renderDetailPanel"
  | "enableRowNumbers"
  | "rowNumberDisplayMode"
  | "positionPagination"
> & { title?: string };

interface DataTableStateProps {
  isLoading: boolean;
  isSuccess: boolean;
  isFetching?: boolean;
  isError: boolean;
  rowSelection?: MRT_RowSelectionState;
  shouldResetPage?: boolean;
  emptyMessage?: string;
}

interface DataTableFilterProps {
  globalFilter?: any;
}

interface DataTablePaginationProps {
  rowsPerPageOptions?: number[];
}

interface DataTableActionProps<T extends Record<string, any>> {
  onRowClick?: RowClickCallback<T>;
  toolbarActions?: ReactNode;
  toolbar?: ReactNode;
  renderRowActions?: RenderRowActionsCallback<T>;
}

interface DataTableEditingProps<T extends Record<string, any>> {
  enableTableEditing?: boolean;
  editingMode?: "modal" | "cell" | "row" | "table";
  muiEditTextFieldProps?: MaterialReactTableProps<T>["muiEditTextFieldProps"];
  enableMultiRowSelection?: boolean;
}

interface DataTableRenderDetailPanel<T extends Record<string, any>> {
  renderDetailPanel?: RenderDetailPanelCallback<T>;
}

interface DataTableRenderBottomToolbarCustomActions<
  T extends Record<string, any>
> {
  renderBottomToolbarCustomActions?: RenderBottomToolbarCustomActionsCallback<T>;
}

interface DataTableSubRows<T extends Record<string, any>> {
  enableExpanding?: boolean;
  manualExpanding?: boolean;
  toolbarActionMargin?: string;
  onExpandedChange?: OnChangeFn<ExpandedState>;
  enableStickyHeader?: boolean;
  enablePinning?: boolean;
  enableExpandAll?: boolean;
  pinAction?: boolean;
  hideRowActions?: boolean;
}

interface DataTableStylingProps<T extends Record<string, any>> {
  getRowBackgroundColor?: (
    original: MRT_Row<T>,
    theme: Theme
  ) => string | undefined;
}

type DataTableProps<T extends Record<string, any>> = DataTableRecordsProps<T> &
  DataTableOptionProps<T> &
  DataTableStateProps &
  DataTablePaginationProps &
  DataTableActionProps<T> &
  Partial<Pick<any, "pagination">> &
  Pick<PaginationOptions, "onPaginationChange"> &
  Pick<PaperProps, "variant"> &
  DataTableEditingProps<T> &
  DataTableFilterProps &
  DataTableRenderDetailPanel<T> &
  DataTableRenderBottomToolbarCustomActions<T> &
  DataTableSubRows<T> &
  DataTableStylingProps<T>;

export const DataTable = <T extends Record<string, any> = {}>(
  props: DataTableProps<T>
): JSX.Element => {
  const manualPagination = Boolean(props.pagination);

  return (
    <Box
      sx={{
        maxWidth: "100vw",
        Width: "100vw",
        overflowX: "auto",
        flexGrow: 1,
      }}
    >
      <MaterialReactTable
        columns={props.columns}
        data={props?.list ?? []}
        rowCount={props?.count ?? 0}
        enableGlobalFilter={true}
        enableGlobalFilterModes
        onGlobalFilterChange={props.globalFilter}
        getRowId={props.getRowId}
        positionGlobalFilter="right"
        muiSearchTextFieldProps={{
          placeholder: `Search ${props.count} rows`,
          sx: { minWidth: 300 },
          variant: "outlined",
          marginTop: 0,
        }}
        initialState={{
          showGlobalFilter: true,
          globalFilter: true,
          paginationDisplayMode: "pages",
          positionToolbarAlertBanner: "top",
          showColumnFilters: true,
          columnPinning: {
            left: props.columns
              .filter((col) => Boolean(col.id) && col.enablePinning)
              // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
              .map((column) => column.id!.toString()),
            right: [props.pinAction ? "mrt-row-actions" : ""],
          },
        }}
        enableRowActions={
          Boolean(props.renderRowActions) ||
          Boolean(props.renderRowActionMenuItems)
        }
        enableRowNumbers={props.enableRowNumbers}
        positionActionsColumn="last"
        // Props related to selection
        enableRowSelection={props.enableRowSelection}
        onRowSelectionChange={props.onRowSelectionChange}
        enableSorting={!props.pagination}
        enableTopToolbar={
          Boolean(props.toolbarActions) || Boolean(props.toolbar)
        }
        muiTopToolbarProps={{
          sx: (theme: Theme) => ({
            background: theme.palette.background.paper,
            borderTopLeftRadius: `${theme.shape.borderRadius}px`,
            borderTopRightRadius: `${theme.shape.borderRadius}px`,
            display: "flex",
            justifyContent: "space-between",
          }),
        }}
        renderTopToolbarCustomActions={() => (
          <Stack
            direction="column"
            width="100%"
            display="flex"
            gap={2}
            justifyContent="end"
            alignItems="center"
            sx={{
              padding: props.toolbarActionMargin
                ? props.toolbarActionMargin
                : "25px",
            }}
          >
            {props.toolbarActions && (
              <Box width="100%" display="flex" gap={2} alignItems="center">
                {props.toolbarActions}
              </Box>
            )}
            {props.toolbar && (
              <Box width="100%" display="flex" gap={2} alignItems="center">
                {props.toolbar}
              </Box>
            )}
          </Stack>
        )}
        enableBottomToolbar={props.enableBottomToolbar}
        renderRowActions={
          !props.renderRowActions ? undefined : props.renderRowActions
        }
        renderRowActionMenuItems={
          props.hideRowActions
            ? undefined
            : !props?.renderRowActionMenuItems
            ? undefined
            : props.renderRowActionMenuItems
        }
        manualPagination={manualPagination}
        {...(props.pagination && {
          onPaginationChange: props.onPaginationChange,
        })}
        state={{
          isLoading: props.isLoading,
          showProgressBars: props.isFetching,
          showAlertBanner: props.isError,
          globalFilter: props.globalFilter,
          ...(props.rowSelection && {
            rowSelection: props.rowSelection,
          }),
          ...(props.pagination && {
            pagination: props.pagination,
          }),
        }}
        muiToolbarAlertBannerProps={
          props.isError
            ? {
                color: "error",
                children: "something went wrong loading records",
              }
            : undefined
        }
        muiTableHeadProps={{
          sx: {
            textTransform: "none",
          },
        }}
        enableFilters={false}
        enableGlobalFilterRankedResults
        enableColumnFilters={false}
        autoResetPageIndex={false}
        enableEditing={props.enableTableEditing}
        {...(props.enableTableEditing && {
          editingMode: props.editingMode ?? "table",
          memoMode: "cells",
          muiEditTextFieldProps: props.muiEditTextFieldProps,
        })}
        // Props related to enabling table styling.
        muiTablePaperProps={{
          elevation: props.variant !== "outlined" ? 0 : undefined,
          variant: props.variant,
        }}
        muiTableContainerProps={{
          sx: {
            borderRadius: props.toolbarActions ? 0 : "inherit",
            maxWidth: "100vw",
          },
        }}
        muiTablePaginationProps={{
          rowsPerPageOptions: props.rowsPerPageOptions ?? [
            5, 10, 20, 25, 30, 50, 100, 500, 1000,
          ],
        }}
        muiTableHeadCellProps={({ column }: any) => {
          return {
            sx: {
              background: "#F8F9FD",
            },
          };
        }}
        muiTableBodyCellProps={({ cell, row }: any) => {
          return {
            sx: (theme: Theme) => ({
              background:
                props.getRowBackgroundColor?.(row, theme) ??
                theme.palette.background.paper,
              "&:hover": {
                borderColor: "transparent", // or any desired color
              },
            }),
          };
        }}
        muiTableBodyRowProps={({ row }: any) => ({
          sx: (theme: Theme) => ({
            background:
              props.getRowBackgroundColor?.(row, theme) ??
              theme.palette.background.paper,
            cursor: props.onRowClick ? "pointer" : "inherit",
          }),
          onClick: () => {
            props.onRowClick?.(row.original);
          },
        })}
        muiTableBodyProps={(table: any) => {
          return {
            sx: () => ({}),
            children:
              !props.isLoading && !props.isFetching && !props.count ? (
                <tr>
                  <td
                    // style={{ textTransform: "capitalize" }}
                    colSpan={table.table.getVisibleLeafColumns().length}
                  >
                    <EmptyDataTable message={props.emptyMessage} />
                  </td>
                </tr>
              ) : undefined,
          };
        }}
        enableColumnActions={false}
        enableDensityToggle={false}
        enableFullScreenToggle={false}
        enableHiding={false}
        renderDetailPanel={
          !props?.renderDetailPanel ? undefined : props.renderDetailPanel
        }
        renderBottomToolbarCustomActions={
          props.renderBottomToolbarCustomActions
        }
        enableStickyHeader={props.enableStickyHeader}
        enableExpandAll={props.enableExpandAll}
        enableMultiRowSelection={props.enableMultiRowSelection}
      />
    </Box>
  );
};

type EmptyTableProps = {
  message?: string;
};
const EmptyDataTable = ({ message }: EmptyTableProps): JSX.Element => {
  return (
    <Box
      sx={{
        textAlign: "center",
        p: 5,
      }}
    >
      <Grid container direction="column" spacing={1}>
        <Grid item>
          <img src="/empty-box.svg" alt="Empty" />
        </Grid>
        <Grid item>
          <Typography variant="h4" fontWeight="500">
            {"empty"}
          </Typography>
          <Typography variant="body1">
            {message ?? "no records available"}
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};
