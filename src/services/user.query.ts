import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";

const authApi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getDeputyUsers: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase
          .from("users")
          .select("*")
          .eq("role", "DEPUTY");
        return { data };
      },
      providesTags: ["users"],
    }),
    getUsers: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase
          .from("users")
          .select("*")
          .eq("role", "USER");
        return { data };
      },
      providesTags: ["users"],
    }),
    getAllUsers: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase
          .from("users")
          .select("*")
          .in("role", ["USER", "DEPUTY"]);
        return { data };
      },
      providesTags: ["users"],
    }),
    getUnVerifiedDeputyUsers: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase
          .from("users")
          .select("*")
          .eq("status", "PENDING");
        return { data };
      },
      providesTags: ["users"],
    }),
    addChannel: build.mutation<any, any>({
      queryFn: async (channel: any) => {
        let { data } = await supabase.from("channels").insert(channel).single();
        return { data };
      },
      invalidatesTags: ["channels"],
    }),
    addDeputyUser: build.mutation<any, any>({
      queryFn: async (user: any) => {
        let { data } = await supabase.from("users").insert(user).single();

        return { data };
      },
      invalidatesTags: ["users"],
    }),
    updateDeputyUser: build.mutation<any, any>({
      queryFn: async (user: any) => {
        // Ensure the user object has an id field to identify the row to update
        const { id, ...rest } = user;
        if (!id) {
          return { error: { message: "User ID is required for update" } };
        }

        let { data, error } = await supabase
          .from("users")
          .update(rest)
          .eq("id", id)
          .select();

        if (error) {
          return { error };
        }

        return { data };
      },
      invalidatesTags: ["users"],
    }),

    addProgram: build.mutation<any, any>({
      queryFn: async (program: any) => {
        let { data } = await supabase
          .from("tv_schedule_program")
          .insert(program)
          .single();
        return { data };
      },
      invalidatesTags: ["channels"],
    }),
    deleteProgram: build.mutation<any, any>({
      queryFn: async (id: any) => {
        let { data } = await supabase
          .from("tv_schedule_program")
          .delete()
          .eq("id", id);
        return { data };
      },
      invalidatesTags: ["channels"],
    }),
    deleteUser: build.mutation<any, any>({
      queryFn: async (id: any) => {
        let { data } = await supabase.from("users").delete().eq("id", id);

        return { data };
      },
      invalidatesTags: ["users"],
    }),
    declineUser: build.mutation<any, any>({
      queryFn: async (id: any) => {
        let { data } = await supabase
          .from("users")
          .update({ status: "FAILED" })
          .eq("id", id)
          .select();
        return { data };
      },
      invalidatesTags: ["users"],
    }),
    approveUser: build.mutation<any, any>({
      queryFn: async (id: any) => {
        let { data } = await supabase
          .from("users")
          .update({ status: "VERIFIED" })
          .eq("id", id)
          .select();
        return { data };
      },
      invalidatesTags: ["users"],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetDeputyUsersQuery,
  useAddChannelMutation,
  useDeleteUserMutation,
  useGetUsersQuery,
  useAddProgramMutation,
  useDeleteProgramMutation,
  useApproveUserMutation,
  useDeclineUserMutation,
  useGetAllUsersQuery,
  useAddDeputyUserMutation,
  useUpdateDeputyUserMutation,
  useGetUnVerifiedDeputyUsersQuery,
} = authApi;
