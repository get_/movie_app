import { supabase } from "../utils/API";
import { Auth, User } from "../models/User";
import { updateUserActivity } from "./UserService";
import moment from "moment";

var bcrypt = require("bcryptjs");

async function comparePassword(password: string, hashedPassword: string) {
  return await bcrypt.compare(password, hashedPassword);
}

export async function login(email: string, password: string): Promise<Auth> {
  let isMatch = false;
  try {
    // sends a signIn request to supabase, authenticating the user
    const user = await getUserByEmail(email);
    if (user.role?.toLowerCase() !== "admin") {
      return {
        success: false,
        access_token: "",
        message: "You are not authorized to login.",
      };
    }
    isMatch = await comparePassword(password, user.password as string);
    if (isMatch) {
      let auth: Auth = await generateToken(email, password);
      auth.user = user;
      auth.message = "";
      await updateUserActivity({
        user_id: user.id,
        last_activity: new Date(Date.now()),
      });
      return auth;
    } else {
      return {
        success: isMatch,
        access_token: "",
        user: null as unknown as User,
        message: "Username or Password is incorrect.",
      };
    }
  } catch (e: any) {
    return {
      success: isMatch,
      access_token: "",
      user: null as unknown as User,
      message: "Oops,Something went wrong. Please try again.",
    };
  }
}

export async function generateToken(
  email: string,
  password: string
): Promise<Auth> {
  try {
    const { data, error } = await supabase.auth.signInWithPassword({
      email: email,
      password: password,
    });
    if (error) {
      return { success: false, access_token: "" };
    }

    return {
      access_token: data.session?.access_token as string,
      success: true,
    };
  } catch (e: any) {
    return { success: false, access_token: "" };
  }
}

export async function signOut() {
  const user = JSON.parse(localStorage.getItem("user") as string);
  await updateUserActivity({
    user_id: user?.id,
    last_activity: new Date(Date.now()),
  });
  localStorage.removeItem("user");
  localStorage.removeItem("access_token");
  return await supabase.auth.signOut();
}

export async function getUserByEmail(email: string): Promise<User> {
  const { data, error } = await supabase
    .from("users")
    .select("*")
    .eq("email", email)
    .limit(1)
    .single();
  if (error) throw error;
  return data as User;
}

function getDate() {
  const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
  const currentTime = moment();
  return currentTime;
}
export async function SignUp(email: string, password: string): Promise<any> {
  const { data, error } = await supabase.auth.signUp({
    email: email,
    password: password,
  });
  if (error) throw error;
  return data;
}
