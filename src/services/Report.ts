import {Message} from "../models/Message";
import {supabase} from "../utils/API";
import {DeputyReport, PublicReport, TVChannelReport} from "../models/Reports";

export async function getdeputyReports(): Promise<DeputyReport[]>{
    try {
        const { data, error } = await supabase
            .from("dupty_report")
            .select("*")
        if (error) throw error;

        if (data != null) {
            return data;
        }
    } catch (error) {
        console.log("error: ",error);}
    return [];
}

export async function deleteDeputyReport(id: number|undefined) {
    try {
        const { data, error } = await supabase
            .from("dupty_report")
            .delete()
            .eq("id", id);
        if (error) throw error;
    } catch (error) {
        console.log("error: ",error);}
}

export async function getPublicReports(): Promise<PublicReport[]>{
    try {
        const { data, error } = await supabase
            .from("report")
            .select("*")
        console.log(data,error)
        if (error) throw error;

        if (data != null) {
            return data;
        }
    } catch (error) {
        console.log("error: ",error);}
    return [];
}

export async function deletePublicReport(id: number|undefined) {
    try {
        const { data, error } = await supabase
            .from("report")
            .delete()
            .eq("id", id);
        if (error) throw error;
    } catch (error) {
        console.log("error: ",error);}
}

export async function getTVChannelReports(): Promise<TVChannelReport[]>{
    try {
        const { data, error } = await supabase
            .from("tv_program_report")
            .select("*")
        if (error) throw error;

        if (data != null) {
            return data;
        }
    } catch (error) {
        console.log("error: ",error);}
    return [];
}

export async function deleteTVChannelReport(id: number|undefined) {
    try {
        const { data, error } = await supabase
            .from("tv_program_report")
            .delete()
            .eq("id", id);
        if (error) throw error;
    } catch (error) {
        console.log("error: ",error);}
}
