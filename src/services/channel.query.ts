import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";

const authApi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getChannels: build.query<any, any>({
      queryFn: async (email: string) => {
        let { data } = await supabase.from("channels").select("*");
        return { data };
      },
      providesTags: ["channels"],
    }),
    getPrograms: build.query<any, { scheduleId: string; programDate: string }>({
      queryFn: async (params) => {
        let { data } = await supabase
          .from("tv_schedule_program")
          .select("*")
          .eq("tv_schedule_id", params.scheduleId)
          .eq("program_date", params.programDate);
        return { data };
      },
      providesTags: ["channels"],
    }),
    addChannel: build.mutation<any, any>({
      queryFn: async (channel: any) => {
        let { data } = await supabase.from("channels").insert(channel).single();
        return { data };
      },
      invalidatesTags: ["channels"],
    }),
    addProgram: build.mutation<any, any>({
      queryFn: async (program: any) => {
        let { data } = await supabase
          .from("tv_schedule_program")
          .insert(program)
          .single();
        return { data };
      },
      invalidatesTags: ["channels"],
    }),
    deleteProgram: build.mutation<any, any>({
      queryFn: async (id: any) => {
        let { data } = await supabase
          .from("tv_schedule_program")
          .delete()
          .eq("id", id);
        return { data };
      },
      invalidatesTags: ["channels"],
    }),
    deleteChannel: build.mutation<any, any>({
      queryFn: async (id: any) => {
        let { data } = await supabase.from("channels").delete().eq("id", id);
        return { data };
      },
      invalidatesTags: ["channels"],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetChannelsQuery,
  useAddChannelMutation,
  useGetProgramsQuery,
  useLazyGetProgramsQuery,
  useAddProgramMutation,
  useDeleteProgramMutation,
  useDeleteChannelMutation,
} = authApi;
