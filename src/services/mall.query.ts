import { Notify } from "../layouts/notify";
import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";

const mallAPi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getMalls: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("mall").select("*");
        return { data };
      },
      providesTags: ["mall"],
    }),

    getMallCinemas: build.query<any, any>({
      queryFn: async (ids: any) => {
        let { data } = await supabase.from("cinema").select("*");
        return { data };
      },
      providesTags: ["mall"],
    }),
    deleteMall: build.mutation<any, any>({
      queryFn: async (id) => {
        const { data, error } = await supabase
          .from("mall")
          .delete()
          .eq("id", id);
        if (error) {
          console.error("Error deleting mall:", error);
          return { error };
        }
        return { data };
      },
      invalidatesTags: ["mall"],
    }),

    addMall: build.mutation<any, any>({
      queryFn: async (mall: any) => {
        let { data } = await supabase.from("mall").insert(mall).single();
        return { data };
      },
      invalidatesTags: ["mall"],
    }),
    updateMall: build.mutation<any, any>({
      queryFn: async (mall: any) => {
        let { data } = await supabase
          .from("malls")
          .update(mall)
          .eq("id", mall.id)
          .select();
        return { data };
      },
      invalidatesTags: ["mall"],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetMallCinemasQuery,
  useGetMallsQuery,
  useLazyGetMallCinemasQuery,
  useAddMallMutation,
  useDeleteMallMutation,
  useUpdateMallMutation,
} = mallAPi;
