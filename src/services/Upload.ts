import { supabase, supabaseUrl } from "../utils/API";
interface StorageResponse {
  Key: string;
  path: any;
  fullPath: any; // Use 'any' here if absolutely necessary
}

export async function upload(file: File, bucket: string, filename: string) {
  console.log("Uploading...");
  try {
    const { data, error } = await supabase.storage
      .from(bucket)
      .upload(`avatar/${filename}`, file, {
        cacheControl: "3600", // optional
        upsert: false, // optional
      });

    if (error) {
      throw new Error(error.message);
    }

    const response: StorageResponse = data as any; // Cast the response to your interface
    console.log("Upload successful", response);

    if (response && response.fullPath) {
      return {
        url: `${supabaseUrl}/storage/v1/object/public/${response.fullPath}`,
      };
    } else {
      throw new Error("File path is undefined");
    }
  } catch (error: any) {
    console.error("Error uploading file to Supabase Storage:", error.message);
    return { url: "", error: error.message };
  }
}

export async function createSignInUrl(key: string, bucket: string) {
  const { data, error } = await supabase.storage
    .from(bucket)
    .createSignedUrl(key, 30);
  if (error) {
    throw error;
  }
  if (data) {
    return data.signedUrl;
  }
}

export async function deleteFile(bucket: string, filename: string[]) {
  try {
    const { data, error } = await supabase.storage
      .from(bucket)
      .remove(filename);
  } catch (error) {
    console.log("Error deleting file from Supabase Storage:", error);
    return { url: "", error };
  }
}
