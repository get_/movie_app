import { Category } from "../models/Category";
import { Genre, Movie } from "../models/Movie";
import { supabase } from "../utils/API";

export async function addMovie(movie: Movie) {
  try {
    const { data, error } = await supabase
      .from("movies")
      .insert(movie)
      .single();

    if (error) throw error;
    // window.location.reload();
  } catch (error) {
    console.log("error: ", error);
  }
}

export async function addMovieSchedule(movie: any) {
  try {
    const { data, error } = await supabase
      .from("movie_schedule")
      .insert(movie)
      .select();

    if (error) throw error;
    // window.location.reload();
  } catch (error) {
    console.log("error: ", error);
  }
}

export async function getMovieSchedule(): Promise<Movie[]> {
  try {
    const { data, error } = await supabase.from("movie_schedule").select("*");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.log("error: ", error);
  }
  return [];
}

export async function getMovies(): Promise<Movie[]> {
  try {
    const { data, error } = await supabase.from("movies").select("*");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.log("error: ", error);
  }
  return [];
}
export async function getCategory(id: number) {
  try {
    const { data, error } = await supabase
      .from("categories")
      .select("name")
      .eq("id", id)
      .limit(1);
    if (error) throw error;

    if (data != null) {
      return data[0].name;
    }
    return "Not Given";
  } catch (error) {
    console.log("error: ", error);
  }
  return "Not Given";
}

export async function updateMovie(movie: Movie) {
  try {
    const { data, error } = await supabase
      .from("movies")
      .update(movie)
      .eq("id", movie.id)
      .select();
    if (error) throw error;

    if (data != null) {
      console.log("data: ", data);
      return data;
    }
  } catch (error) {
    console.log("error: ", error);
  }
}

export async function deleteMovie(id: number | undefined) {
  try {
    const { data, error } = await supabase.from("movies").delete().eq("id", id);
    if (error) throw error;
    // window.location.reload();
  } catch (error) {
    console.log("error: ", error);
  }
}

// genre service
export async function getGenres(): Promise<Genre[]> {
  try {
    const { data, error } = await supabase.from("filters").select("*");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.log("error: ", error);
  }
  return [];
}

export async function getMovieReviewers(ids: number[] | undefined) {
  try {
    if (ids === undefined || ids.length === 0) return [];
    const { data, error } = await supabase
      .from("board_member")
      .select("*")
      .in("id", ids);
    if (error) throw error;

    if (data != null) {
      return data;
    }
    return [];
  } catch (error) {
    console.log("error: ", error);
  }
  return [];
}

export async function getCategories(): Promise<Category[]> {
  try {
    const { data, error } = await supabase.from("categories").select("*");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.error("Error fetching categories:", error);
    return [];
  }
  return [];
}
