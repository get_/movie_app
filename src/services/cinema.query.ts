import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";

const cinemaApi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getCinemas: build.query<any, any>({
      queryFn: async () => {
        let { data, error } = await supabase
          .from("cinema")
          .select("*")
          .order("created_at", { ascending: false });
        if (error) throw error;
        return { data };
      },
      providesTags: ["Cinema"],
    }),
    addCinema: build.mutation<any, any>({
      queryFn: async (cinema) => {
        let { data, error } = await supabase.from("cinema").insert(cinema);
        if (error) throw error;
        return { data };
      },
      invalidatesTags: ["Cinema"],
    }),
    deleteCinema: build.mutation<any, any>({
      queryFn: async (id) => {
        try {
          const { data, error } = await supabase
            .from("cinema")
            .delete()
            .eq("id", Number(id));
          if (error) {
            console.error("Error deleting cinema:", error);
            throw error;
          }
          return { data };
        } catch (error) {
          console.error("Unexpected error during deletion:", error);
          throw error;
        }
      },
      invalidatesTags: ["Cinema"],
    }),

    getCinemaMovies: build.query<any, any>({
      queryFn: async (cinemaId) => {
        let { data, error } = await supabase
          .from("cinema_movies")
          .select(
            `
            movies (
              *
            )
          `
          )
          .eq("cinema_id", cinemaId);
        if (error) throw error;
        return { data: data?.map((entry) => entry.movies) };
      },
      providesTags: ["Cinema"],
    }),
    updateCinema: build.mutation<any, any>({
      queryFn: async ({ id, ...cinema }) => {
        let { data, error } = await supabase
          .from("cinema")
          .update(cinema)
          .eq("id", id)
          .single();
        if (error) throw error;
        return { data };
      },
      invalidatesTags: ["Cinema"],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetCinemasQuery,
  useAddCinemaMutation,
  useDeleteCinemaMutation,
  useGetCinemaMoviesQuery,
  useUpdateCinemaMutation,
} = cinemaApi;
