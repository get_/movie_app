import { error } from "console";
import { Channel } from "../models/Channel";
import { supabase } from "../utils/API";
import { Movie } from "../models/Movie";
import { DeputyUser, User, UserActivity } from "../models/User";

export async function getUsers(): Promise<User[]> {
  try {
    const { data, error } = await supabase
      .from("users")
      .select("*")
      .eq("role", "USER");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}

export async function getDeputyUsers(): Promise<User[]> {
  try {
    const { data, error } = await supabase
      .from("users")
      .select("*")
      .eq("role", "DEPUTY");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}

export async function getUnVerifiedDeputyUsers(): Promise<User[]> {
  try {
    const { data, error } = await supabase
      .from("users")
      .select("*")
      .eq("role", "DEPUTY")
      .eq("is_verified", false);
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}

export async function deleteUser(id: number | undefined) {
  try {
    const { data, error } = await supabase.from("users").delete().eq("id", id);
    if (error) throw error;
    // window.location.reload();
  } catch (error) {}
}

async function addUserActivity(activity: UserActivity) {
  try {
    const { data, error } = await supabase
      .from("user_activities")
      .insert(activity)
      .single();

    if (error) throw error;
  } catch (error) {}
}
export async function addDeputyUser(activity: DeputyUser) {
  try {
    const { error } = await supabase.from("users").insert(activity).single();

    if (error) throw error;
  } catch (error) {}
}
export async function uploadUserFile(
  file: File,
  bucket: string,
  filename: string
) {
  console.log("uploading ...");
  try {
    const { data, error } = await supabase.storage
      .from(bucket)
      .upload(`mtrcb/users/upload${filename}`, file, {
        cacheControl: "3600", // optional
        upsert: false, // optional
      });

    if (error) {
      throw error;
    }

    if (data) {
      return { url: data.path };
    }

    throw new Error("Failed to upload file.");
  } catch (error) {
    console.error("Error uploading file to Supabase Storage:", error);
    return { url: "", error };
  }
}

export async function getStorageUrl(key: any) {
  try {
    const { data } = await supabase.storage.from("users").getPublicUrl(key);

    console.log("File URL:", data.publicUrl);
    return data.publicUrl;
  } catch (error: any) {
    console.error("Error getting file URL:", error.message);
    return null;
  }
}

export async function updateUserActivity(activity: UserActivity) {
  try {
    const res = await checkUserActivity(activity.user_id);
    if (!res.found) {
      await addUserActivity(activity);
      return;
    }
    const { data, error } = await supabase
      .from("user_activities")
      .update(activity)
      .eq("id", res.id)
      .select();
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
}

async function checkUserActivity(user_id: number) {
  try {
    const { data, error } = await supabase
      .from("user_activities")
      .select("id")
      .eq("user_id", user_id)
      .single();
    if (error || data == null) {
      return { found: false, id: null };
    } else if (data !== null) {
      return { found: true, id: data.id };
    }
  } catch (error) {
    return { found: false, id: null };
  }
  return { found: false, id: null };
}
export async function getLastActivity(user_id: number) {
  try {
    const { data, error } = await supabase
      .from("user_activities")
      .select("last_activity")
      .eq("user_id", user_id)
      .single();
    if (error || data == null) {
      return null;
    } else if (data !== null) {
      return data.last_activity;
    }
  } catch (error) {
    return null;
  }
  return null;
}

export async function verifyUser(id: number): Promise<User> {
  try {
    const { data, error } = await supabase
      .from("users")
      .update({ is_verified: true })
      .eq("id", id)
      .select();
    if (error) throw error;

    if (data != null) {
      return data[0];
    }
  } catch (error) {}
  return {} as User;
}
export async function declineUser(id: number): Promise<User> {
  try {
    const { data, error } = await supabase
      .from("users")
      .update({ is_verified: false })
      .eq("id", id)
      .select();
    if (error) throw error;

    if (data != null) {
      return data[0];
    }
  } catch (error) {}
  return {} as User;
}
