import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";
import { deleteDeputyReport, getPublicReports } from "./Report";
import { addTVChannelReportResponse } from "./Response";

const reportApi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getDeputyReports: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("dupty_report").select("*");
        return { data };
      },
      providesTags: ["reports"],
    }),
    getPublicReports: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("report").select("*");
        return { data };
      },
      providesTags: ["reports"],
    }),

    getDeputyReportId: build.query<any, any>({
      queryFn: async (id: string) => {
        let { data } = await supabase
          .from("dupty_report")
          .delete()
          .eq("id", id);
        return { data };
      },
      providesTags: ["reports"],
    }),
    deletePublicReport: build.mutation<any, any>({
      queryFn: async (id: string) => {
        let { data } = await supabase.from("report").delete().eq("id", id);
        return { data };
      },
      invalidatesTags: ["reports"],
    }),
    deleteDeputyReport: build.mutation<any, any>({
      queryFn: async (id: string) => {
        let { data } = await supabase
          .from("dupty_report")
          .delete()
          .eq("id", id);
        return { data };
      },
      invalidatesTags: ["reports"],
    }),
    addPublicResponse: build.mutation<any, any>({
      queryFn: async (response) => {
        let { data } = await supabase
          .from("public_responses")
          .insert(response)
          .single();
        return { data };
      },
      invalidatesTags: ["reports"],
    }),

    addDeputyResponse: build.mutation<any, any>({
      queryFn: async (response) => {
        let { data } = await supabase
          .from("deputy_responses")
          .insert(response)
          .single();
        return { data };
      },
      invalidatesTags: ["reports"],
    }),
    addTVChannelReportResponse: build.mutation<any, any>({
      queryFn: async (response) => {
        let { data } = await supabase
          .from("tv_channel_responses")
          .insert(response)
          .single();
        return { data };
      },
      invalidatesTags: ["reports"],
    }),

    getTVChannelReports: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("tv_program_report").select("*");
        return { data };
      },
      providesTags: ["reports"],
    }),

    deleteTVChannelReport: build.mutation<any, any>({
      queryFn: async (id) => {
        let { data } = await supabase
          .from("tv_program_report")
          .delete()
          .eq("id", id);
        return { data };
      },
      invalidatesTags: ["reports"],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetDeputyReportIdQuery,
  useGetTVChannelReportsQuery,
  useGetDeputyReportsQuery,
  useLazyGetDeputyReportIdQuery,
  useDeleteTVChannelReportMutation,
  useDeletePublicReportMutation,
  useLazyGetTVChannelReportsQuery,
  useLazyGetDeputyReportsQuery,
  useGetPublicReportsQuery,
  useAddPublicResponseMutation,
  useAddTVChannelReportResponseMutation,
  useAddDeputyResponseMutation,
  useDeleteDeputyReportMutation
} = reportApi;
