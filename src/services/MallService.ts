import { supabase } from "../utils/API";
import { Mall } from "../models/Mall";
import { Cinema } from "../models/Cinema";
import { getCinemaMall } from "./CinemaService";
import { City } from "../models/City";

export async function addMall(mall: Mall) {
  try {
    const { data, error } = await supabase.from("mall").insert(mall).single();
    if (error) throw error;
  } catch (error) {}
}
export async function getMalls(): Promise<Mall[]> {
  try {
    const { data, error } = await supabase.from("mall").select("*");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}
export async function deleteMall(id: number | undefined) {
  try {
    const { data, error } = await supabase.from("mall").delete().eq("id", id);
    if (error) throw error;
    // window.location.reload();
  } catch (error) {
    console.log("error: ", error);
  }
}

export async function updateMall(mall: Mall) {
  try {
    const { data, error } = await supabase
      .from("malls")
      .update(mall)
      .eq("id", mall.id)
      .select();
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.log("error: ", error);
  }
}
export async function getMallCinemas(ids: number[]): Promise<Cinema[]> {
  try {
    const { data, error } = await supabase
      .from("cinemas")
      .select("*")
      .in("id", ids);
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}

export async function getMallCity(cinema_id: number): Promise<City[]> {
  try {
    const malls = await getCinemaMall(cinema_id);
    let ids = malls.map((mall) => mall.id?.toString());
    const { data, error } = await supabase
      .from("cities")
      .select("*")
      .contains("malls", ids);

    if (error) {
      return [];
    }
    if (data != null) {
      return data;
    }
  } catch (error) {
    return [];
  }
  return [];
}
