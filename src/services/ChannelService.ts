import { Channel } from "../models/Channel";
import { supabase } from "../utils/API";
import {BoardMember} from "../models/BoardMember";
import {Program} from "../models/Program";

export async function addChannel(channel: Channel) {
    try {
      const { data, error } = await supabase
        .from("channels")
        .insert(channel)
        .single();

      if (error) throw error;
      // window.location.reload();
    } catch (error) {
        console.log("error: ",error);
    
    }
  }

  export async function getChannels(): Promise<Channel[]>{
    try {
      const { data, error } = await supabase
        .from("channels")
        .select("*")
      if (error) throw error;

      if (data != null) {
        return data;
      }
    } catch (error) {
      console.log("error: ",error);}
      return [];
  }

  export async function getDelete() {
    try {
      const { data, error } = await supabase
        .from("channels")
        .select("*")
      if (error) throw error;

      if (data != null) {
        return data;
      }
    } catch (error) {
      console.log("error: ",error);}
  }

  export async function updateChannel(channel:Channel) {
    try {
      const { data, error } = await supabase
        .from("channels")
        .update(channel)
        .eq("id", channel.id)
        .select();
      if (error) throw error;

      if (data != null) {
        console.log("data: ",data);
      }
    } catch (error) {
      console.log("error: ",error);}
  }


  export async function deleteChannel(id: number|undefined) {
    try {
      const { data, error } = await supabase
        .from("channels")
        .delete()
        .eq("id", id);
      if (error) throw error;
      // window.location.reload();
    } catch (error) {
      console.log("error: ",error);}
  }
export async function getBoardMembers(): Promise<BoardMember[]>{
  try {
    const { data, error } = await supabase
        .from("board_member")
        .select("*")
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.log("error: ",error);}
  return [];
}

export async function getPrograms(): Promise<Program[]>{
  try {
    const { data, error } = await supabase
        .from("tv_schedule_program")
        .select("*")
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.log("error: ",error);}
  return [];
}

export async function getChannel(id:number){
  try {
    const { data, error } = await supabase
        .from("channels")
        .select("title").eq("id", id).limit(1)
    if (error) throw error;

    if (data != null) {
      return data[0].title;
    }
    return 'Not Given';
  } catch (error) {
    console.log("error: ",error);}
  return 'Not Given';
}
export async function deleteProgram(id: number|undefined) {
  try {
    const { data, error } = await supabase
        .from("tv_schedule_program")
        .delete()
        .eq("id", id);
    if (error) throw error;
    // window.location.reload();
  } catch (error) {
    console.log("error: ",error);}
}

export async function addProgram(program: Program) {
  try {
    const { data, error } = await supabase
        .from("tv_schedule_program")
        .insert(program)
        .single();

    if (error) throw error;
    // window.location.reload();
  } catch (error) {
    console.log("error: ",error);

  }
}
