import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";

const reportApi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getMessages: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("unique_messages").select("*");
        return { data };
      },
      providesTags: ["messages"],
    }),
    deleteMessage: build.mutation<any, any>({
      queryFn: async (id: string) => {
        let { data } = await supabase
          .from("message")
          .delete()
          .eq("message", id);
        return { data };
      },
      invalidatesTags: ["messages"],
    }),

    addMessage: build.mutation<any, any>({
      queryFn: async (message) => {
        console.log("message", message);
        let { data, error } = await supabase.from("message").insert(message);
        return { data };
      },
      invalidatesTags: ["messages"],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetMessagesQuery,
  useDeleteMessageMutation,
  useAddMessageMutation,
} = reportApi;
