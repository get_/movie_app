import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";
import { addMovieSchedule } from "./MovieService";

const cityAPi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getMovies: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("movies").select("*");
        return { data };
      },
      providesTags: ["movies"],
    }),

    getMovieSchedule: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("movie_schedule").select("*");
        return { data };
      },
      providesTags: ["movies"],
    }),
    getCategoryById: build.query<any, any>({
      queryFn: async (id: string) => {
        let { data } = await supabase
          .from("categories")
          .select("name")
          .eq("id", id)
          .limit(1);
        return { data };
      },
      providesTags: ["movies"],
    }),

    getCategories: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("categories").select("*");
        return { data };
      },
      providesTags: ["movies"],
    }),

    getGenres: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("filters").select("*");

        return { data };
      },
      providesTags: ["movies"],
    }),

    getMovieReviewers: build.query<any, any>({
      queryFn: async (ids: any) => {
        let { data } = await supabase
          .from("board_member")
          .select("*")
          .in("id", ids);
        return { data };
      },
      providesTags: ["movies"],
    }),

    deleteMovie: build.mutation<any, any>({
      queryFn: async (id) => {
        let { data } = await supabase.from("movies").delete().eq("id", id);
        return { data };
      },
      invalidatesTags: ["movies"],
    }),
    deleteMovieSchedule: build.mutation<any, any>({
      queryFn: async (id) => {
        let { data } = await supabase
          .from("movie_schedule")
          .delete()
          .eq("id", id);
        return { data };
      },
      invalidatesTags: ["movies"],
    }),
    addMove: build.mutation<any, any>({
      queryFn: async (movie: any) => {
        let { data } = await supabase.from("movies").insert(movie).single();
        return { data };
      },
      invalidatesTags: ["movies"],
    }),
    addMovieSchedule: build.mutation<any, any>({
      queryFn: async (movie: any) => {
        let { data } = await supabase
          .from("movie_schedule")
          .insert(movie)
          .select();
        return { data };
      },
      invalidatesTags: ["movies"],
    }),

    updateMovie: build.mutation<any, any>({
      queryFn: async (movie: any) => {
        let { data } = await supabase
          .from("movies")
          .update(movie)
          .eq("id", movie.id)
          .select();
        return { data };
      },
      invalidatesTags: ["movies"],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetMoviesQuery,
  useDeleteMovieMutation,
  useAddMoveMutation,
  useAddMovieScheduleMutation,
  useUpdateMovieMutation,
  useGetCategoryByIdQuery,
  useGetCategoriesQuery,
  useGetMovieReviewersQuery,
  useGetMovieScheduleQuery,
  useDeleteMovieScheduleMutation,
  useGetGenresQuery,
} = cityAPi;
