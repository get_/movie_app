import { apiSlice } from "../store/app.api";
import { supabase } from "../utils/API";

const authApi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getUserByEmail: build.query<any, any>({
      queryFn: async (email: string) => {
        let { data } = await supabase
          .from("users")
          .select("*")
          .eq("email", email)
          .limit(1)
          .single();
        return { data };
      },
      providesTags: ["users"],
    }),
    deleteCity: build.mutation<any, any>({
      queryFn: async (id) => {
        let { data } = await supabase.from("city").delete().eq("id", id);
        return { data };
      },
      invalidatesTags: ["city"],
    }),
    addCity: build.mutation<any, any>({
      queryFn: async (city) => {
        let { data } = await supabase.from("city").insert(city).single();
        return { data };
      },
      invalidatesTags: ["city"],
    }),
  }),
  overrideExisting: true,
});

export const { useGetUserByEmailQuery, useDeleteCityMutation,useAddCityMutation } = authApi;
