import { Channel } from "../models/Channel";
import { supabase } from "../utils/API";
import { Cinema, CinemaMovie } from "../models/Cinema";
import { Movie } from "../models/Movie";
import { Mall } from "../models/Mall";

export async function addCinema(cinema: Cinema) {
  try {
    const { data, error } = await supabase
      .from("cinema")
      .insert(cinema)
      .single();

    if (error) throw error;
  } catch (error) {}
}
export async function getCinemas(): Promise<Cinema[]> {
  try {
    const { data, error } = await supabase.from("cinema").select("*");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}

export async function addCinemaMovie(cinema: CinemaMovie) {
  try {
    const { data, error } = await supabase
      .from("cinema_movies")
      .insert(cinema)
      .single();

    if (error) throw error;
  } catch (error) {}
}
async function getCinemaMovie(cinema_id: number): Promise<number[]> {
  try {
    const { data, error } = await supabase
      .from("cinema_movies")
      .select("movie_id")
      .eq("cinema_id", cinema_id);
    if (error) throw error;

    if (data != null) {
      return data.map((item: any) => item.movie_id);
    }
  } catch (error) {}
  return [];
}
export async function getCinemaMovies(cinema_id: number): Promise<Movie[]> {
  try {
    const movie_ids = await getCinemaMovie(cinema_id);
    const { data, error } = await supabase
      .from("movies")
      .select("*")
      .in("id", movie_ids);
    if (error) throw error;
    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}

export async function deleteCinema(id: number) {
  try {
    const { data, error } = await supabase.from("cinema").delete().eq("id", id);
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
}

async function getCinemaMovieId(
  cinema_id: number,
  movie_id: number
): Promise<number | null> {
  try {
    const { data, error } = await supabase
      .from("cinema_movies")
      .select("id")
      .eq("movie_id", movie_id)
      .eq("cinema_id", cinema_id)
      .single();
    console.log(data);
    console.log(error);

    if (error) throw error;
    if (data != null) {
      return data.id;
    }
  } catch (error) {}
  return null;
}

export async function deleteCinemaMovie(cinema_id: number, movie_id: number) {
  try {
    const id = await getCinemaMovieId(cinema_id, movie_id);
    alert(cinema_id + "---" + movie_id);
    console.log("id: ", id);
    const { data, error } = await supabase
      .from("cinema_movies")
      .delete()
      .eq("id", id);
    if (error) throw error;
    if (data != null) {
      return data;
    }
  } catch (error) {}
}

export async function getCinemaMall(cinema_id: number): Promise<Mall[]> {
  try {
    const { data, error } = await supabase
      .from("malls")
      .select("*")
      .contains("cinemas", [cinema_id.toString()]);
    if (error) {
      return [];
    }
    if (data != null) {
      return data;
    }
  } catch (error) {
    return [];
  }
  return [];
}
