import {Message} from "../models/Message";
import {supabase} from "../utils/API";
import {DeputyReportResponse, PublicReportResponse, TvChannelReportResponse} from "../models/Response";

export async function addPublicReportResponse(response: PublicReportResponse) {
    try {
        const { data, error } = await supabase
            .from("public_responses")
            .insert(response)
            .single();

        if (error) throw error;
    } catch (error) {
        console.log("error: ",error);

    }
}
export async function addDeputyReportResponse(response: DeputyReportResponse) {
    try {
        const { data, error } = await supabase
            .from("deputy_responses")
            .insert(response)
            .single();
        return data;
    } catch (error) {
        console.log("error: ",error);
        return error

    }
}
export async function addTVChannelReportResponse(response: TvChannelReportResponse) {
    try {
        const { data, error } = await supabase
            .from("tv_channel_responses")
            .insert(response)
            .single();

        if (error) throw error;
    } catch (error) {
        console.log("error: ",error);

    }
}
