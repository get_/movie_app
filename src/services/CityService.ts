import { supabase } from "../utils/API";
import { City } from "../models/City";
import { Cinema } from "../models/Cinema";
import { Movie } from "../models/Movie";
import { Mall } from "../models/Mall";

export async function addCity(city: City) {
  console.log(city);
  try {
    const { data, error } = await supabase.from("city").insert(city).single();
    console.log(error);

    if (error) throw error;
  } catch (error) {}
}
export async function getCities(): Promise<City[]> {
  try {
    const { data, error } = await supabase.from("city").select("*");
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}
export async function deleteCity(id: number | undefined) {
  try {
    const { data, error } = await supabase.from("city").delete().eq("id", id);
    if (error) throw error;
    // window.location.reload();
  } catch (error) {
    console.log("error: ", error);
  }
}

export async function updateCity(city: City) {
  try {
    const { data, error } = await supabase
      .from("movies")
      .update(city)
      .eq("id", city.id)
      .select();
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {
    console.log("error: ", error);
  }
}

export async function getCityMalls(ids: number[]): Promise<Mall[]> {
  try {
    const { data, error } = await supabase
      .from("malls")
      .select("*")
      .in("id", ids);
    if (error) throw error;

    if (data != null) {
      return data;
    }
  } catch (error) {}
  return [];
}
