import {Movie} from "../models/Movie";
import {supabase} from "../utils/API";
import {Message} from "../models/Message";

export async function addMessage(message: Message) {
    try {
        const { data, error } = await supabase
            .from("message")
            .insert(message)
            .single();

        if (error) throw error;
        // window.location.reload();
    } catch (error) {
        console.log("error: ",error);

    }
}


export async function getMessages(): Promise<Message[]>{
    try {
        const { data, error } = await supabase
            .from("message")
            .select("*")
        if (error) throw error;

        if (data != null) {
            return data;
        }
    } catch (error) {
        console.log("error: ",error);}
    return [];
}

export async function deleteMessage(id: number|undefined) {
    try {
        const { data, error } = await supabase
            .from("message")
            .delete()
            .eq("id", id);
        if (error) throw error;
        // window.location.reload();
    } catch (error) {
        console.log("error: ",error);}
}
