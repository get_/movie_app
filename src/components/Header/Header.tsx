import React, { useContext, useEffect, useState } from "react";
import {
  AppBar,
  Box,
  Grid,
  Toolbar,
  Typography,
  useMediaQuery,
} from "@mui/material";
import UserContext from "../../context/UserContext";
import { User, UserContextType } from "../../models/User";
import { getUserByEmail } from "../../services/AuthService";
import useIsMobile from "../../hooks/is-mobile";

const Header: React.FC = () => {
  const [loggedUser, setUser] = useState<User>({} as User);

  const { user } = useContext(UserContext) as UserContextType;
  const lUser: User = JSON.parse(localStorage.getItem("user") as string);
  const email = lUser?.email || (user as string);
  useEffect(() => {
    const fetchUser = async () => {
      if (email) {
        const u: User = await getUserByEmail(email);
        if (u) {
          setUser(u);
        }
      }
    };
    fetchUser();
  }, []);
  const isMobile = useIsMobile();

  return (
    <AppBar
      elevation={0}
      position="static"
      sx={{
        backgroundColor: "#2891FF",
        boxShadow: 1,
        zIndex: (theme) => theme.zIndex.drawer + 4,
        height: 65,
      }}
    >
      <Toolbar>
        <Grid container alignItems="center" justifyContent="center">
          <Grid
            item
            xs={12}
            md={10}
            lg={8}
            sx={{
              display: "flex",
              justifyContent: isMobile ? "center" : "space-between", // Center align on mobile, space-between on other sizes
              marginX: isMobile ? "0" : "20px", // Remove margin on mobile
            }}
          >
            {!isMobile && ( // Show logo only on larger screens
              <Box>
                <img
                  src="/icon.png"
                  alt="Logo"
                  style={{
                    width: "100%",
                    maxWidth: "94px",
                    height: "auto",
                    borderRadius: "5px",
                  }}
                />
              </Box>
            )}

            {/* User info */}
            <Box sx={{ display: "flex", alignItems: "center", gap: "1em" }}>
              <img
                alt={loggedUser.name}
                src={loggedUser.profile_picture}
                style={{
                  width: "3.75em",
                  height: "3.75em",
                  borderRadius: "50%",
                }}
              />
              <Box>
                <Typography variant="subtitle1" sx={{ color: "#fff" }}>
                  {loggedUser.name}
                </Typography>
                <Typography variant="body2" sx={{ color: "#ccc" }}>
                  {loggedUser.role?.toLowerCase()}
                </Typography>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
