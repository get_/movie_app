import {
  Chip,
  Container,
  FormControl,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React, { useEffect, useState } from "react";

import LoadingButton from "@mui/lab/LoadingButton";
import CustomizedSnackbars from "../common/SnackBar";
import { useFormik } from "formik";
import { MallValidationsForm } from "./Validation";
import { Mall } from "../../models/Mall";
import { addMall } from "../../services/MallService";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ErrorIcon from "@mui/icons-material/Error";
import OutlinedInput from "@mui/material/OutlinedInput";
import { Cinema } from "../../models/Cinema";
import { getCinemas } from "../../services/CinemaService";

export default function AddNewMall() {
  const formik = useFormik({
    initialValues: {
      name: "",
      cinemas: [],
    },
    validationSchema: MallValidationsForm,
    onSubmit: async (values, { resetForm }) => {
      setLoading(true);
      let formData: Mall = {
        name: values.name,
        cinemas: values.cinemas,
      };
      await addMall(formData);
      setOpen(true);
      setLoading(false);
      setMessage("Cinema created successfully.");
      resetForm();
    },
  });

  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState<boolean>(false);
  const [message, setMessage] = React.useState<string>("");
  const [cinemas, setCinemas] = useState<Cinema[]>([]);
  const [mallSelected, setMallSelected] = useState<boolean>(false);

  useEffect(() => {
    const fetchCinema = async () => {
      const _cinemas = await getCinemas();
      setCinemas(_cinemas);
    };
    fetchCinema();
  }, []);

  return (
    <Container
      sx={{
        position: "absolute",
        display: "flex",
        flexDirection: "column",
        backgroundColor: "white",
        borderRadius: "15px",
        marginTop: "30px",
        padding: "15px",
        width: "50%",
        height: "370px",
        top: "10%",
        gap: "0px",
        opacity: "0px",
        marginX: "10%",
      }}
    >
      <form
        onSubmit={formik.handleSubmit}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          alignContent: "center",
          gap: "15px",
        }}
      >
        <FormControl>
          <label
            htmlFor="id_reason"
            style={{
              color: "#AFAFAF",
              marginBottom: "10px",
            }}
          >
            Name
          </label>
          <TextField
            sx={{ width: "358px" }}
            name="name"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.touched.name && formik.errors.name}
            onBlur={formik.handleBlur}
            placeholder={"e.g Majestic Cinemas"}
            variant="outlined"
            type="text"
          />
        </FormControl>

        <FormControl>
          <label
            style={{
              color: "#AFAFAF",
              backgroundColor: "white",
              marginBottom: "10px",
            }}
          >
            Cinemas
          </label>
          <InputLabel
            id="multiple_cinemas"
            style={{
              color: cinemas.length === 0 ? "red" : "#AFAFAF",
              backgroundColor: "white",
              marginTop: "30px",
              marginLeft: cinemas.length > 0 ? "10px" : "10px",
              marginBottom: "5px",
            }}
          >
            {mallSelected
              ? cinemas.length > 0
                ? ""
                : "No Available Cinemas"
              : "Select Cinema"}
          </InputLabel>
          <Select
            sx={{
              width: "358px",
            }}
            id="multiple_cinemas"
            labelId="multiple_cinemas"
            IconComponent={() =>
              cinemas.length > 0 ? (
                <ExpandMoreIcon />
              ) : (
                <ErrorIcon sx={{ color: "red", marginRight: "15px" }} />
              )
            }
            disabled={cinemas.length === 0}
            multiple
            name={"cinemas"}
            variant={"outlined"}
            value={formik.values.cinemas}
            onChange={formik.handleChange}
            input={<OutlinedInput />}
            renderValue={(selected) => (
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                {selected.map((value) => (
                  <Chip
                    key={value}
                    label={cinemas.find((cinema) => cinema.id === value)?.name}
                    style={{ margin: 2 }}
                  />
                ))}
              </div>
            )}
            MenuProps={{
              PaperProps: {
                style: {
                  maxHeight: 48 * 4.5 + 8,
                  width: 250,
                },
              },
            }}
            onOpen={() => setMallSelected(true)}
          >
            {cinemas.map((cinema) => (
              <MenuItem key={cinema.id} value={cinema.id}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    gap: "10px",
                  }}
                >
                  <ListItemText primary={cinema.name} />
                </div>
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <LoadingButton
          loading={loading}
          type="submit"
          variant="contained"
          color="primary"
          className="hover:bg-green-700 text-white px-4 py-2 rounded-md bg-color: #4CAF50;"
          sx={{
            textTransform: "none",
            backgroundColor: "#0054D3",
            width: "358px",
            height: "49px",
            padding: "15px",
            gap: "0px",
            borderRadius: "55px",
            opacity: "0px",
          }}
        >
          Add Cinema
        </LoadingButton>
      </form>
      <CustomizedSnackbars message={message} open={open} />
    </Container>
  );
}
