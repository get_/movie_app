import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  OutlinedInput,
  Chip,
  Box,
  Checkbox,
} from "@mui/material";
import React, { useMemo, useState } from "react";
import { Formik, Form, Field } from "formik";
import { LoadingButton } from "@mui/lab";
import { Notify } from "../../layouts/notify";
import Add from "@mui/icons-material/Add";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import * as Yup from "yup";

import {
  useAddMallMutation,
  useDeleteMallMutation,
  useGetMallCinemasQuery,
  useGetMallsQuery,
} from "../../services/mall.query";
import { DefaultPage } from "../../layouts/default-page";
import { format } from "date-fns";
import { useGetCiyQuery } from "../city/query";
import { DataTable } from "../../layouts/data-table";
import { Delete } from "@mui/icons-material";
const mallSchema = Yup.object({
  name: Yup.string().required("Mall name is required"),
  street: Yup.string().required("Street is required"),
  zip_code: Yup.string()
    .required("Zip code is required")
    .matches(/^\d{5}(-\d{4})?$/, "Zip code is not valid"),
  city_id: Yup.string().required("City is required"),
});
export default function MallList() {
  const {
    data: malls,
    isLoading,
    isFetching,
    isError,
    isSuccess,
  } = useGetMallsQuery("");
  const { data: cities, isLoading: cityLoading } = useGetCiyQuery("");
  const { data: cinemas, isLoading: cinemasLoading } =
    useGetMallCinemasQuery("");
  const [openModal, setOpenModal] = useState(false);
  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);

  const [deleteMall, { isLoading: deleteLoading }] = useDeleteMallMutation();
  const [addMall, { isLoading: AddMallLoading }] = useAddMallMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const handleModalClose = () => {
    setOpenModal(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  // Handler for deleting a mall
  const deleteItem = async () => {
    try {
      console.log("Deleting mall with ID:", itemTobeDeleted?.id);
      await deleteMall(itemTobeDeleted?.id).unwrap();
      Notify("info", "Mall deleted successfully");
      setOpen(false);
    } catch (error) {
      console.error("Error deleting mall:", error);
      Notify("error", "Failed to delete mall");
    }
  };

  const columns = [
    {
      accessorKey: "name",
      header: "Name",
      size: 40,
    },
    {
      accessorKey: "city_id",
      header: "City",
      size: 40,
      Cell: ({ row }: any) => {
        const city = cities?.find((city: { id: any }) => city.id);

        // Debugging output to check what is being matched

        // Return the city name if found, otherwise provide a fallback message
        return city?.name;
      },
    },

    {
      accessorKey: "street",
      header: "Street",
      size: 40,
    },

    {
      accessorKey: "zip_code",
      header: "Zip Code",
      size: 40,
    },
    {
      accessorKey: "created_at",
      size: 40,
      header: "Registration Date",
      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  const [globalFilter, setGlobalFilter] = useState("");
  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return malls; // return all movies if no filter is applied
    return malls?.filter((pr: { name: any }) => {
      const searchContent = `${pr?.name} `.toLowerCase();
      return searchContent?.includes(globalFilter?.toLowerCase());
    });
  }, [malls, globalFilter]);
  return (
    <DefaultPage
      title={"Malls"}
      primaryButtonProps={{
        startIcon: <Add />,
        children: "Add Mall",
        onClick: () => {
          setOpenModal(true);
        },
      }}
    >
      <DataTable
        count={filteredData?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        toolbar={true}
        toolbarActions={true}
        isSuccess={isSuccess}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <Dialog
        open={openModal}
        onClose={handleModalClose}
        maxWidth="md"
        sx={{
          "& .MuiDialog-paper": { width: "50%", maxWidth: "none" },
        }}
      >
        <DialogTitle>Add new mall</DialogTitle>
        <Formik
          initialValues={{
            name: "",
            street: "",
            city_id: "",
            zip_code: "",
          }}
          validationSchema={mallSchema}
          onSubmit={async (values, { setSubmitting }) => {
            try {
              await addMall({
                ...values,
                city: cities.find(
                  (city: { id: string }) => city.id === values.city_id
                )?.name,
              });
              Notify("info", "Mall added Successfully");
            } catch (error) {
              console.error("Error adding mall:", error);
            }
            handleModalClose(); // Close the modal on successful submission
            setSubmitting(false);
          }}
        >
          {({ values, setFieldValue, isSubmitting, touched, errors }: any) => (
            <Form>
              <DialogContent>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Field
                      as={TextField}
                      name="name"
                      label="Mall Name"
                      fullWidth
                      autoFocus
                      margin="dense"
                      error={touched.name && Boolean(errors.name)}
                      helperText={touched.name && errors.name}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Field
                      as={TextField}
                      name="street"
                      label="Street"
                      fullWidth
                      margin="dense"
                      error={touched.street && Boolean(errors.street)}
                      helperText={touched.street && errors.street}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Field
                      as={TextField}
                      name="zip_code"
                      label="Zip Code"
                      fullWidth
                      margin="dense"
                      error={touched.zip_code && Boolean(errors.zip_code)}
                      helperText={touched.zip_code && errors.zip_code}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <FormControl fullWidth margin="dense">
                      <InputLabel id="city-label">City</InputLabel>
                      <Field
                        name="city_id"
                        as={Select}
                        labelId="city-label"
                        label="City"
                        displayEmpty
                        input={<OutlinedInput notched label="City" />}
                        error={touched.city_id && Boolean(errors.city_id)}
                        helperText={touched.city_id && errors.city_id}
                      >
                        {cities?.map((city: any) => (
                          <MenuItem key={city.id} value={city.id}>
                            {city.name}
                          </MenuItem>
                        ))}
                      </Field>
                    </FormControl>
                  </Grid>
                </Grid>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleModalClose} color="primary">
                  Cancel
                </Button>
                <LoadingButton
                  type="submit"
                  color="primary"
                  variant="contained"
                  loading={AddMallLoading}
                >
                  Save
                </LoadingButton>
              </DialogActions>
            </Form>
          )}
        </Formik>
      </Dialog>
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.name}
        itemName={itemTobeDeleted?.name}
      />
    </DefaultPage>
  );
}
