import {
  Avatar,
  Box,
  Chip,
  Container,
  FormControl,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React, { useEffect, useState } from "react";

import LoadingButton from "@mui/lab/LoadingButton";
import CustomizedSnackbars from "../common/SnackBar";
import { useFormik } from "formik";
import { CityValidationsForm } from "./Validation";
import { City } from "../../models/City";
import { addCity } from "../../services/CityService";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import OutlinedInput from "@mui/material/OutlinedInput";
import { Mall } from "../../models/Mall";
import { getMalls } from "../../services/MallService";
import ErrorIcon from "@mui/icons-material/Error";

export default function AddNewCinema() {
  const formik = useFormik({
    initialValues: {
      name: "",
      short_code: "",
      street: "",
      state: "",
      zip_code: "",
      malls: [],
    },
    validationSchema: CityValidationsForm,
    onSubmit: async (values, { resetForm }) => {
      setLoading(true);
      let formData: City = {
        name: values.name,
        short_code: values.short_code,
        street: values.street,
        state: values.state,
        zip_code: values.zip_code,
        malls: values.malls,
      };
      console.log(formData);
      await addCity(formData);
      setOpen(true);
      setLoading(false);
      setMessage("City created successfully.");
      resetForm();
    },
  });
  console.log("formik values: ", formik.values);

  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const [mallSelected, setMallSelected] = useState<boolean>(false);

  const [malls, setMalls] = useState<Mall[]>([]);

  useEffect(() => {
    const fetchMalls = async () => {
      const _malls = await getMalls();
      setMalls(_malls);
    };
    fetchMalls();
  }, []);

  return (
    <Container
      sx={{
        position: "absolute",
        display: "flex",
        flexDirection: "column",
        backgroundColor: "white",
        borderRadius: "15px",
        marginTop: "30px",
        padding: "15px",
        width: "50%",
        height: "370px",
        top: "10%",
        gap: "0px",
        opacity: "0px",
        marginX: "10%",
      }}
    >
      <Box
        sx={{
          marginTop: "20px",
        }}
      >
        <form onSubmit={formik.handleSubmit}>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              gap: "50px",
            }}
          >
            <Box>
              <Grid container spacing={1}>
                <Grid
                  item
                  xs={4}
                  sm={4}
                  md={4}
                  sx={{
                    marginTop: "10px",
                  }}
                >
                  <FormControl>
                    <label
                      htmlFor="id_name"
                      style={{
                        color: "#AFAFAF",
                        marginBottom: "10px",
                      }}
                    >
                      Name
                    </label>
                    <TextField
                      sx={{ width: "80%" }}
                      name="name"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      error={formik.touched.name && Boolean(formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                      onBlur={formik.handleBlur}
                      placeholder={"e.g New York"}
                      variant="outlined"
                      type="text"
                    />
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={4}
                  sm={4}
                  md={4}
                  sx={{
                    marginTop: "10px",
                  }}
                >
                  <FormControl>
                    <label
                      htmlFor="id_city"
                      style={{
                        color: "#AFAFAF",
                        marginBottom: "10px",
                      }}
                    >
                      Short Code
                    </label>
                    <TextField
                      sx={{ width: "80%" }}
                      name="short_code"
                      value={formik.values.short_code}
                      onChange={formik.handleChange}
                      variant="outlined"
                      placeholder={"e.g NY"}
                      type="text"
                    />
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={4}
                  sm={4}
                  md={4}
                  sx={{
                    marginTop: "10px",
                  }}
                >
                  <FormControl>
                    <label
                      htmlFor="id_reason"
                      style={{
                        color: "#AFAFAF",
                        marginBottom: "10px",
                      }}
                    >
                      Street
                    </label>
                    <TextField
                      sx={{
                        width: "80%",
                      }}
                      name="street"
                      value={formik.values.street}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      variant="outlined"
                      placeholder={"e.g 890 Broadway"}
                      type="text"
                    />
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={4}
                  sm={4}
                  md={4}
                  sx={{
                    marginTop: "10px",
                  }}
                >
                  <FormControl>
                    <label
                      htmlFor="id_reason"
                      style={{
                        color: "#AFAFAF",
                        marginBottom: "10px",
                      }}
                    >
                      State
                    </label>
                    <TextField
                      sx={{
                        width: "80%",
                      }}
                      name="state"
                      value={formik.values.state}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      variant="outlined"
                      placeholder={"e.g California"}
                      type="text"
                    />
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={4}
                  sm={4}
                  md={4}
                  sx={{
                    marginTop: "10px",
                  }}
                >
                  <FormControl>
                    <label
                      htmlFor="id_reason"
                      style={{
                        color: "#AFAFAF",
                        marginBottom: "10px",
                      }}
                    >
                      Zip Code
                    </label>
                    <TextField
                      sx={{
                        width: "80%",
                      }}
                      name="zip_code"
                      value={formik.values.zip_code}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      variant="outlined"
                      placeholder={"e.g 1003"}
                      type="text"
                    />
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={4}
                  sm={4}
                  md={4}
                  sx={{
                    marginTop: "10px",
                  }}
                >
                  <FormControl sx={{ width: "100%" }}>
                    <label
                      style={{
                        color: "#AFAFAF",
                        backgroundColor: "white",
                        marginBottom: "10px",
                      }}
                    >
                      Mall
                    </label>
                    <InputLabel
                      id="multiple_malls"
                      style={{
                        color: malls.length === 0 ? "red" : "#AFAFAF",
                        backgroundColor: "white",
                        marginTop: "30px",
                        marginLeft: malls.length > 0 ? "10px" : "10px",
                        marginBottom: "5px",
                      }}
                    >
                      {mallSelected
                        ? malls.length > 0
                          ? ""
                          : "No Available Malls"
                        : "Select Mall"}
                    </InputLabel>
                    <Select
                      sx={{
                        width: "80%",
                      }}
                      id="multiple_malls"
                      labelId="multiple_malls"
                      IconComponent={() =>
                        malls.length > 0 ? (
                          <ExpandMoreIcon />
                        ) : (
                          <ErrorIcon
                            sx={{ color: "red", marginRight: "15px" }}
                          />
                        )
                      }
                      disabled={malls.length === 0}
                      multiple
                      name={"malls"}
                      variant={"outlined"}
                      value={formik.values.malls}
                      onChange={formik.handleChange}
                      input={<OutlinedInput />}
                      renderValue={(selected) => (
                        <div style={{ display: "flex", flexWrap: "wrap" }}>
                          {selected.map((value) => (
                            <Chip
                              key={value}
                              label={
                                malls.find((mall) => mall.id === value)?.name
                              }
                              style={{ margin: 2 }}
                            />
                          ))}
                        </div>
                      )}
                      MenuProps={{
                        PaperProps: {
                          style: {
                            maxHeight: 48 * 4.5 + 8,
                            width: 250,
                          },
                        },
                      }}
                      onOpen={() => setMallSelected(true)}
                    >
                      {malls.map((mall) => (
                        <MenuItem key={mall.id} value={mall.id}>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              gap: "10px",
                            }}
                          >
                            <ListItemText primary={mall.name} />
                          </div>
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
              </Grid>
            </Box>

            <Box
              sx={{
                marginBottom: "20px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
              }}
            >
              <LoadingButton
                loading={loading}
                type="submit"
                variant="contained"
                color="primary"
                className="hover:bg-green-700 text-white px-4 py-2 rounded-md bg-color: #4CAF50;"
                sx={{
                  textTransform: "none",
                  backgroundColor: "#0054D3",
                  width: "358px",
                  height: "49px",
                  padding: "15px",
                  gap: "0px",
                  borderRadius: "55px",
                  opacity: "0px",
                }}
              >
                Add City
              </LoadingButton>
            </Box>
          </Box>
        </form>
      </Box>

      <CustomizedSnackbars message={message} open={open} />
    </Container>
  );
}
