import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";
import React, { useMemo, useState } from "react";
import { Formik, Form, Field } from "formik";
import { Notify } from "../../layouts/notify";
import { LoadingButton } from "@mui/lab";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { useGetCiyQuery } from "./query";
import { DefaultPage } from "../../layouts/default-page";
import { Add, Delete } from "@mui/icons-material";
import { DataTable } from "../../layouts/data-table";
import {
  useAddCityMutation,
  useDeleteCityMutation,
} from "../../services/auth.query";
import { format } from "date-fns";

export default function CityList() {
  const [openModal, setOpenModal] = useState(false);
  const {
    data: cities,
    isLoading,
    isError,
    isSuccess,
    isFetching,
  } = useGetCiyQuery("");

  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);

  const [deleteCityMutation, { isLoading: deleteLoading }] =
    useDeleteCityMutation();
  const [addCity, { isLoading: addCityLoading }] = useAddCityMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteCityMutation(itemTobeDeleted?.id);
    setOpen(false);
  };

  const columns = [
    {
      accessorKey: "name",
      header: "Name",
    },
    {
      accessorKey: "mall_count",
      header: "Mall Count",
    },
    {
      accessorKey: "cinema_count",
      header: "Cinema Count",
    },
    {
      accessorKey: "created_at",
      header: "Registration Date",
      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  const [globalFilter, setGlobalFilter] = useState("");
  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return cities; // return all movies if no filter is applied
    return cities?.filter((pr: { name: any }) => {
      const searchContent = `${pr?.name} `.toLowerCase();
      return searchContent?.includes(globalFilter?.toLowerCase());
    });
  }, [cities, globalFilter]);
  return (
    <DefaultPage
      title="Users"
      loading={isLoading}
      primaryButtonProps={{
        startIcon: <Add />,
        children: "Add  City",
        onClick: () => {
          setOpenModal(true);
        },
      }}
    >
      <DataTable
        count={cities?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        toolbar={true}
        toolbarActions={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.name}
        itemName={itemTobeDeleted?.name}
      />

      <Dialog
        open={openModal}
        onClose={() => setOpenModal(false)}
        maxWidth="md"
        sx={{
          "& .MuiDialog-paper": { width: "30%", maxWidth: "650px" },
        }}
      >
        <DialogTitle>Add New City</DialogTitle>
        <Formik
          initialValues={{ name: "" }}
          onSubmit={async (values, { setSubmitting }) => {
            console.log("Submitting form:", values);
            try {
              await addCity(values);
              Notify("info", "City added Successfully");
            } catch (error) {
              console.error("Error adding city:", error);
            }
            setOpenModal(false); // Close the modal on successful submission
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <DialogContent>
                <Field
                  as={TextField}
                  name="name"
                  label="City Name"
                  fullWidth
                  autoFocus
                  margin="dense"
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={() => setOpenModal(false)} color="primary">
                  Cancel
                </Button>
                <LoadingButton
                  type="submit"
                  color="primary"
                  variant="contained"
                  loading={addCityLoading}
                >
                  Save
                </LoadingButton>
              </DialogActions>
            </Form>
          )}
        </Formik>
      </Dialog>
    </DefaultPage>
  );
}
