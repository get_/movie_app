import { apiSlice } from "../../store/app.api";
import { supabase } from "../../utils/API";
const cityAPi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getCiy: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.from("city").select("*");
        return { data };
      },
      providesTags: ["city"],
    }),
  }),
  overrideExisting: true,
});

export const { useGetCiyQuery } = cityAPi;
