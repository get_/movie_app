import * as yup from "yup";

export const CityValidationsForm =yup.object().shape( {
    name: yup.string().required("Name is Required"),
});
