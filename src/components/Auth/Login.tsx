import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Stack,
  IconButton,
  InputAdornment,
  TextField,
  Checkbox,
  FormControlLabel,
  Container, // Added Container
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { Box } from "@mui/system";
import { Icon } from "@iconify/react";
import { login } from "../../services/AuthService";
import { UserContextType } from "../../models/User";
import { useFormik } from "formik";
import Alert from "@mui/material/Alert";
import UserContext from "../../context/UserContext";

export default function Login() {
  const navigate = useNavigate();
  const emailFromStorage = localStorage.getItem("email") || "";
  const passwordFromStorage = localStorage.getItem("password") || "";
  const rememberFromStorage = localStorage.getItem("remember") === "t" || false;
  const [remember, setRemember] = useState(rememberFromStorage);
  const [loginMessageError, setLoginMessageError] = useState("");

  const { setUser } = useContext<UserContextType | null>(
    UserContext
  ) as UserContextType;

  const [showPassword, setShowPassword] = useState(false);
  const formik = useFormik({
    initialValues: {
      email: emailFromStorage,
      password: passwordFromStorage,
    },
    validate: (values) => {
      const errors: any = {};
      if (!values.email) {
        errors.email = "Email is required";
      }
      if (!values.password) {
        errors.password = "Password is required";
      }
      return errors;
    },
    onSubmit: async (values, { setSubmitting }) => {
      try {
        setSubmitting(true);
        const res = await login(values.email, values.password);
        if (res.success) {
          setUser(res.user?.email as string);
          localStorage.setItem("user", JSON.stringify(res.user, null, 2));
          localStorage.setItem("access_token", res.access_token as string);
          if (remember) {
            localStorage.setItem("email", values.email);
            localStorage.setItem("password", values.password);
            localStorage.setItem("remember", "t");
          } else {
            localStorage.removeItem("email");
            localStorage.removeItem("password");
            localStorage.removeItem("remember");
          }
          navigate("/", { replace: true });
        } else {
          setLoginMessageError(res.message as string);
        }
      } catch (e) {
        setLoginMessageError("Oops, Something went wrong. Please try again.");
      } finally {
        setSubmitting(false);
      }
    },
  });

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100vh",
      }}
    >
      <Container maxWidth="sm">
        <Box
          sx={{
            backgroundColor: "#F6F7FD",
            padding: "30px",
            borderRadius: "10px",
            boxShadow: "0px 4px 20px rgba(0, 0, 0, 0.1)",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        marginBottom: "30px",
      }}
    >
      <img
        src="/logo.png"
        alt="logo icon"
        style={{
          width: "94px",
          height: "94px",
        }}
      />
    </Box>
    {loginMessageError && (
      <Alert
        variant="filled"
        severity="error"
        style={{ marginBottom: "20px", width: "100%" }}
      >
        {loginMessageError}
      </Alert>
    )}
    <form onSubmit={formik.handleSubmit} style={{ width: "100%" }}>
          <Stack spacing={3}>
            <TextField
              type={"email"}
              value={formik.values.email}
              onChange={formik.handleChange}
              name="email"
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
              onBlur={formik.handleBlur}
              label="Email address"
            />
            <TextField
              name="password"
              label="Password"
              type={showPassword ? "text" : "password"}
              value={formik.values.password}
              onChange={formik.handleChange}
              error={formik.touched.password && Boolean(formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
              onBlur={formik.handleBlur}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => setShowPassword(!showPassword)}
                      edge="end"
                    >
                      <Box
                        component={Icon}
                        icon={
                          showPassword ? "eva:eye-fill" : "eva:eye-off-fill"
                        }
                      />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </Stack>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            sx={{ my: 2 }}
          >
            <FormControlLabel
              control={
                <Checkbox
                  name="remember"
                  checked={remember}
                  onChange={(e) => setRemember(e.target.checked)}
                />
              }
              label="Remember me"
            />
          </Stack>
          <LoadingButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            loading={formik.isSubmitting}
          >
            Login
          </LoadingButton>
        </form>
      </Box>
      </Container>
      </Box>
  );
}
