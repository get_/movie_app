import React, { useState, useContext } from "react";
import {
  Box,
  Collapse,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import { ExpandLess, ExpandMore, Logout } from "@mui/icons-material";
import { Link, useLocation, useNavigate } from "react-router-dom";
import UserContext from "../../context/UserContext";
import { UserContextType } from "../../models/User";
import { signOut } from "../../services/AuthService";
import menus from "../../utils/menu";

const LeftSideNav: React.FC = () => {
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const location = useLocation();
  const navigator = useNavigate();
  const [selectedItem, setSelectedItem] = useState("");
  const [openSections, setOpenSections] = useState<string[]>([]);
  const [mobileOpen, setMobileOpen] = useState(false);
  const user = useContext(UserContext) as UserContextType;

  const handleSectionToggle = (section: string) => {
    setOpenSections((prevOpenSections) =>
      prevOpenSections.includes(section)
        ? prevOpenSections.filter((s) => s !== section)
        : [...prevOpenSections, section]
    );
  };

  const handleListItemClick = (itemName: string) => {
    setSelectedItem(itemName);
    if (isSmallScreen) {
      setMobileOpen(false); // Close the drawer on item click in mobile mode
    }
  };

  const isActive = (path: string): boolean => {
    return location.pathname === path;
  };

  const handleLogout = async () => {
    await signOut();
    navigator("/login");
  };

  const drawerWidth = 240;

  return (
    <Drawer
      variant={isSmallScreen ? "temporary" : "permanent"}
      open={isSmallScreen ? mobileOpen : true}
      onClose={() => setMobileOpen(!mobileOpen)}
      ModalProps={{
        keepMounted: true, // Better open performance on mobile.
      }}
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        "& .MuiDrawer-paper": {
          width: drawerWidth,
          boxSizing: "border-box", // Makes sure padding doesn't affect the width
          marginTop: "10px",
          height: "calc(100vh - 75px)", // Updated to take full available height minus 75px
          borderRight: "none",
          borderTopRightRadius: "12px",
          position: "relative",
          transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
          }),
          ml: { sm: 10, md: 20 }, // Horizontal margins added here
        },
      }}
    >
      <Box sx={{ overflow: "auto" }}>
        <List dense={false}>
          {menus.map((menu) => (
            <React.Fragment key={menu.path}>
              <ListItem
                button
                onClick={() => handleSectionToggle(menu.path)}
                selected={isActive(menu.path)}
              >
                <ListItemIcon>{menu.icon}</ListItemIcon>
                <ListItemText>{menu.label} </ListItemText>
                {openSections.includes(menu.path) ? (
                  <ExpandLess />
                ) : (
                  <ExpandMore />
                )}
              </ListItem>
              <Collapse
                in={openSections.includes(menu.path)}
                timeout="auto"
                unmountOnExit
              >
                <List component="div">
                  {menu?.subMenu?.map((subMenu) => (
                    <Link
                      to={subMenu.path}
                      key={subMenu.path}
                      style={{ textDecoration: "none" }}
                    >
                      <ListItemButton
                        selected={isActive(subMenu.path)}
                        onClick={() => handleListItemClick(subMenu.path)}
                      >
                        <ListItemText inset>{subMenu.label} </ListItemText>
                      </ListItemButton>
                    </Link>
                  ))}
                </List>
              </Collapse>
            </React.Fragment>
          ))}

          <ListItem button onClick={handleLogout}>
            <ListItemIcon>
              <Logout />
            </ListItemIcon>
            <ListItemText primary="Logout" />
          </ListItem>
        </List>
      </Box>
    </Drawer>
  );
};

export default LeftSideNav;
