import Box from "@mui/material/Box";
import {
  useDeleteUserMutation,
  useGetUsersQuery,
} from "../../services/user.query";
import { DefaultPage } from "../../layouts/default-page";
import { DataTable } from "../../layouts/data-table";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { Button, Chip } from "@mui/material";
import { Delete } from "@mui/icons-material";
import React, { useMemo, useState } from "react";

export default function User() {
  const {
    data: users,
    isLoading,
    isSuccess,
    isFetching,
    isError,
  } = useGetUsersQuery("");
  /*  */
  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);

  const [deleteDeputyReport, { isLoading: deleteLoading }] =
    useDeleteUserMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteDeputyReport(itemTobeDeleted?.id);
    setOpen(false);
  };
  const columns = [
    {
      accessorKey: "name",
      header: "Name",
      size: 40,
    },

    {
      accessorKey: "email",
      header: "Email",
      size: 40,
    },
    {
      accessorKey: "phone_number",
      header: "phone_number",
      size: 40,
    },
    {
      accessorKey: "role",
      header: "Role",
      size: 40,
    },

    {
      accessorKey: "status",
      header: "status",
      size: 40,
      Cell: ({ row }: any) => {
        const status = row.original.status;
        return (
          <Chip
            label={status}
            variant="filled"
            color={
              status === "VERIFIED"
                ? "success"
                : status === "FAILED"
                ? "warning"
                : "primary"
            }
          />
        );
      },
    },
  ];
  const [globalFilter, setGlobalFilter] = useState("");

  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return users; // return all movies if no filter is applied
    return users?.filter((pr: { name: any; email: any; phone_number: any }) => {
      const searchContent =
        `${pr?.name} ${pr?.email} ${pr?.phone_number}`.toLowerCase();
      return searchContent?.includes(globalFilter?.toLowerCase());
    });
  }, [users, globalFilter]);
  return (
    <DefaultPage title="Deputy Users" loading={isLoading}>
      <DataTable
        count={users?.length}
        columns={columns}
        toolbar={true}
        toolbarActions={true}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.name}
        itemName={itemTobeDeleted?.name}
      />
    </DefaultPage>
  );
}
