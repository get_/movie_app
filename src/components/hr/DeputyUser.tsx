import Box from "@mui/material/Box";
import {
  Button,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  Grid,
  Paper,
  IconButton,
  FormControl,
  TextField,
  Chip,
} from "@mui/material";
import { v4 as uuidv4 } from "uuid";
import React, { ChangeEvent, useMemo, useState, useEffect } from "react";
import {
  addDeputyUser,
  getStorageUrl,
  uploadUserFile,
} from "../../services/UserService";
import { LoadingButton } from "@mui/lab";
import { useFormik } from "formik";
import { Notify } from "../../layouts/notify";
import { Add, Delete } from "@mui/icons-material";
import { SignUp } from "../../services/AuthService";
import { DefaultPage } from "../../layouts/default-page";
import {
  useAddDeputyUserMutation,
  useDeleteUserMutation,
  useGetDeputyUsersQuery,
  useUpdateDeputyUserMutation,
} from "../../services/user.query";
import { DataTable } from "../../layouts/data-table";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import * as Yup from "yup";
import { current } from "@reduxjs/toolkit";
import { upload } from "../../services/Upload";

const validationSchema = Yup.object({
  name: Yup.string().required("Name is required"),
  email: Yup.string()
    .email("Invalid email address")
    .required("Email is required"),
  password: Yup.string()
    .min(6, "Password must be at least 8 characters long")
    .required("Password is required"),
  phone_number: Yup.string()
    .matches(/^[0-9]+$/, "Phone number must be only digits")
    .min(10, "Phone number must be at least 10 digits long")
    .max(15, "Phone number must not exceed 15 digits"),
  profile_picture: Yup.mixed().required("Profile picture is required"),
});

export default function DeputyUser() {
  const {
    data: deputyUsers,
    isLoading,
    isFetching,
    isError,
    isSuccess,
  } = useGetDeputyUsersQuery("");
  const [addDeputyUser, { isLoading: addDeputyUserLoading }] =
    useAddDeputyUserMutation();
  const [updateDeputyuser, { isLoading: updating }] =
    useUpdateDeputyUserMutation();
  const [avatarImage, setAvatarImage] = useState<any>(null);
  const [open, setOpen] = useState<boolean>(false);
  const [itemToBeDeleted, setItemToBeDeleted] = useState<any>(null);
  const [openAddModal, setOpenAddModal] = useState<boolean>(false);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [currentUser, setCurrentUser] = useState<any>(null);

  const handleAvatarImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.currentTarget.files) {
      setAvatarImage(e.target.files[0]);
      formik.setFieldValue("profile_picture", e.currentTarget.files[0]);
      formik.setErrors({ profile_picture: "" });
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemToBeDeleted(row);
  };

  const deleteItem = async () => {
    await deleteDeputyReport(itemToBeDeleted?.id);
    setOpen(false);
  };

  const openAddEditModal = (user: any = null) => {
    if (user) {
      setEditMode(true);
      setCurrentUser(user);
      formik.setValues({
        email: user.email,
        password: user.password,
        phone_number: user.phone_number,
        profile_picture: user.profile_picture,
        name: user.name,
      });
    } else {
      setEditMode(false);
      formik.resetForm();
    }
    setOpenAddModal(true);
  };

  const closeAddEditModal = () => {
    setOpenAddModal(false);
    setCurrentUser(null);
    setAvatarImage(null);
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      phone_number: "",
      profile_picture: null,
      name: "",
    },
    validationSchema: validationSchema,
    onSubmit: async (values: any) => {
      try {
        if (editMode) {
          // Handle updating user
          const updatedUser = {
            ...values,
            status: "VERIFIED",
            role: "DEPUTY",
            email: values.email,
            phone_number: values.phone_number,
            gov_issued_ID_url: null,
            name: values.name,
            id: currentUser?.id,
            profile_picture: avatarImage
              ? (await handleImageUpload(avatarImage)).url
              : currentUser.profile_picture,
          };
          await updateDeputyuser(updatedUser);
          Notify("info", "User updated Successfully");
        } else {
          // Handle adding new user
          const signUpResponse = await SignUp(values.email, values.password);
          console.log("SignUp Response:", signUpResponse);

          if (signUpResponse && avatarImage) {
            const imageUploadResponse = await handleImageUpload(avatarImage);
            if (imageUploadResponse) {
              const addDeputyUserResponse = await addDeputyUser({
                ...values,
                status: "VERIFIED",
                role: "DEPUTY",
                email: values.email,
                profile_picture: imageUploadResponse.url,
                phone_number: values.phone_number,
                user_id: signUpResponse.user.id,
                gov_issued_ID_url: null,
                name: values.name,
              });
              console.log("Deputy User Added:", addDeputyUserResponse);
              Notify("info", "User added Successfully");
            }
          }
        }
        setOpenAddModal(false);
      } catch (error) {
        console.error("Error during form submission:", error);
        Notify("error", "Failed to add user:");
      }
    },
  });

  const handleImageUpload = async (file: File) => {
    const fileExt = file.name.split(".").pop();
    const filePath = `${uuidv4()}-${Math.random()}.${fileExt}`;
    return await upload(file, "images", filePath);
  };

  const columns = [
    {
      accessorKey: "name",
      header: "Name",
      size: 40,
    },
    {
      accessorKey: "email",
      header: "Email",
      size: 40,
    },
    {
      accessorKey: "phone_number",
      header: "Phone Number",
      size: 40,
    },
    {
      accessorKey: "role",
      header: "Role",
      size: 40,
    },
    {
      accessorKey: "status",
      header: "Status",
      size: 40,
      Cell: ({ row }: any) => {
        const status = row.original.status;
        return (
          <Chip
            label={status}
            variant="filled"
            color={status === "VERIFIED" ? "success" : "primary"}
          />
        );
      },
    },
  ];

  const [deleteDeputyReport, { isLoading: deleteLoading }] =
    useDeleteUserMutation();
  const [globalFilter, setGlobalFilter] = useState("");

  const filteredData = useMemo(() => {
    if (!globalFilter) return deputyUsers;
    return deputyUsers?.filter(
      (pr: { name: any; email: any; phone_number: any }) => {
        const searchContent =
          `${pr?.name} ${pr?.email} ${pr?.phone_number}`.toLowerCase();
        return searchContent?.includes(globalFilter?.toLowerCase());
      }
    );
  }, [deputyUsers, globalFilter]);

  return (
    <DefaultPage
      title="Deputy Users"
      loading={isLoading}
      primaryButtonProps={{
        startIcon: <Add />,
        children: "Add Deputy Users",
        onClick: () => openAddEditModal(),
      }}
    >
      <DataTable
        count={deputyUsers?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        toolbar={true}
        toolbarActions={true}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
            <Button onClick={() => openAddEditModal(row?.original)}>
              Edit
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemToBeDeleted?.name}
        itemName={itemToBeDeleted?.name}
      />
      <Dialog
        open={openAddModal}
        onClose={closeAddEditModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <Paper
          elevation={0}
          sx={{
            backgroundColor: "white",
            width: "100%",
            borderRadius: "15px",
            padding: "15px",
          }}
        >
          <IconButton
            aria-label="close"
            onClick={closeAddEditModal}
            sx={{
              position: "absolute",
              top: 0,
              right: 0,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <img alt={"close icon"} src={"/x.png"} />
          </IconButton>
          <Typography variant="subtitle1" component="div">
            {editMode ? "Edit Deputy User" : "Add Deputy User"}
          </Typography>
          <form noValidate onSubmit={formik.handleSubmit}>
            <DialogContent sx={{ width: "100%", height: "100%" }}>
              <FormControl fullWidth>
                <label
                  htmlFor="name"
                  style={{
                    color: "#AFAFAF",
                    marginBottom: "7px",
                  }}
                >
                  Name
                </label>
                <TextField
                  sx={{
                    width: "100%",
                    margin: "0px",
                    padding: "0px",
                  }}
                  name="name"
                  id="name"
                  variant="outlined"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  type="text"
                  onBlur={formik.handleBlur}
                  error={formik.touched.name && Boolean(formik.errors.name)}
                  helperText={
                    formik.touched.name && (formik.errors.name as any)
                  }
                />
              </FormControl>
              <FormControl fullWidth>
                <label
                  htmlFor="email"
                  style={{
                    color: "#AFAFAF",
                    marginBottom: "7px",
                  }}
                >
                  Email
                </label>
                <TextField
                  sx={{
                    width: "100%",
                    margin: "0px",
                    padding: "0px",
                  }}
                  name="email"
                  id="email"
                  variant="outlined"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  type="email"
                  onBlur={formik.handleBlur}
                  error={formik.touched.email && Boolean(formik.errors.email)}
                  helperText={
                    formik.touched.email && (formik.errors.email as any)
                  }
                  disabled={editMode}
                />
              </FormControl>
              <FormControl fullWidth>
                <label
                  htmlFor="password"
                  style={{
                    color: "#AFAFAF",
                    marginBottom: "7px",
                  }}
                >
                  Password
                </label>
                <TextField
                  sx={{
                    width: "100%",
                    margin: "0px",
                    padding: "0px",
                  }}
                  name="password"
                  id="password"
                  variant="outlined"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  type="password"
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={
                    formik.touched.password && (formik.errors.password as any)
                  }
                  disabled={editMode}
                />
              </FormControl>
              <FormControl fullWidth>
                <label
                  htmlFor="phone_number"
                  style={{
                    color: "#AFAFAF",
                    marginBottom: "7px",
                  }}
                >
                  Phone Number
                </label>
                <TextField
                  sx={{
                    width: "100%",
                    margin: "0px",
                    padding: "0px",
                  }}
                  name="phone_number"
                  id="phone_number"
                  variant="outlined"
                  value={formik.values.phone_number}
                  onChange={formik.handleChange}
                  type="text"
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.phone_number &&
                    Boolean(formik.errors.phone_number)
                  }
                  helperText={
                    formik.touched.phone_number &&
                    (formik.errors.phone_number as any)
                  }
                />
              </FormControl>
              <FormControl fullWidth>
                <label
                  htmlFor="profile_picture"
                  style={{
                    color: "#AFAFAF",
                    marginBottom: "7px",
                  }}
                >
                  Profile Picture
                </label>
                <Grid item xs={12} sm={2} md={2}>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      bgcolor: "#DEECFF",
                      width: { xs: "100%" },
                      height: { xs: "100px" },
                      borderRadius: "20px",
                    }}
                  >
                    <label htmlFor={"contained-file"}>
                      <Box
                        sx={{
                          width: "100%",
                          height: "100%",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          alignContent: "center",
                        }}
                      >
                        <img
                          alt={"upload icon"}
                          src={"/upload.png"}
                          style={{
                            position: "absolute",
                          }}
                        />
                      </Box>
                    </label>
                    <input
                      accept={"image/*"}
                      style={{ display: "none" }}
                      id="contained-file"
                      type="file"
                      multiple
                      name={"avatar_file"}
                      onChange={handleAvatarImageChange}
                      onBlur={formik.handleBlur}
                    />
                  </Box>
                  {avatarImage && <span>{avatarImage.name}</span>}
                  {formik.errors.profile_picture && (
                    <span style={{ color: "red", fontSize: "16px" }}>
                      {formik.errors.profile_picture as any}
                    </span>
                  )}
                  <img
                    src={
                      avatarImage
                        ? URL.createObjectURL(avatarImage as any)
                        : (formik.values.profile_picture as any)
                    }
                    alt="Profile"
                    style={{ width: "50%", height: "30%" }}
                  />
                </Grid>
              </FormControl>
            </DialogContent>
            <DialogActions>
              <Grid container spacing={2}>
                <Grid item xs={1} sm={1} md={1} />
                <Grid item xs={10} sm={10} md={10}>
                  <LoadingButton
                    color="primary"
                    type={"submit"}
                    sx={{
                      textTransform: "none",
                      color: "white",
                      width: "100%",
                      borderRadius: "20px",
                      "&:hover": {
                        backgroundColor: "#5489de",
                        color: "white",
                      },
                      margin: 1,
                      backgroundColor: "#0054D3",
                    }}
                    loading={
                      addDeputyUserLoading || updating || formik.isSubmitting
                    }
                  >
                    Save{" "}
                  </LoadingButton>
                </Grid>
                <Grid item xs={1} sm={1} md={1} />
              </Grid>
            </DialogActions>
          </form>
        </Paper>
      </Dialog>
    </DefaultPage>
  );
}
