import Box from "@mui/material/Box";
import {
  Avatar,
  Button,
  Chip,
  Dialog,
  IconButton,
  Tooltip,
} from "@mui/material";
import React, { useMemo, useState } from "react";
import FullscreenIcon from "@mui/icons-material/Fullscreen";
import CloseIcon from "@mui/icons-material/Close";
import {
  useApproveUserMutation,
  useDeclineUserMutation,
  useGetUnVerifiedDeputyUsersQuery,
} from "../../services/user.query";
import { DefaultPage } from "../../layouts/default-page";
import { DataTable } from "../../layouts/data-table";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { Notify } from "../../layouts/notify";
import { useNavigate } from "react-router";

export default function VerificationCenter() {
  const {
    data: users,
    isLoading,
    isFetching,
    isSuccess,
    isError,
  } = useGetUnVerifiedDeputyUsersQuery("");
  const [openFullScreen, setOpenFullScreen] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const handleOpenFullScreen = (imageUrl: string) => {
    setSelectedImage(imageUrl);
    setOpenFullScreen(true);
  };

  const handleCloseFullScreen = () => {
    setOpenFullScreen(false);
  };

  const [open, setOpen] = React.useState<boolean>(false);
  const [itemToBeDecline, setItemToBeDecline] = React.useState<any>(null);
  const navigate = useNavigate();
  const [declineUser, { isLoading: declining }] = useDeclineUserMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemToBeDecline(row);
  };
  console.log("itemToBeDecline", itemToBeDecline);
  const declineItem = async () => {
    try {
      await declineUser(itemToBeDecline?.id);

      Notify("info", "Decline Successfully");
      navigate("/hr/user");
      setOpen(false);
    } catch (err) {}
    setOpen(false);
    Notify("error", "error happened while declining");
  };

  const [openApprove, setOpenApprove] = React.useState<boolean>(false);
  const [itemTOBeApproved, setItemTobeApproved] = React.useState<any>(null);

  const [approveUser, { isLoading: approving }] = useApproveUserMutation();
  const handleCloseApprove = () => {
    setOpenApprove(false);
  };
  const openApproveModal = (row: any) => {
    setOpenApprove(true);
    setItemTobeApproved(row);
  };
  const approveItem = async () => {
    try {
      await approveUser(itemTOBeApproved?.id);
      Notify("info", "Verified Successfully");
      setOpenApprove(false);
      navigate("/hr/user");
    } catch (err) {
      setOpenApprove(false);
      Notify("error", "Error Happened while Approving");
    }
  };
  const columns = [
    {
      accessorKey: "avatar_img",
      header: "Government Issued Url",
      size: 100,
      Cell: ({ row }: any) => {
        const imageUrl =
          row.original.gov_issued_ID_url || row.original.profile_picture;
        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
            <Avatar
              src={imageUrl}
              sx={{ width: 60, height: 60, cursor: "pointer" }}
              onClick={() => handleOpenFullScreen(imageUrl)}
            />
            {imageUrl && (
              <Tooltip
                title="View Full Screen"
                enterDelay={500}
                leaveDelay={200}
              >
                <IconButton onClick={() => handleOpenFullScreen(imageUrl)}>
                  <FullscreenIcon />
                </IconButton>
              </Tooltip>
            )}
          </Box>
        );
      },
    },

    {
      accessorKey: "email",
      header: "Email",
      size: 40,
    },
    {
      accessorKey: "phone_number",
      header: "phone_number",
      size: 40,
    },
    {
      accessorKey: "role",
      header: "Role",
      size: 40,
    },
    {
      accessorKey: "status",
      header: "status",
      size: 40,
      Cell: ({ row }: any) => {
        const status = row.original.status;
        return (
          <Chip
            label={status}
            variant="filled"
            color={status === "VERIFIED" ? "success" : "primary"}
          />
        );
      },
    },
  ];
  const [globalFilter, setGlobalFilter] = useState("");

  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return users; // return all movies if no filter is applied
    return users?.filter((pr: { name: any; email: any; phone_number: any }) => {
      const searchContent =
        `${pr?.name} ${pr?.email} ${pr?.phone_number}`.toLowerCase();
      return searchContent?.includes(globalFilter?.toLowerCase());
    });
  }, [users, globalFilter]);
  return (
    <DefaultPage title="Verification Center" loading={isLoading}>
      <DataTable
        count={users?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        toolbar={true}
        toolbarActions={true}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex", gap: 2 }}>
            <Button
              color="success"
              variant="contained"
              onClick={() => openApproveModal(row?.original)}
            >
              Approve
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              Decline
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={declineItem}
        loading={declining}
        confirmMessage="Are You Sure to Decline ?"
        item={itemToBeDecline?.email}
        itemName={itemToBeDecline?.email}
      />
      <DeleteConfirmationDialog
        open={openApprove}
        onClose={handleCloseApprove}
        onConfirm={approveItem}
        loading={approving}
        confirmMessage="Are You Sure to Approve?"
        item={itemTOBeApproved?.email}
        itemName={itemTOBeApproved?.email}
      />
      <Dialog
        fullScreen
        open={openFullScreen}
        onClose={handleCloseFullScreen}
        aria-labelledby="full-screen-dialog-title"
      >
        <IconButton
          edge="start"
          color="inherit"
          onClick={handleCloseFullScreen}
          aria-label="close"
          sx={{ position: "absolute", right: 8, top: 8, color: "white" }}
        >
          <CloseIcon />
        </IconButton>
        <Box
          sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "rgba(0, 0, 0, 0.9)",
          }}
        >
          <img
            src={selectedImage}
            alt="Full Screen Content"
            style={{ maxWidth: "100%", maxHeight: "100%" }}
          />
        </Box>
      </Dialog>
    </DefaultPage>
  );
}
