import * as yup from "yup";

export const CinemaValidationsForm =yup.object().shape( {
    name: yup.string().required("Name is Required"),
});
export const CinemaMovieValidationsForm =yup.object().shape( {
    movie_id: yup.string().required("Title is Required"),
});
