import { Container, FormControl, TextField } from "@mui/material";
import React from "react";

import LoadingButton from "@mui/lab/LoadingButton";
import CustomizedSnackbars from "../common/SnackBar";
import { useFormik } from "formik";
import { CinemaValidationsForm } from "./Validation";
import { Cinema } from "../../models/Cinema";
import { addCinema } from "../../services/CinemaService";

export default function AddNewCinema() {
  const formik = useFormik({
    initialValues: {
      name: "",
    },
    validationSchema: CinemaValidationsForm,
    onSubmit: async (values, { resetForm }) => {
      setLoading(true);
      let formData: Cinema = {
        name: values.name,
      };
      await addCinema(formData);
      setOpen(true);
      setLoading(false);
      setMessage("Cinema created successfully.");
      resetForm();
    },
  });

  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState<boolean>(false);
  const [message, setMessage] = React.useState<string>("");

  return (
    <Container
      sx={{
        position: "absolute",
        backgroundColor: "white",
        borderRadius: "15px",
        marginTop: "30px",
        width: "50%",
        height: "200px",
        top: "146px",
        marginX: "10%",
        padding: "15px",
      }}
    >
      <form
        onSubmit={formik.handleSubmit}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          alignContent: "center",
          gap: "15px",
        }}
      >
        <FormControl>
          <label
            htmlFor="id_reason"
            style={{
              color: "#AFAFAF",
              marginBottom: "10px",
            }}
          >
            Name
          </label>
          <TextField
            sx={{ width: "358px" }}
            name="name"
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.touched.name && formik.errors.name}
            onBlur={formik.handleBlur}
            placeholder={"e.g Majestic Cinemas"}
            variant="outlined"
            type="text"
          />
        </FormControl>
        <LoadingButton
          loading={loading}
          type="submit"
          variant="contained"
          color="primary"
          className="hover:bg-green-700 text-white px-4 py-2 rounded-md bg-color: #4CAF50;"
          sx={{
            textTransform: "none",
            backgroundColor: "#0054D3",
            width: "358px",
            height: "49px",
            padding: "15px",
            gap: "0px",
            borderRadius: "55px",
            opacity: "0px",
          }}
        >
          Add Cinema
        </LoadingButton>
      </form>
      <CustomizedSnackbars message={message} open={open} />
    </Container>
  );
}
