import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  OutlinedInput,
  Box,
} from "@mui/material";
import React, { useMemo, useState } from "react";
import { Formik, Form, Field } from "formik";
import { LoadingButton } from "@mui/lab";
import { Notify } from "../../layouts/notify";
import Add from "@mui/icons-material/Add";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { Delete } from "@mui/icons-material";
import { format } from "date-fns";
import { DefaultPage } from "../../layouts/default-page";
import { DataTable } from "../../layouts/data-table";
import {
  useAddCinemaMutation,
  useDeleteCinemaMutation,
  useGetCinemasQuery,
} from "../../services/cinema.query";
import { v4 as uuidv4 } from "uuid";

import {
  useGetMallCinemasQuery,
  useGetMallsQuery,
} from "../../services/mall.query";
import { upload } from "../../services/Upload";

export default function CinemaList() {
  const [openModal, setOpenModal] = useState(false);
  const {
    data: cinemas,
    isLoading,
    isError,
    isSuccess,
    isFetching,
  } = useGetCinemasQuery("");
  const { data: malls } = useGetMallsQuery("");
  const [addCinema, { isLoading: addCinemaLoading }] = useAddCinemaMutation();

  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);

  const [deleteCinema, { isLoading: deleteLoading }] =
    useDeleteCinemaMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    try {
      await deleteCinema(itemTobeDeleted?.id);
      setOpen(false);
      Notify("success", "Item deleted successfully");
    } catch (error) {
      Notify("error", "Failed to delete item");
    }
  };

  const columns = [
    {
      accessorKey: "qr_value",
      header: "QR Code",
      size: 40,
      Cell: ({ row }: any) => {
        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
            <img
              src={row.original.qr_value}
              style={{ width: 50, height: 50, cursor: "pointer" }}
              onClick={() => window.open(row.original.qr_value, "_blank")}
            />
          </Box>
        );
      },
    },
    {
      accessorKey: "cinema_img",
      header: "Cinema Image",
      size: 40,
      Cell: ({ row }: any) => {
        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
            <img
              src={row.original.cinema_img}
              style={{ width: 50, height: 50, cursor: "pointer" }}
              onClick={() => window.open(row.original.cinema_img, "_blank")}
            />
          </Box>
        );
      },
    },
    {
      accessorKey: "name",
      header: "Name",
    },

    {
      accessorKey: "mall_id",
      header: "Mall",
      Cell: ({ row }: any) => {
        const mall = malls?.find((m: { id: any }) => m.id);
        return mall?.name;
      },
    },

    {
      accessorKey: "created_at",
      header: "Registration Date",
      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  const [globalFilter, setGlobalFilter] = useState("");
  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return cinemas; // return all movies if no filter is applied
    return cinemas?.filter((pr: { name: any }) => {
      const searchContent = `${pr?.name} `.toLowerCase();
      return searchContent?.includes(globalFilter?.toLowerCase());
    });
  }, [cinemas, globalFilter]);

  return (
    <DefaultPage
      title={"Cinemas"}
      primaryButtonProps={{
        startIcon: <Add />,
        children: "Add  Cinemas",
        onClick: () => {
          setOpenModal(true);
        },
      }}
    >
      <DataTable
        count={cinemas?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        enableBottomToolbar={true}
        toolbar={true}
        toolbarActions={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.name}
        itemName={itemTobeDeleted?.name}
      />

      <Dialog
        open={openModal}
        onClose={() => setOpenModal(false)}
        maxWidth="md"
        sx={{
          "& .MuiDialog-paper": { width: "50%", maxWidth: "none" },
        }}
      >
        <DialogTitle>Add new cinema</DialogTitle>
        <Formik
          initialValues={{
            name: "",
            mall_id: "",
            cinema_img: null,
          }}
          onSubmit={async (values: any, { setSubmitting }: any) => {
            try {
              let formData: any = {
                name: values?.name,
                mall_id: values?.mall_id,
                cinema_img: "",
              };
              if (values.cinema_img) {
                const file = values?.cinema_img as any;
                const fileExt = values?.cinema_img?.name?.split(".").pop();
                const filePath = `${uuidv4()}-${Math.random()}.${fileExt}`;
                let data = await upload(file!, "images", filePath);
                formData.cinema_img = data.url;
              }
              console.log("form data", formData);
              await addCinema(formData);
              Notify("info", "Cinema added successfully");
              setOpenModal(false);
            } catch (error) {
              console.error("Error adding cinema:", error);
              Notify("error", "Failed to add cinema");
            } finally {
              setSubmitting(false);
            }
          }}
        >
          {({ values, setFieldValue, isSubmitting }: any) => (
            <Form>
              <DialogContent>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Field
                      as={TextField}
                      name="name"
                      label="Cinema Name"
                      fullWidth
                      autoFocus
                      margin="dense"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <input
                      accept="image/*"
                      type="file"
                      onChange={(event: any) => {
                        setFieldValue(
                          "cinema_img",
                          event?.currentTarget?.files[0]
                        );
                      }}
                      style={{ width: "100%", padding: "10px 0" }}
                    />
                  </Grid>

                  <Grid item xs={12} sm={12}>
                    <FormControl fullWidth margin="dense">
                      <InputLabel id="city-label">Mall</InputLabel>
                      <Field
                        name="mall_id"
                        as={Select}
                        labelId="city-label"
                        label="Mall"
                        displayEmpty
                        input={<OutlinedInput notched label="City" />}
                      >
                        {malls.map((mall: any) => (
                          <MenuItem key={mall.id} value={mall.id}>
                            {mall.name}
                          </MenuItem>
                        ))}
                      </Field>
                    </FormControl>
                  </Grid>
                </Grid>
              </DialogContent>
              <DialogActions>
                <Button onClick={() => setOpenModal(false)} color="primary">
                  Cancel
                </Button>
                <LoadingButton
                  type="submit"
                  color="primary"
                  variant="contained"
                  loading={addCinemaLoading}
                >
                  Save
                </LoadingButton>
              </DialogActions>
            </Form>
          )}
        </Formik>
      </Dialog>
    </DefaultPage>
  );
}
