import Box from "@mui/material/Box";
import {
  Button,
  Card,
  CardActionArea,
  CardContent,
  Container,
  FormControl,
  Grid,
  IconButton,
  List,
  ListItem,
  MenuItem,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import {
  getCategory,
  getMovieReviewers,
  getMovies,
} from "../../services/MovieService";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { useLocation } from "react-router";
import { useFormik } from "formik";
import {
  addCinemaMovie,
  deleteCinemaMovie,
} from "../../services/CinemaService";
import { Movie } from "../../models/Movie";
import { CinemaMovie as CinemaItemInfo } from "../../models/Cinema";
import { createSignInUrl } from "../../services/Upload";
import { CinemaMovieValidationsForm } from "./Validation";
const CinemaItem = (movie: Movie) => {
  let { state } = useLocation();

  const handleUnlinkCinemaMovie = async () => {
    await deleteCinemaMovie(state.id, movie.id as number);
    window.location.reload();
  };
  return (
    <Paper
      elevation={0}
      style={{
        width: "800px",
        borderRadius: "15px",
        marginTop: "30px",
        padding: "30px",
      }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={2} md={2}>
          <Box
            sx={{
              width: "123px",
              position: "relative",
              height: "180px",
              borderRadius: "8px",
            }}
          >
            <img
              alt={movie.title}
              src={movie.avatar_img}
              style={{
                position: "absolute",
                width: "123px",
                height: "180px",
                borderRadius: "8px",
              }}
            />
          </Box>
        </Grid>
        <Grid item xs={12} sm={7} md={7}>
          <Box
            sx={{
              marginLeft: "15px",
              marginTop: "20px",
            }}
          >
            <Typography
              variant="subtitle1"
              component="div"
              sx={{
                color: "#AFAFAF",
                width: "29px",
                height: "19px",
              }}
            >
              Rating
            </Typography>
            <Typography
              component="div"
              sx={{
                color: "black",
                width: "200px",
                overflow: "hidden",
                textOverflow: "ellipsis",
                fontSize: "16px",
                lineHeight: "19.09px",
                marginTop: "10px",
              }}
            >
              {movie.category_name}
            </Typography>
          </Box>
        </Grid>
        <Grid item xs={12} sm={3} md={3}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              flexDirection: "column",
              alignItems: "end",
              marginRight: "20px",
              marginTop: "20px",
            }}
          >
            <Button
              sx={{
                width: "74px",
                height: "39px",
                textTransform: "none",
                backgroundColor: "#797E8B",
                color: "white",
                marginLeft: "10px",
                borderRadius: "25px",
                "&:hover": {
                  backgroundColor: "gray.100",
                  color: "black",
                },
              }}
              onClick={() => handleUnlinkCinemaMovie()}
            >
              Delete
            </Button>
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              flexDirection: "column",
              alignItems: "start",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "10px",
              }}
            >
              <Typography
                variant="subtitle1"
                component="div"
                sx={{
                  color: "#AFAFAF",
                  width: "100%",
                  height: "19px",
                  marginTop: "20px",
                }}
              >
                Movie Name
              </Typography>
              <Typography
                component="div"
                sx={{
                  color: "black",
                  width: "200px",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  fontSize: "16px",
                  lineHeight: "19.09px",
                  marginTop: "10px",
                }}
              >
                {movie.title}
              </Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "10px",
              }}
            >
              <Typography
                variant="subtitle1"
                component="div"
                sx={{
                  color: "#AFAFAF",
                  width: "100%",
                  height: "19px",
                  marginTop: "20px",
                }}
              >
                Movie Date
              </Typography>
              <Typography
                component="div"
                sx={{
                  color: "black",
                  width: "200px",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  fontSize: "16px",
                  lineHeight: "19.09px",
                  marginTop: "10px",
                }}
              >
                {movie.release_date?.toString()}
              </Typography>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default function CinemaMovie() {
  let { state } = useLocation();
  let linkedMovies: Movie[] = state.movies || [];

  const [open, setOpen] = React.useState<boolean>(false);
  const [movies, setMovies] = useState<Movie[]>([]);

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const fetchMovies = async () => {
      let movies: Movie[] = await getMovies();
      movies = movies.filter(
        (movie) =>
          !linkedMovies.some((linkedMovie) => linkedMovie.id === movie.id)
      );
      for (let i = 0; i < movies.length; i++) {
        let category = await getCategory(movies[i].category as number);

        const movie = movies[i];
        movie.category_name = category;
        let committees = movie.committees?.map((id) => Number(id));
        movie.committee_members = await getMovieReviewers(committees);
        if (movie.avatar_img) {
          movie.avatar_img = await createSignInUrl(movie.avatar_img, "movie");
        }
        if (movie.cover_img) {
          movie.cover_img = await createSignInUrl(movie.cover_img, "movie");
        }
      }

      setMovies(movies);
    };
    fetchMovies();
  }, []);

  const formik = useFormik({
    initialValues: {
      movie_id: "",
      cinema_id: state.id,
    },
    validationSchema: CinemaMovieValidationsForm,
    onSubmit: async (values, { resetForm }) => {
      let data: CinemaItemInfo = {
        movie_id: Number(values.movie_id),
        cinema_id: Number(values.cinema_id),
      };
      await addCinemaMovie(data);
      resetForm();
      setOpen(false);
      window.location.reload();
    },
  });
  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "100%",
        gap: "30px",
        marginX: "10%",
        padding: "15px",
      }}
    >
      <Card
        elevation={0}
        sx={{
          width: "100%",
          height: "100px",
          gap: "0px",
          borderRadius: "15px",
          opacity: "0px",
          backgroundColor: "#DAEBFF",
          display: "flex",
          flexDirection: "row",
          alignContent: "center",
          alignItems: "center",
          marginX: "10%",
          justifyContent: "center",
        }}
      >
        <CardActionArea
          sx={{
            width: "850px",
            height: "100px",
            margin: "0px",
          }}
        >
          <CardContent
            sx={{
              width: "850px",
              height: "55px",
              margin: "0px",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              alignContent: "center",
            }}
            onClick={() => setOpen(true)}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                width: "100%",
                gap: "33px",
                alignContent: "center",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <img alt={"plus icon"} src={"plus.png"} />
              <Typography
                sx={{
                  width: "100%",
                  height: "19px",
                  gap: "10px",
                  opacity: "0px",
                  color: "#0054D3",
                  fontFamily: "SF Pro Display",
                  fontSize: "16px",
                  fontWeight: 400,
                  lineHeight: "19.09px",
                  textAlign: "left",
                }}
              >
                Add new movie
              </Typography>
            </Box>
          </CardContent>
        </CardActionArea>
      </Card>

      <List>
        {linkedMovies.length === 0 ? (
          <ListItem>No Linked Movie Found</ListItem>
        ) : (
          linkedMovies.map((movie) => {
            return <CinemaItem key={movie.id} {...movie} />;
          })
        )}
      </List>

      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <Paper
            elevation={0}
            sx={{
              backgroundColor: "white",
              height: "200px",
              width: "400px",
              borderRadius: "15px",
              padding: "15px",
            }}
          >
            <IconButton
              aria-label="close"
              onClick={() => handleClose()}
              sx={{
                position: "absolute",
                top: 0,
                right: 0,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <img alt={"close icon"} src={"x.png"} />
            </IconButton>
            <form noValidate onSubmit={formik.handleSubmit}>
              <DialogContent>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "15px",
                  }}
                >
                  {movies.length === 0 ? (
                    <Typography align={"center"} color={"red"}>
                      Movie Not Available.
                    </Typography>
                  ) : (
                    <FormControl>
                      <label
                        style={{
                          color: "#AFAFAF",
                          marginBottom: "7px",
                        }}
                      >
                        Movie
                      </label>

                      <TextField
                        sx={{
                          width: "100%",
                          height: "auto",
                        }}
                        id="id_movie"
                        select
                        name="movie_id"
                        variant="outlined"
                        SelectProps={{
                          IconComponent: ExpandMoreIcon,
                        }}
                        value={formik.values.movie_id}
                        onChange={formik.handleChange}
                        error={
                          formik.touched.movie_id &&
                          Boolean(formik.errors.movie_id)
                        }
                        helperText={
                          formik.touched.movie_id && formik.errors.movie_id
                        }
                        onBlur={formik.handleBlur}
                      >
                        {movies.map((movie) => (
                          <MenuItem key={movie.id} value={movie.id}>
                            {movie.title}
                          </MenuItem>
                        ))}
                      </TextField>
                    </FormControl>
                  )}
                </div>
              </DialogContent>
              <DialogActions>
                <Grid container spacing={2}>
                  <Grid item xs={1} sm={1} md={1} />

                  <Grid item xs={10} sm={10} md={10}>
                    <Button
                      color="primary"
                      type={"submit"}
                      disabled={movies.length === 0}
                      sx={{
                        textTransform: "none",
                        color: "white",
                        width: "100%",
                        borderRadius: "20px",
                        "&:hover": {
                          backgroundColor: "#5489de",
                          color: "white",
                        },
                        margin: 1,
                        backgroundColor:
                          movies.length === 0 ? "gray" : "#0054D3",
                      }}
                    >
                      Add Movie
                    </Button>
                  </Grid>
                  <Grid item xs={1} sm={1} md={1} />
                </Grid>
              </DialogActions>
            </form>
          </Paper>
        </Dialog>
      </div>
    </Container>
  );
}
