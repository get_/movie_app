import React, { useMemo, useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  TextField,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Box,
} from "@mui/material";
import { Formik, Form, Field, FieldArray } from "formik";
import { LoadingButton } from "@mui/lab";
import Add from "@mui/icons-material/Add";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { Delete } from "@mui/icons-material";
import { DataTable } from "../../layouts/data-table";
import { DefaultPage } from "../../layouts/default-page";
import * as Yup from "yup";
import {
  useAddMovieScheduleMutation,
  useDeleteMovieMutation,
  useDeleteMovieScheduleMutation,
  useGetMovieScheduleQuery,
  useGetMoviesQuery,
} from "../../services/movies.query";
import { useGetCinemasQuery } from "../../services/cinema.query";
import { Notify } from "../../layouts/notify";

const movieScheduleSchema = Yup.object({
  cinema_id: Yup.string().required("Selecting a cinema is required"),
  movie_id: Yup.string().required("Selecting a movie is required"),
  date: Yup.date()
    .required("Date is required")
    .min(new Date(), "Date cannot be in the past"),
  times: Yup.array()
    .of(
      Yup.object().shape({
        start_time: Yup.string().required("Start time is required"),
        end_time: Yup.string()
          .required("End time is required")
          .test(
            "is-greater",
            "End time must be later than start time",
            function (value) {
              const { start_time } = this.parent;
              return start_time && value && value > start_time;
            }
          ),
      })
    )
    .min(1, "At least one schedule time must be added")
    .nullable(),
});

export default function CinemaMovieList() {
  const [openModal, setOpenModal] = useState(false);
  const {
    data: movieSchedules,
    isLoading,
    isError,
    isSuccess,
    isFetching,
  } = useGetMovieScheduleQuery("");
  const { data: cinemas, isLoading: cinemasLoading } = useGetCinemasQuery("");
  const { data: movies, isLoading: moviesLoading } = useGetMoviesQuery("");
  const [addMovieSchedule, { isLoading: adding }] =
    useAddMovieScheduleMutation();
  const [open, setOpen] = useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = useState<any>(null);

  const handleSubmit = async (values: any, actions: any) => {
    console.log("Submitting form with values:", values);
    try {
      const formattedTimes = values.times.map(
        (time: { start_time: any; end_time: any }) => ({
          start_time: time.start_time,
          end_time: time.end_time,
          full_start_time: new Date(
            `${values.date}T${time.start_time}`
          ).toISOString(),
          full_end_time: new Date(
            `${values.date}T${time.end_time}`
          ).toISOString(),
        })
      );

      const payload = {
        cinema_id: values.cinema_id,
        movie_id: values.movie_id,
        date: values.date,
        start_time: formattedTimes[0].start_time,
        end_time: formattedTimes[0].end_time,
        times: formattedTimes.flatMap(
          (ft: { full_start_time: any; full_end_time: any }) => [
            ft.full_start_time,
          ]
        ),
      };

      console.log("Payload to be sent:", payload);

      await addMovieSchedule(payload);
      Notify("success", "Movie schedule added successfully!");
      actions.setSubmitting(false);
      setOpenModal(false);
    } catch (error) {
      console.error("Error during submission:", error);
      Notify("error", "Failed to add movie schedule");
      actions.setSubmitting(false);
    }
  };

  const [deleteCinema, { isLoading: deleteLoading }] =
    useDeleteMovieScheduleMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteCinema(itemTobeDeleted?.id);
    setOpen(false);
  };

  const columns = [
    {
      accessorKey: "movie_id",
      header: "Movie",
      size: 20,
      Cell: ({ row }: any) => {
        const cat = movies?.find(
          (m: { id: any }) => m.id === row.original.movie_id
        )?.title;
        return cat;
      },
    },
    {
      accessorKey: "cinema_id",
      header: "Cinema",
      size: 20,
      Cell: ({ row }: any) => {
        const cat = cinemas?.find(
          (m: { id: any }) => m.id === row.original.cinema_id
        )?.name;
        return cat;
      },
    },
    {
      accessorKey: "start_time",
      header: "Start Time",
      size: 20,
    },
    {
      accessorKey: "end_time",
      header: "End Time",
      size: 20,
    },
    {
      accessorKey: "date",
      header: " Date",
      size: 20,
    },
  ];

  const [globalFilter, setGlobalFilter] = useState("");
  const filteredData = useMemo(() => {
    if (!globalFilter) return movieSchedules;
    return movieSchedules?.filter((pr: { movie_id: any }) => {
      const searchContent = `${pr?.movie_id} `.toLowerCase();
      return searchContent?.includes(globalFilter?.toLowerCase());
    });
  }, [movieSchedules, globalFilter]);

  return (
    <DefaultPage
      title={"Movies Schedule"}
      primaryButtonProps={{
        startIcon: <Add />,
        children: "Add Movie Schedule",
        onClick: () => {
          setOpenModal(true);
        },
      }}
    >
      <DataTable
        count={filteredData?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading || moviesLoading || cinemasLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        toolbar={true}
        toolbarActions={true}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.movie_id}
        itemName={itemTobeDeleted?.movie_id}
      />
      <Dialog
        open={openModal}
        onClose={() => setOpenModal(false)}
        maxWidth="md"
      >
        <DialogTitle>Add New Movie Schedule</DialogTitle>
        <Formik
          initialValues={{
            cinema_id: "",
            movie_id: "",
            date: "",
            times: [{ start_time: null, end_time: null }],
          }}
          validationSchema={movieScheduleSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting, errors, touched, values }: any) => (
            <Form>
              <DialogContent>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <FormControl
                      error={touched.cinema_id && Boolean(errors.cinema_id)}
                      fullWidth
                    >
                      <InputLabel id="cinema-label">Cinema</InputLabel>
                      <Field
                        as={Select}
                        name="cinema_id"
                        labelId="cinema-label"
                        label="Cinema"
                      >
                        {cinemas?.map((cinema: any) => (
                          <MenuItem key={cinema.id} value={cinema.id}>
                            {cinema.name}
                          </MenuItem>
                        ))}
                      </Field>
                      {touched.cinema_id && errors.cinema_id && (
                        <Box color="error.main">{errors.cinema_id}</Box>
                      )}
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <FormControl
                      error={touched.movie_id && Boolean(errors.movie_id)}
                      fullWidth
                    >
                      <InputLabel id="movie-label">Movie</InputLabel>
                      <Field
                        as={Select}
                        name="movie_id"
                        labelId="movie-label"
                        label="Movie"
                      >
                        {movies?.map((movie: any) => (
                          <MenuItem key={movie.id} value={movie.id}>
                            {movie.title}
                          </MenuItem>
                        ))}
                      </Field>
                      {touched.movie_id && errors.movie_id && (
                        <Box color="error.main">{errors.movie_id}</Box>
                      )}
                    </FormControl>
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      as={TextField}
                      name="date"
                      type="date"
                      label="Date"
                      fullWidth
                      InputLabelProps={{ shrink: true }}
                    />
                    {touched.date && errors.date && (
                      <Box color="error.main">{errors.date}</Box>
                    )}
                  </Grid>
                  <FieldArray name="times">
                    {({ remove, push }) => (
                      <Grid item xs={12}>
                        {values.times.map((_: any, index: any) => (
                          <Grid container spacing={2} key={index}>
                            <Grid item xs={5}>
                              <Field
                                as={TextField}
                                name={`times.${index}.start_time`}
                                type="time"
                                label="Start Time"
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                              />
                              {touched.times?.[index]?.start_time &&
                                errors.times?.[index]?.start_time && (
                                  <Box color="error.main">
                                    {errors?.times[index]?.start_time}
                                  </Box>
                                )}
                            </Grid>
                            <Grid item xs={5}>
                              <Field
                                as={TextField}
                                name={`times.${index}.end_time`}
                                type="time"
                                label="End Time"
                                fullWidth
                                InputLabelProps={{ shrink: true }}
                              />
                              {touched.times?.[index]?.end_time &&
                                errors.times?.[index]?.end_time && (
                                  <Box color="error.main">
                                    {errors.times[index].end_time}
                                  </Box>
                                )}
                            </Grid>
                            <Grid item xs={2}>
                              <Button onClick={() => remove(index)}>-</Button>
                              {index === values.times.length - 1 && (
                                <Button
                                  onClick={() =>
                                    push({ start_time: "", end_time: "" })
                                  }
                                >
                                  +
                                </Button>
                              )}
                            </Grid>
                          </Grid>
                        ))}
                      </Grid>
                    )}
                  </FieldArray>
                </Grid>
              </DialogContent>
              <DialogActions>
                <Button onClick={() => setOpenModal(false)}>Cancel</Button>
                <LoadingButton type="submit" loading={isSubmitting || adding}>
                  Save
                </LoadingButton>
              </DialogActions>
            </Form>
          )}
        </Formik>
      </Dialog>
    </DefaultPage>
  );
}
