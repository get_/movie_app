import {  Box } from "@mui/material";
const QrCellImage = ({ row }: any) => {
  return (
    <Box display="flex" alignItems="center" sx={{ textTransform: "none" }}>
      <img
        src={row.original.qr_value as any}
        alt={row.original.title}
        style={{ width: 50, height: 50 }}
      />
    </Box>
  );
};

export default QrCellImage;
