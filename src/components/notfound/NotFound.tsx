import { Typography } from '@mui/material';
import { Helmet } from 'react-helmet-async';

// ----------------------------------------------------------------------

export default function NotFound() {
  return (
    <>
        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                textAlign: 'center'
            }}
        >
            <Helmet >
                <title> page not found </title>
            </Helmet>

            <Typography variant="h3" sx={{ mb: 3 }}>
                Sorry, Page not found!
            </Typography>
        </div>

    </>
  );
}
