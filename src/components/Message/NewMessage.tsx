import Box from "@mui/material/Box";
import { Chip, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { LoadingButton } from "@mui/lab";
import { useAddMessageMutation } from "../../services/message.query";
import { Notify } from "../../layouts/notify";
import { useNavigate } from "react-router";
import {
  useGetAllUsersQuery,
  useGetUsersQuery,
} from "../../services/user.query";

export default function NewMessage() {
  const [messageFor, setMessageFor] = useState("");
  const [message, setMessage] = useState("");
  const [selectedItem, setSelectedItem] = useState("");
  const navigate = useNavigate();
  const [messageForError, setMessageForError] = useState("");
  const [messageError, setMessageError] = useState("");
  const [addMessage, { isLoading: messageLoading }] = useAddMessageMutation();
  const handleMessage = (value: string) => {
    setMessage(value);
    setMessageError("");
  };
  const { data: allUsers, isLoading: allUsersLoading } =
    useGetAllUsersQuery("");
  const { data: users, isLoading: UsersLoading } = useGetUsersQuery("");
  const { data: deputyUsers, isLoading: deputyUsersLoading } =
    useGetAllUsersQuery("");

  const handleOnSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    if (!message) {
      setMessageError("Message is required.");
      return;
    }

    let usersToSend = [];
    if (selectedItem === "all") {
      usersToSend = allUsers;
    } else if (selectedItem === "users") {
      usersToSend = users;
    } else if (selectedItem === "deputies") {
      usersToSend = deputyUsers;
    }

    const messages = usersToSend.map((user: { user_id: any }) => ({
      user_id: user.user_id,
      user_type: messageFor === "deputies" ? "DEPUTY" : messageFor,
      message: message,
    }));

    try {
      await addMessage(messages);
      setMessage("");
      setSelectedItem("");
      setMessageError("");
      Notify("info", "Message Added Successfully");
      navigate("/messages/message-history");
    } catch (error) {
      console.error("Error adding message:", error);
      Notify("error", "Failed to send messages");
    }
  };

  return (
    <Box
      sx={{
        width: "100%",
        height: "auto",
        marginTop: "2%",
        gap: "0px",
        borderRadius: 5,
        backgroundColor: "#FFFFFF",
        position: "relative",
      }}
    >
      <form
        style={{
          display: "flex",
          flexDirection: "column",
        }}
        onSubmit={handleOnSubmit}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            padding: "20px",
            justifyContent: "flex-start",
            alignItems: "center",
          }}
        >
          <Typography>Message For</Typography>
          {["All", "Users", "Deputies"].map((item) => (
            <Chip
              key={item}
              clickable
              label={item}
              onClick={() => {
                setMessageFor(item.toLowerCase());
                setSelectedItem(item.toLowerCase());
                setMessageForError("");
              }}
              sx={{
                color:
                  selectedItem === item.toLowerCase() ? "#0054D3" : "#707070",
                backgroundColor:
                  selectedItem === item.toLowerCase() ? "#DAEBFF" : "#EDEFF2",
                marginLeft: 2,
              }}
            />
          ))}
          {messageForError && (
            <span style={{ color: "red" }}>{messageForError}</span>
          )}
        </Box>
        <Box
          sx={{
            width: "90%",
            borderRadius: 10,
            borderColor: "primary",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <TextField
            multiline
            InputProps={{
              disableUnderline: true,
            }}
            rows={6}
            variant="outlined"
            sx={{
              width: "90%",
              height: "70%",
              borderRadius: 10,
            }}
            value={message}
            onChange={(e) => handleMessage(e.target.value)}
            placeholder="Type your message here"
            error={!!messageError}
            helperText={messageError}
          />
          <LoadingButton
            loading={messageLoading}
            sx={{
              width: "50%",
              marginTop: "15px",
              mb: 2,
              backgroundColor: "#0054D3",
              textTransform: "none",
              color: "#FFFFFF",
              borderRadius: "0px", // Remove border-radius
              "&:hover": {
                backgroundColor: "#0054D3",
              },
              "&:focus": {
                backgroundColor: "gray",
              },
            }}
            type="submit"
            disabled={messageLoading}
          >
            Send
          </LoadingButton>
        </Box>
      </form>
    </Box>
  );
}
