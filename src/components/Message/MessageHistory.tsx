import Box from "@mui/material/Box";
import { Button } from "@mui/material";
import React, { useMemo, useState } from "react";
import {
  useDeleteMessageMutation,
  useGetMessagesQuery,
} from "../../services/message.query";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { DefaultPage } from "../../layouts/default-page";
import { DataTable } from "../../layouts/data-table";
import { Delete } from "@mui/icons-material";
import { format } from "date-fns";

export default function MessageHistory() {
  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);

  const [deleteMessageItem, { isLoading: deleteLoading }] =
    useDeleteMessageMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteMessageItem(itemTobeDeleted?.message);
    setOpen(false);
  };
  const {
    data: messages,
    isLoading,
    isSuccess,
    isError,
    isFetching,
  } = useGetMessagesQuery("");
  const columns = [
    {
      accessorKey: "message",
      header: "Message",
    },

    {
      accessorKey: "user_type",
      header: "User Type",
    },

    {
      accessorKey: "created_at",
      header: "Date",
      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  const [globalFilter, setGlobalFilter] = useState("");
  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return messages; // return all movies if no filter is applied
    return messages?.filter((pr: { message: any; user_type: any }) => {
      const searchContent = `${pr?.message} ${pr?.user_type}`.toLowerCase();
      return searchContent?.includes(globalFilter?.toLowerCase());
    });
  }, [messages, globalFilter]);
  return (
    <DefaultPage title="Message History" loading={isLoading}>
      <DataTable
        count={messages?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        toolbar={true}
        toolbarActions={true}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.message}
        itemName={itemTobeDeleted?.message}
      />
    </DefaultPage>
  );
}
