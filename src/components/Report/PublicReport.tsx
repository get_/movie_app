import Box from "@mui/material/Box";
import { Button } from "@mui/material";
import React, { useMemo, useState } from "react";
import {
  useDeletePublicReportMutation,
  useGetPublicReportsQuery,
} from "../../services/report.query";
import { DefaultPage } from "../../layouts/default-page";
import { Delete } from "@mui/icons-material";
import { DataTable } from "../../layouts/data-table";
import { format } from "date-fns";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { PublicReportDetailPanel } from "./public-report-detail-panel";

export default function PublicReport() {
  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);
  const [globalFilter, setGlobalFilter] = useState("");

  const [deletePublicReport, { isLoading: deleteLoading }] =
    useDeletePublicReportMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deletePublicReport(itemTobeDeleted?.id);
    setOpen(false);
  };
  const {
    data: PublicReports,
    isLoading,
    isFetching,
    isSuccess,
    isError,
  } = useGetPublicReportsQuery("");
  const columns = [
    {
      accessorKey: "phone_number",
      header: "Phone Number",
      size: 40,
    },
    {
      accessorKey: "email",
      header: "Email",
      size: 40,
    },
    {
      accessorKey: "place_type",
      header: "Place Type",
      size: 40,
    },
    {
      accessorKey: "report",
      header: "Report",
      size: 40,
    },
    {
      accessorKey: "event_date",
      header: "Event Date",
      size: 40,
      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return PublicReports; // return all movies if no filter is applied
    return PublicReports?.filter(
      (pr: { email: any; phone_number: any; report: any }) => {
        const searchContent =
          `${pr?.email} ${pr?.phone_number} ${pr?.report}`.toLowerCase();
        return searchContent?.includes(globalFilter.toLowerCase());
      }
    );
  }, [globalFilter, PublicReports]);

  return (
    <DefaultPage title="Public Reports" loading={isLoading}>
      <DataTable
        count={PublicReports?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        toolbar={true}
        toolbarActions={true}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderDetailPanel={({ row }: any) => {
          return <PublicReportDetailPanel report={row.original} />;
        }}
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.id}
        itemName={itemTobeDeleted?.title}
      />
    </DefaultPage>
  );
}
