import Box from "@mui/material/Box";
import { Button } from "@mui/material";
import React, { useMemo, useState } from "react";
import {
  useDeleteTVChannelReportMutation,
  useGetTVChannelReportsQuery,
} from "../../services/report.query";
import { DefaultPage } from "../../layouts/default-page";
import { Delete } from "@mui/icons-material";
import { DataTable } from "../../layouts/data-table";
import { format } from "date-fns";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { TVChannelReportDetailPanel } from "./tv-channel-report-detail";

export default function TvProgramReport() {
  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);

  const [deleteTVChannel, { isLoading: deleteLoading }] =
    useDeleteTVChannelReportMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteTVChannel(itemTobeDeleted?.id);
    setOpen(false);
  };
  const [globalFilter, setGlobalFilter] = useState("");

  const {
    data: TvProgramReport,
    isLoading,
    isFetching,
    isSuccess,
    isError,
  } = useGetTVChannelReportsQuery("");
  const columns = [
    {
      accessorKey: "program",
      header: "Program",
      size: 40,
    },
    {
      accessorKey: "email",
      header: "Email",
      size: 40,
    },
    {
      accessorKey: "channel",
      header: "Channel",
      size: 40,
    },
    {
      accessorKey: "report",
      header: "Report",
      size: 40,
    },
    {
      accessorKey: "event_date",
      header: "Event Date",
      size: 40,
      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return TvProgramReport; // return all movies if no filter is applied
    return TvProgramReport?.filter(
      (pr: { program: any; email: any; channel: any }) => {
        const searchContent =
          `${pr?.program} ${pr?.email} ${pr?.channel}`.toLowerCase();
        return searchContent?.includes(globalFilter.toLowerCase());
      }
    );
  }, [globalFilter, TvProgramReport]);
  return (
    <DefaultPage title="Tv Program Reports" loading={isLoading}>
      <DataTable
        count={TvProgramReport?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        toolbar={true}
        toolbarActions={true}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderDetailPanel={({ row }: any) => {
          console.log("row", row.original);
          return <TVChannelReportDetailPanel report={row.original} />;
        }}
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.program}
        itemName={itemTobeDeleted?.program}
      />
    </DefaultPage>
  );
}
