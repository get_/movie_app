import React, { useState } from "react";
import { Avatar, Box, Grid, TextField, Typography } from "@mui/material";
import { PublicReportResponse } from "../../models/Response";
import { LoadingButton } from "@mui/lab";
import { Notify } from "../../layouts/notify";
import { useAddTVChannelReportResponseMutation } from "../../services/report.query";

export const TVChannelReportDetailPanel = ({ report }: any) => {
  const [response, setResponse] = useState<string>("");
  const [responseError, setResponseError] = useState<string>("");
  const [addTvReportChannel, { isLoading }] =
    useAddTVChannelReportResponseMutation();
  const handleSendResponse = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!response.trim()) {
      setResponseError("Response cannot be empty");
      return;
    }
    const data: PublicReportResponse = {
      report_id: report.id,
      response: response,
    };
    try {
      await addTvReportChannel(data);
      setResponse("");
      setResponseError("");
      Notify("success", "Response Added Successfully");
    } catch (error) {
      console.error("Error submitting response:", error);
      setResponseError("Failed to submit response");
    }
  };

  return (
    <Box sx={{ maxWidth: "100%", overflowX: "auto" }}>
      <Grid container spacing={2} sx={{ p: 2, alignItems: "center" }}>
        <Grid item xs={12} md={6}>
          <Avatar
            src={report?.photo_url}
            alt={"Report image"}
            style={{ width: "100%", maxWidth: "500px", height: "auto" }}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Typography variant="h6" gutterBottom>
            Details
          </Typography>
          <Typography variant="subtitle1" gutterBottom>
            Program: {report?.program}
          </Typography>
          <Typography variant="subtitle1" gutterBottom>
            Time: {report?.time}
          </Typography>
          <Typography variant="subtitle1" gutterBottom>
            Event Date: {report?.event_date}
          </Typography>

          <Typography variant="subtitle1" gutterBottom>
            Channel: {report?.channel}
          </Typography>
          <Typography variant="subtitle1" gutterBottom>
            Email: {report?.email}
          </Typography>
          <Typography variant="subtitle1" sx={{ mb: 2 }}>
            report: {report?.report}
          </Typography>
          <form onSubmit={handleSendResponse}>
            <TextField
              fullWidth
              rows={4}
              multiline
              variant="outlined"
              placeholder="Type your response here..."
              value={response}
              onChange={(e) => setResponse(e.target.value)}
              error={Boolean(responseError)}
              helperText={responseError || " "}
              sx={{ mb: 2 }}
            />
            <LoadingButton
              type="submit"
              variant="contained"
              loading={isLoading}
              sx={{ width: "100%" }}
            >
              Send Response
            </LoadingButton>
          </form>
        </Grid>
      </Grid>
    </Box>
  );
};
