import Box from "@mui/material/Box";
import { Button } from "@mui/material";
import React, { useMemo, useState } from "react";
import {
  useDeleteDeputyReportMutation,
  useGetDeputyReportsQuery,
} from "../../services/report.query";
import { DefaultPage } from "../../layouts/default-page";
import { Delete } from "@mui/icons-material";
import { DataTable } from "../../layouts/data-table";
import { format } from "date-fns";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { DeputyReportDetailPanel } from "./deputy-report-detail";

export default function DeputyReport() {
  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);
  const [globalFilter, setGlobalFilter] = useState("");

  const [deleteDeputyReport, { isLoading: deleteLoading }] =
    useDeleteDeputyReportMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteDeputyReport(itemTobeDeleted?.id);
    setOpen(false);
  };
  const {
    data: TvProgramReport,
    isLoading,
    isFetching,
    isSuccess,
    isError,
  } = useGetDeputyReportsQuery("");
  const columns = [
    {
      accessorKey: "name",
      header: "Name",
      size: 40,
    },
    {
      accessorKey: "location",
      header: "Location",
      size: 40,
    },
    {
      accessorKey: "email",
      header: "Email",
      size: 40,
    },
    {
      accessorKey: "phone_number",
      header: "phone_number",
      size: 40,
    },
    {
      accessorKey: "tv_program_name",
      header: "TV Program",
      size: 40,
    },

    {
      accessorKey: "event_date",
      header: "Event Date",
      size: 40,

      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  // Filtered data using global search across multiple columns
  const filteredData = useMemo(() => {
    if (!globalFilter) return TvProgramReport; // return all movies if no filter is applied
    return TvProgramReport?.filter(
      (pr: { name: any; location: any; phone_number: any }) => {
        const searchContent =
          `${pr?.name} ${pr?.location} ${pr?.phone_number}`.toLowerCase();
        return searchContent?.includes(globalFilter?.toLowerCase());
      }
    );
  }, [TvProgramReport, globalFilter]);
  return (
    <DefaultPage title="Deputy Reports" loading={isLoading}>
      <DataTable
        count={TvProgramReport?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        toolbar={true}
        toolbarActions={true}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderDetailPanel={({ row }: any) => {
          return <DeputyReportDetailPanel report={row.original} />;
        }}
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.name}
        itemName={itemTobeDeleted?.name}
      />
    </DefaultPage>
  );
}
