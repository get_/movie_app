import { DefaultPage } from "../../layouts/default-page";
import { Box, CircularProgress, Grid } from "@mui/material";
import StatisticCard from "./static-card";
import ReportPieChart from "./pie-chart";
import MovieIcon from "@mui/icons-material/Movie";
import TvIcon from "@mui/icons-material/Tv";
import LocationCityIcon from "@mui/icons-material/LocationCity";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import TheatersIcon from "@mui/icons-material/Theaters";
import ScheduleIcon from "@mui/icons-material/Schedule";
import { useGetCountsQuery } from "./dashboard.query";
import { Person3 } from "@mui/icons-material";
export const MyDashboard = () => {
  const {
    data: counts,
    isLoading,
    isSuccess,
    isFetching,
    isError,
  } = useGetCountsQuery("");
  const processedCount = isSuccess
    ? Array.isArray(counts)
      ? counts[0]
      : []
    : [];
  const reportData = [
    { name: "TV Reports", value: processedCount?.tv_program_report_count },
    { name: "Public Reports", value: processedCount?.public_report_count },
    { name: "Deputy Reports", value: processedCount?.deputy_report_count },
  ];

  const userPieChart = [
    { name: "Deputy Users", value: processedCount?.deputy_user_count },
    {
      name: "Waiting for Verification",
      value: processedCount?.not_verified_deputy_user_count,
    },
    { name: "All Users", value: processedCount?.all_user_count },
  ];

  const stats = [
    {
      title: "Total Users",
      total: processedCount?.all_user_count,
      IconComponent: Person3,
      redirectUrl: "/hr/user",
    },
    {
      title: "Total Movies",
      total: processedCount?.movie_count,
      IconComponent: MovieIcon,
      redirectUrl: "/movies/movies",
    },
    {
      title: "Channels",
      total: processedCount?.channel_count,
      IconComponent: TvIcon,
      redirectUrl: "/tv/channels",
    },

    {
      title: "Cities",
      total: processedCount?.city_count,
      IconComponent: LocationCityIcon,
      redirectUrl: "/cinemas/cities",
    },
    {
      title: "Malls",
      total: processedCount?.mall_count,
      IconComponent: LocalMallIcon,
      redirectUrl: "/cinemas/malls",
    },
    {
      title: "Cinemas",
      total: processedCount?.cinema_count,
      IconComponent: TheatersIcon,
      redirectUrl: "/cinemas/cinemas",
    },
    {
      title: "Movie Schedule",
      total: processedCount?.movie_schedule_count,
      IconComponent: ScheduleIcon,
      redirectUrl: "/cinemas/movies",
    },
    {
      title: "Messages",
      total: processedCount?.message_count,
      IconComponent: ScheduleIcon,
      redirectUrl: "/messages/message-history",
    },
  ];

  return (
    <DefaultPage title={"Dashboard"}>
      {isLoading || isFetching ? (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh",
          }}
        >
          <CircularProgress />
        </Box>
      ) : (
        <Grid container spacing={2}>
          {/* Your grid content with stats and charts */}
          {stats.map((stat: any, index: number) => (
            <Grid item xs={12} sm={6} md={3} key={index}>
              <StatisticCard
                title={stat.title}
                total={stat.total}
                IconComponent={stat.IconComponent}
                redirectUrl={stat.redirectUrl}
              />
            </Grid>
          ))}
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <ReportPieChart data={reportData} />
            </Grid>
            <Grid item xs={12} md={6}>
              <ReportPieChart data={userPieChart} />
            </Grid>
          </Grid>
        </Grid>
      )}
    </DefaultPage>
  );
};
