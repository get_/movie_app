import React from "react";
import { Card, CardContent, Typography, Box } from "@mui/material";
import { useNavigate } from "react-router-dom";

function StatisticCard({ title, total, IconComponent, redirectUrl }: any) {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(redirectUrl);
  };

  return (
    <Card
      sx={{
        boxShadow: 2,
        "&:hover": {
          cursor: "pointer",
          backgroundColor: "primary.main", // Use your theme's primary color
          color: "primary.contrastText", // Ensures readable text color on hover
        },
      }}
    >
      <CardContent onClick={handleClick}>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Box display="flex" flexDirection="row" alignItems="center">
            <Typography variant="h5">{title}</Typography>
            <Box
              sx={{
                ml: 1,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "green.200",
                borderRadius: "4px",
                p: "2px 8px",
                color: "green.800",
              }}
            >
              <Typography variant="body2" fontSize={20} fontWeight={500}>
                {total}
              </Typography>
            </Box>
          </Box>
          <Box sx={{ color: "grey.500" }}>
            <IconComponent sx={{ color: "grey.500" }} />
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
}

export default StatisticCard;
