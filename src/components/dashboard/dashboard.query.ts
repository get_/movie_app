import { apiSlice } from "../../store/app.api";
import { supabase } from "../../utils/API";
const dashboardAPi = apiSlice.injectEndpoints({
  endpoints: (build) => ({
    getCounts: build.query<any, any>({
      queryFn: async () => {
        let { data } = await supabase.rpc("get_all_counts");

        return { data };
      },
      providesTags: [
        "city",
        "messages",
        "movies",
        "mall",
        "Cinema",
        "channels",
        "reports",
        "users",
      ],
    }),
  }),
  overrideExisting: true,
});

export const { useGetCountsQuery } = dashboardAPi;
