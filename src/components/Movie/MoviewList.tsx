import {
  Avatar,
  Backdrop,
  Button,
  Chip,
  CircularProgress,
  Container,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  List,
  ListItem,
  ListItemText,
  MenuItem,
  Paper,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { Genre, Movie } from "../../models/Movie";
import React, { ChangeEvent, useEffect, useState } from "react";
import { createSignInUrl, deleteFile, upload } from "../../services/Upload";
import {
  deleteMovie,
  getCategories,
  getCategory,
  getGenres,
  getMovieReviewers,
  getMovies,
  updateMovie,
} from "../../services/MovieService";
import { useFormik } from "formik";
import Box from "@mui/material/Box";
import SearchIcon from "@mui/icons-material/Search";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { DatePicker } from "@mui/x-date-pickers";
import OutlinedInput from "@mui/material/OutlinedInput";
import { v4 as uuidv4 } from "uuid";
import dayjs, { Dayjs } from "dayjs";
import { Category } from "../../models/Category";
import { BoardMember } from "../../models/BoardMember";
import { supabase } from "../../utils/API";
import { getBoardMembers } from "../../services/ChannelService";
import { useNavigate } from "react-router-dom";
import { FilterModel } from "../../models/State";
import useIsMobile from "../../hooks/is-mobile";
import { LoadingButton } from "@mui/lab";
import { Notify } from "../../layouts/notify";

const MovieItem = (movie: Movie) => {
  console.log("move", movie);
  const [open, setOpen] = React.useState<boolean>(false);
  const [editMode, setEditMode] = React.useState<boolean>(false);

  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = () => {
    setOpen(true);
  };
  const deleteItem = async (id: number | undefined) => {
    await deleteMovie(id);
    setOpen(false);
  };

  //edit form
  const formik = useFormik({
    initialValues: {
      title: movie.title,
      category: movie.category,
      genre: movie.genre,
      applicant: movie.applicant,
      permit_number: movie.permit_number,
      reason: movie.reason,
      release_date: movie.release_date,
      premier_date: movie.premiere_date,
      cover_file: null,
      avatar_file: null,
      committees: movie.committees
        ? movie.committees?.map((id) => Number(id))
        : [],
    },
    onSubmit: async (values, { resetForm }) => {
      try {
        let formData: Movie = {
          id: movie.id,
        };
        if (values.title && values.title !== movie.title) {
          formData.title = values.title;
        }
        if (values.category && values.category !== movie.category) {
          formData.category = Number(values.category);
        }
        if (values.release_date && values.release_date !== movie.release_date) {
          formData.release_date = values.release_date;
        }
        if (
          values.premier_date &&
          values.premier_date !== movie.premiere_date
        ) {
          formData.premiere_date = values.premier_date;
        }
        if (values.genre && values.genre !== movie.genre) {
          formData.genre = values.genre;
        }
        if (values.applicant && values.applicant !== movie.applicant) {
          formData.applicant = values.applicant;
        }
        if (
          values.permit_number &&
          values.permit_number !== movie.permit_number
        ) {
          formData.permit_number = values.permit_number;
        }
        if (values.reason && values.reason !== movie.reason) {
          formData.reason = values.reason;
        }
        if (values.committees && values.committees !== movie.committees) {
          formData.committees = values.committees.map((id) => Number(id));
        }
        if (avatarImage) {
          const file = avatarImage;
          const fileExt = file?.name.split(".").pop();
          const filePath = `${uuidv4()}-${Math.random()}.${fileExt}`;
          let data = await upload(file!, "movie", filePath);
          formData.avatar_img = data.url;
        }

        if (coverImage) {
          const file = coverImage;
          const fileExt = file?.name.split(".").pop();
          const filePath = `${uuidv4()}-${Math.random()}.${fileExt}`;
          let data = await upload(file!, "movie", filePath);
          formData.cover_img = data.url;
        }
        if (Object.keys(formData).length > 1) {
          await updateMovie(formData);
        }
        let filenames = [];
        if (movie.avatar_img && formData.avatar_img) {
          filenames.push(movie.avatar_img);
        }
        if (movie.cover_img && formData.cover_img) {
          filenames.push(movie.cover_img);
        }
        if (filenames.length > 0) {
          await deleteFile("movie", filenames);
        }
        setOpen(false);
        setAvatarImage(null);
        setCoverImage(null);
        setReleaseDate(null);
        setPremiereDate(null);
        setIsFocused(false);
        setGenreIsFocused(false);
        setIsOpen(false);
        setPremiereIsOpen(false);
        setIsCommitteeSeclected(false);
        setEditMode(false);
        Notify("info", "Movie Updated Successfully");
      } catch (err) {
        Notify("error", "Error occurred while editing");
      }
    },
  });

  const [avatarImage, setAvatarImage] = useState<File | null>(null);
  const [coverImage, setCoverImage] = useState<File | null>(null);
  const [release_date, setReleaseDate] = useState<Dayjs | null | undefined>(
    dayjs(movie.release_date)
  );
  const [premiere_date, setPremiereDate] = useState<Dayjs | null | undefined>(
    dayjs(movie.premiere_date)
  );
  const handleAvatarImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.currentTarget.files) {
      setAvatarImage(e.target.files[0]);
      formik.setFieldValue("avatar_file", e.currentTarget.files[0]);
    }
  };
  const handleCoverImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.currentTarget.files) {
      setCoverImage(e.target.files[0]);
      formik.setFieldValue("cover_file", e.currentTarget.files[0]);
    }
  };
  const [categories, setCategories] = useState<Category[]>([]);
  const [types, setTypes] = useState<Genre[]>([]);

  const handleReleaseDateChange = (newValue: Dayjs | null | undefined) => {
    setReleaseDate(newValue);
    formik.setFieldValue("release_date", newValue);
  };
  const handlePremierDateChange = (newValue: Dayjs | null | undefined) => {
    setPremiereDate(newValue);
    formik.setFieldValue("premier_date", newValue);
  };
  const [isFocused, setIsFocused] = useState(false);
  const [genreIsFocused, setGenreIsFocused] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isPremiereDateOpen, setPremiereIsOpen] = useState(false);
  const [isCommitteSelected, setIsCommitteeSeclected] = useState(false);
  const [users, setBoardMembers] = useState<BoardMember[]>([]);

  async function getCategories() {
    try {
      const { data, error } = await supabase
        .from("categories")
        .select("*")
        .limit(10);

      if (error) throw error;

      if (data != null) {
        setCategories(data);
      }
    } catch (error) {
      console.error("Error fetching categories:", error);
    }
  }

  useEffect(() => {
    getCategories();
  }, []);
  useEffect(() => {
    const fetchData = async () => {
      try {
        let data = await getGenres();
        if (data) {
          setTypes(data);
        }
      } catch (error) {
        console.error("Error fetching types:", error);
      }
    };

    fetchData();
  }, []);
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        let data = await getBoardMembers();
        if (data) {
          setBoardMembers(data);
        }
      } catch (error) {
        console.error("Error fetching types:", error);
      }
    };

    fetchUserData();
  }, []);

  return editMode ? (
    <Paper
      elevation={1}
      sx={{
        width: "100%",
        borderRadius: "15px",
        padding: 2,
      }}
    >
      <form autoComplete="off" onSubmit={formik.handleSubmit}>
        <Grid container spacing={2}>
          <Grid item xs={9} sm={2} md={2}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                width: "123px",
                height: "180px",
              }}
            >
              <Box
                sx={{
                  width: "123px",
                  position: "relative",
                  height: "180px",
                }}
              >
                <img
                  alt={movie.title}
                  src={movie.avatar_img}
                  style={{
                    position: "absolute",
                    width: "123px",
                    height: "180px",
                    borderRadius: "8px",
                  }}
                />

                <input
                  accept={"image/*"}
                  style={{ display: "none" }}
                  id="contained-file"
                  type="file"
                  multiple
                  name={"avatar_file"}
                  onChange={handleAvatarImageChange}
                  onBlur={formik.handleBlur}
                />

                <label htmlFor={"contained-file"}>
                  <Button
                    sx={{
                      width: "123px",
                      position: "relative",
                      height: "180px",
                      ":focus": {
                        border: "none",
                        outline: "none",
                        bgcolor: "none",
                      },
                      ":hover": {
                        border: "none",
                        outline: "none",
                        bgcolor: "none",
                      },
                    }}
                    variant="text"
                    component="span"
                    endIcon={
                      <img
                        alt={"sync"}
                        src={"sync.png"}
                        style={{
                          width: "20px",
                          height: "20px",
                          position: "absolute",
                          top: "5px",
                          right: "5px",
                        }}
                      />
                    }
                  ></Button>
                </label>
              </Box>
            </Box>
            {avatarImage && <span>{avatarImage.name}</span>}
          </Grid>
          <Grid
            item
            xs={9}
            sm={9}
            md={9}
            direction="column"
            container
            spacing={2}
            style={{ marginTop: 1 }}
          >
            <FormControl fullWidth color="primary">
              <Typography>Title</Typography>
              <TextField
                sx={{
                  width: "100%",
                  margin: "0px",
                  padding: "0px",
                  borderRadius: 5,
                  color: "Primary",
                }}
                InputProps={{
                  disableUnderline: true,
                }}
                name="title"
                id={"id_title"}
                variant="outlined"
                color="primary"
                value={formik.values.title}
                onChange={formik.handleChange}
                error={formik.touched.title && Boolean(formik.errors.title)}
                helperText={formik.touched.title && formik.errors.title}
                onBlur={formik.handleBlur}
                type="text"
              />
            </FormControl>
            <FormControl
              fullWidth
              sx={{
                my: 1,
              }}
            >
              <Typography
                sx={{
                  borderRadius: 10,
                }}
              >
                Rating
              </Typography>

              <TextField
                sx={{
                  width: isFocused || formik.values.category ? "100%" : "100%",
                  height: "auto",
                  borderRadius: 10,
                  marginTop:
                    isFocused || formik.values.category ? "16px" : "0px",
                }}
                InputLabelProps={{
                  style: {
                    // fontSize: '15px',
                  },
                }}
                id="id_category"
                select
                name="category"
                label={isFocused || formik.values.category ? "" : "Rating"}
                InputProps={{
                  disableUnderline: true,
                }}
                variant="outlined"
                color="primary"
                SelectProps={{
                  IconComponent: ExpandMoreIcon,
                }}
                value={formik.values.category}
                onChange={formik.handleChange}
                error={
                  formik.touched.category && Boolean(formik.errors.category)
                }
                helperText={formik.touched.category && formik.errors.category}
                onBlur={formik.handleBlur}
                onFocus={() => setIsFocused(true)}
              >
                {categories.map((category) => (
                  <MenuItem key={category.id} value={category.id}>
                    {category.name}
                  </MenuItem>
                ))}
              </TextField>
            </FormControl>
            <FormControl
              fullWidth
              sx={{
                my: 1,
              }}
            >
              {" "}
              <Typography>Genre</Typography>
              <TextField
                sx={{
                  width:
                    genreIsFocused || formik.values.genre ? "100%" : "100%",
                  height: "auto",
                  borderRadius: 10,
                  marginTop:
                    genreIsFocused || formik.values.genre ? "16px" : "16px",
                }}
                id="id_genre"
                select
                name="genre"
                label={genreIsFocused || formik.values.genre ? "" : "Genre"}
                InputProps={{
                  disableUnderline: true,
                }}
                SelectProps={{
                  IconComponent: ExpandMoreIcon,
                }}
                variant="outlined"
                color="primary"
                value={formik.values.genre}
                onChange={formik.handleChange}
                error={formik.touched.genre && Boolean(formik.errors.genre)}
                helperText={formik.touched.genre && formik.errors.genre}
                onBlur={formik.handleBlur}
                onFocus={() => setGenreIsFocused(true)}
              >
                {types.map((option) => (
                  <MenuItem key={option.id} value={option.name}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </FormControl>
            <br />
          </Grid>
          <Grid item xs={1} sm={1} md={1}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "flex-end",
                flexDirection: "column",
                alignItems: "end",
                alignContent: "end",
                marginRight: "20px",
                borderRadius: 10,
              }}
            >
              <IconButton
                aria-label="close"
                onClick={() => setEditMode(false)}
                sx={{
                  color: (theme) => theme.palette.grey[500],
                }}
              >
                <img alt={"close icon"} src={"x.png"} />
              </IconButton>
              <Button
                sx={{
                  width: "74px",
                  height: "39px",
                  textTransform: "none",
                  color: "white",
                  marginLeft: "10px",
                  borderRadius: "25px",
                  "&:hover": {
                    backgroundColor: "red",
                    color: "black",
                  },
                }}
                onClick={() => openDeleteModal()}
              >
                Delete
              </Button>
            </Box>
          </Grid>
        </Grid>
        <Grid container spacing={2} width={"100%"}>
          <Grid item xs={2} sm={12} md={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <FormControl fullWidth>
                <Typography>Release Date</Typography>

                <DatePicker
                  sx={{
                    width:
                      isOpen ||
                      release_date ||
                      dayjs(formik.values.release_date)
                        ? "100"
                        : "30%",
                    marginTop:
                      isOpen ||
                      release_date ||
                      dayjs(formik.values.release_date)
                        ? "16px"
                        : "0px",
                  }}
                  slotProps={{
                    textField: {
                      variant: "outlined",
                      color: "primary",
                      InputProps: { disableUnderline: true },
                    },
                  }}
                  slots={{
                    openPickerIcon: () => (
                      <img alt="datepicker icon" src={"picker.png"} />
                    ),
                  }}
                  label={
                    isOpen || release_date || dayjs(formik.values.release_date)
                      ? ""
                      : "Date"
                  }
                  onOpen={() => setIsOpen(true)}
                  disablePast
                  name={"release_date"}
                  value={release_date || dayjs(formik.values.release_date)}
                  onChange={handleReleaseDateChange}
                  format="DD.MM.YYYY"
                />
              </FormControl>
            </Box>
          </Grid>
          <Grid item xs={2} sm={12} md={12}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <FormControl fullWidth>
                <Typography>Premiere Date</Typography>

                <DatePicker
                  sx={{
                    width:
                      isPremiereDateOpen ||
                      premiere_date ||
                      dayjs(formik.values.premier_date)
                        ? "100"
                        : "30%",
                    marginTop:
                      isPremiereDateOpen ||
                      premiere_date ||
                      dayjs(formik.values.premier_date)
                        ? "16px"
                        : "0px",
                  }}
                  slotProps={{
                    textField: {
                      variant: "outlined",
                      color: "primary",
                      InputProps: { disableUnderline: true },
                    },
                  }}
                  slots={{
                    openPickerIcon: () => (
                      <img alt="datepicker icon" src={"picker.png"} />
                    ),
                  }}
                  label={
                    isPremiereDateOpen ||
                    premiere_date ||
                    dayjs(formik.values.premier_date)
                      ? ""
                      : "Date"
                  }
                  disablePast
                  name={"premier_date"}
                  value={premiere_date || dayjs(formik.values.premier_date)}
                  onChange={handlePremierDateChange}
                  onOpen={() => setPremiereIsOpen(true)}
                  format="DD.MM.YYYY"
                />
              </FormControl>
            </Box>
          </Grid>

          <Grid item xs={2} sm={12} md={12}>
            <FormControl fullWidth>
              <Typography>Applicant Name</Typography>
              <TextField
                sx={{ borderRadius: 10 }}
                InputProps={{
                  disableUnderline: true,
                }}
                id="id_applicant"
                name="applicant"
                variant="outlined"
                color="primary"
                type="text"
                value={formik.values.applicant}
                onChange={formik.handleChange}
                error={
                  formik.touched.applicant && Boolean(formik.errors.applicant)
                }
                helperText={formik.touched.applicant && formik.errors.applicant}
                onBlur={formik.handleBlur}
              />
            </FormControl>
          </Grid>
          <Grid item xs={2} sm={12} md={12}>
            <FormControl fullWidth>
              <Typography>Permit Number</Typography>
              <TextField
                sx={{ width: "100%", borderRadius: 10 }}
                InputProps={{
                  disableUnderline: true,
                }}
                id="id_permit_number"
                name="permit_number"
                variant="outlined"
                color="primary"
                type="text"
                value={formik.values.permit_number}
                onChange={formik.handleChange}
                error={
                  formik.touched.permit_number &&
                  Boolean(formik.errors.permit_number)
                }
                helperText={
                  formik.touched.permit_number && formik.errors.permit_number
                }
                onBlur={formik.handleBlur}
              />
            </FormControl>
          </Grid>
          <Grid item xs={2} sm={12} md={12}>
            <FormControl fullWidth>
              <Typography>Reason for Classification</Typography>
              <TextField
                sx={{
                  borderRadius: 10,
                }}
                fullWidth
                InputProps={{
                  disableUnderline: true,
                }}
                multiline
                id="id_reason"
                name="reason"
                value={formik.values.reason}
                onChange={formik.handleChange}
                variant="outlined"
                color="primary"
                type="text"
              />
            </FormControl>
          </Grid>
        </Grid>

        <Typography
          sx={{
            width: "100%",
            color: "#AFAFAF",
            margin: 1,
            fontSize: 20,
            fontWeight: "bold",
            borderRadius: 10,
          }}
        >
          Review Committee Members
        </Typography>
        <FormControl sx={{ m: 1 }} fullWidth>
          <Typography>Committee Members</Typography>
          <InputLabel id="multiple_committee">
            {isCommitteSelected || formik.values.committees
              ? ""
              : "Select Committee"}
          </InputLabel>
          <Select
            sx={{
              "& fieldset": {
                border: "primary",
              },
              width:
                isCommitteSelected || formik.values.committees ? "50%" : "20%",
              marginTop:
                isCommitteSelected || formik.values.committees ? "16px" : "0px",
            }}
            id="multiple_committee"
            labelId="multiple_committee"
            IconComponent={ExpandMoreIcon}
            multiple
            name={"committees"}
            color="primary"
            variant={"outlined"}
            value={formik.values.committees}
            onChange={formik.handleChange}
            input={<OutlinedInput />}
            renderValue={(selected) => (
              <div style={{ display: "flex", flexWrap: "wrap" }}>
                {selected.map((value) => (
                  <Chip
                    avatar={
                      <Avatar
                        alt={users.find((u) => Number(u.id) === value)?.name}
                        src={
                          users.find((u) => Number(u.id) === value)?.profile_url
                        }
                      />
                    }
                    key={value}
                    label={users.find((u) => Number(u.id) === value)?.name}
                    style={{ margin: 2 }}
                  />
                ))}
              </div>
            )}
            MenuProps={{
              PaperProps: {
                style: {
                  maxHeight: 48 * 4.5 + 8,
                  width: 250,
                },
              },
            }}
            onOpen={() => setIsCommitteeSeclected(true)}
          >
            {users.map((user) => (
              <MenuItem key={user.id} value={user.id}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    gap: "10px",
                  }}
                >
                  <Avatar alt={user.name} src={user.profile_url} />
                  <ListItemText primary={user.name} />
                </div>
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <Grid item xs={12} sm={2} md={2}>
          <Box
            sx={{
              bgcolor: "#DEECFF",
              width: "390px",
              height: "333px",
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 2,
              justifyContent: "center",
            }}
          >
            <Box
              sx={{
                position: "relative",
                width: "390px",
                height: "333px",
              }}
            >
              <img
                alt={movie.title}
                src={movie.cover_img}
                style={{
                  position: "absolute",
                  width: "390px",
                  height: "333px",
                }}
              />

              <input
                accept={"image/*"}
                style={{ display: "none" }}
                id="cover_img"
                type="file"
                multiple
                name={"cover_file"}
                onChange={handleCoverImageChange}
                onBlur={formik.handleBlur}
              />

              <label htmlFor={"cover_img"}>
                <Button
                  sx={{
                    position: "relative",
                    width: "390px",
                    height: "333px",
                    ":focus": {
                      border: "none",
                      outline: "none",
                      bgcolor: "none",
                    },
                    ":hover": {
                      border: "none",
                      outline: "none",
                      bgcolor: "none",
                    },
                  }}
                  variant="text"
                  component="span"
                  endIcon={
                    <img
                      alt={"sync"}
                      src={"sync.png"}
                      style={{
                        width: "20px",
                        height: "20px",
                        position: "absolute",
                        top: "5px",
                        right: "5px",
                      }}
                    />
                  }
                ></Button>
              </label>
            </Box>
          </Box>
          <div>{coverImage && <span>{coverImage.name}</span>}</div>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={10} md={10} />
          <LoadingButton
            type="submit"
            variant="contained"
            color="primary"
            className="hover:bg-green-700 text-white px-4 py-2 rounded-md"
            sx={{
              marginTop: 2,
              width: "50%",
              bgcolor: "#0054D3",
              borderRadius: "10px",
              textTransform: "none",
            }}
            loading={formik.isSubmitting}
          >
            Save
          </LoadingButton>
        </Grid>
      </form>
    </Paper>
  ) : (
    <Paper
      elevation={0}
      style={{
        width: "80%",
        borderRadius: "2%",
        marginTop: "2%",
      }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={2} md={2}>
          <Box
            style={{
              width: "30%",
              position: "relative",
              height: "180px",
              margin: 20,
              borderRadius: "8px",
            }}
          >
            <img
              alt={movie.title}
              src={movie.avatar_img}
              style={{
                position: "absolute",
                width: "123px",
                height: "180px",
                borderRadius: "8px",
              }}
            />
          </Box>
        </Grid>
        <Grid item xs={12} sm={7} md={7}>
          <Box
            sx={{
              marginLeft: "7%",
              marginTop: "2%",
            }}
          >
            <Typography
              variant="subtitle1"
              component="div"
              sx={{
                color: "#AFAFAF",
                width: "29px",
                height: "19px",
              }}
            >
              Title
            </Typography>
            <Typography
              component="div"
              sx={{
                color: "black",
                width: "200px",
                overflow: "hidden",
                textOverflow: "ellipsis",
                fontSize: "16px",
                lineHeight: "19.09px",
                marginTop: "10px",
              }}
            >
              {movie.title}
            </Typography>
            <Typography
              variant="subtitle1"
              component="div"
              sx={{
                color: "#AFAFAF",
                width: "29px",
                height: "19px",
                marginTop: "20px",
              }}
            >
              Rating
            </Typography>
            <Typography
              component="div"
              sx={{
                color: "black",
                width: "200px",
                overflow: "hidden",
                textOverflow: "ellipsis",
                fontSize: "16px",
                lineHeight: "19.09px",
                marginTop: "10px",
              }}
            >
              {movie.category_name}
            </Typography>
            <Typography
              variant="subtitle1"
              component="div"
              sx={{
                color: "#AFAFAF",
                width: "29px",
                height: "19px",
                marginTop: "20px",
              }}
            >
              Genre
            </Typography>
            <Typography
              component="div"
              sx={{
                color: "black",
                width: "200px",
                overflow: "hidden",
                textOverflow: "ellipsis",
                fontSize: "16px",
                lineHeight: "19.09px",
                marginTop: "10px",
              }}
            >
              {movie.genre}
            </Typography>
          </Box>
        </Grid>
        <Grid item xs={12} sm={3} md={3}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              flexDirection: "column",
              alignItems: "end",
              marginRight: "20px",
              marginTop: "20px",
            }}
          >
            <IconButton
              aria-label="close"
              onClick={() => setEditMode(true)}
              sx={{
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <img alt={"close icon"} src={"edit.png"} />
            </IconButton>
            <Button
              sx={{
                width: "74px",
                height: "39px",
                textTransform: "none",
                backgroundColor: "#797E8B",
                color: "white",
                marginLeft: "10px",
                borderRadius: "25px",
                "&:hover": {
                  backgroundColor: "gray.100",
                  color: "black",
                },
              }}
              onClick={() => openDeleteModal()}
            >
              Delete
            </Button>
          </Box>
        </Grid>
      </Grid>
      <Box>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <Paper
            elevation={0}
            sx={{
              backgroundColor: "#F6F7FD",
              width: "600px",
              height: "413px",
              borderRadius: "15px",
            }}
          >
            <DialogTitle id="alert-dialog-title">
              <Typography
                variant="body1"
                component="div"
                align={"center"}
                sx={{
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                }}
              >
                Do you want to delete <b>{movie.title}</b>?
              </Typography>
            </DialogTitle>
            <IconButton
              aria-label="close"
              onClick={() => handleClose()}
              sx={{
                position: "absolute",
                top: 8,
                right: 0,
                color: (theme) => theme.palette.error.main,
              }}
            >
              {/*<CloseIcon/>*/}
              <img alt={"close icon"} src={"x.png"} />
            </IconButton>
            <DialogContent
              sx={{
                backgroundColor: "white",
                height: "220px",
              }}
            >
              <div style={{ display: "flex", justifyContent: "flex-start" }}>
                <img
                  alt={movie.title}
                  src={movie.avatar_img}
                  style={{ width: "123px", height: "180px" }}
                />
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div
                    style={{
                      marginLeft: "20px",
                      marginTop: "20px",
                    }}
                  >
                    <Typography
                      variant="subtitle1"
                      component="div"
                      sx={{
                        color: "#AFAFAF",
                        width: "29px",
                        height: "19px",
                      }}
                    >
                      Title
                    </Typography>
                    <Typography
                      component="div"
                      sx={{
                        color: "black",
                        width: "200px",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        fontSize: "16px",
                        lineHeight: "19.09px",
                        marginTop: "10px",
                      }}
                    >
                      {movie.title}
                    </Typography>
                    <Typography
                      variant="subtitle1"
                      component="div"
                      sx={{
                        color: "#AFAFAF",
                        width: "29px",
                        height: "19px",
                        marginTop: "20px",
                      }}
                    >
                      Rating
                    </Typography>
                    <Typography
                      component="div"
                      sx={{
                        color: "black",
                        width: "200px",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        fontSize: "16px",
                        lineHeight: "19.09px",
                        marginTop: "10px",
                      }}
                    >
                      {movie.category_name}
                    </Typography>
                    <Typography
                      variant="subtitle1"
                      component="div"
                      sx={{
                        color: "#AFAFAF",
                        width: "29px",
                        height: "19px",
                        marginTop: "20px",
                      }}
                    >
                      Genre
                    </Typography>
                    <Typography
                      component="div"
                      sx={{
                        color: "black",
                        width: "200px",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        fontSize: "16px",
                        lineHeight: "19.09px",
                        marginTop: "10px",
                      }}
                    >
                      {movie.genre}
                    </Typography>
                  </div>
                </div>
              </div>
            </DialogContent>
            <DialogActions>
              <Grid container spacing={2}>
                <Grid item xs={1} sm={1} md={1} />

                <Grid item xs={10} sm={10} md={10}>
                  <Button
                    onClick={() => deleteItem(movie.id)}
                    color="error"
                    sx={{
                      textTransform: "none",
                      color: "white",
                      width: "100%",
                      borderRadius: "55px",
                      "&:hover": {
                        backgroundColor: "red",
                        color: "white",
                      },
                      margin: 2,
                      backgroundColor: "red",
                    }}
                  >
                    Confirm
                  </Button>
                </Grid>
                <Grid item xs={1} sm={1} md={1} />
              </Grid>
            </DialogActions>
          </Paper>
        </Dialog>
      </Box>
    </Paper>
  );
};

export default function MovieList() {
  const [loading, setLoading] = useState<boolean>(true);
  const [showFilter, setShowFilter] = useState<boolean>(false);
  const [movies, setMovies] = useState<Movie[]>([]);
  const [genres, setGenres] = useState<Genre[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);
  const isMobile = useIsMobile();

  useEffect(() => {
    const fetchMovies = async () => {
      const movies: Movie[] = await getMovies();
      for (let i = 0; i < movies.length; i++) {
        let category = await getCategory(movies[i].category as number);

        const movie = movies[i];
        movie.category_name = category;
        let committees = movie.committees?.map((id) => Number(id));
        movie.committee_members = await getMovieReviewers(committees);
        if (movie.avatar_img) {
          movie.avatar_img = await createSignInUrl(movie.avatar_img, "movie");
        }
        if (movie.cover_img) {
          movie.cover_img = await createSignInUrl(movie.cover_img, "movie");
        }
      }

      setMovies(movies);
    };
    fetchMovies();
  }, [movies]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        let data = await getGenres();
        if (data) {
          setGenres(data);
        }
      } catch (error) {
        console.error("Error fetching types:", error);
      }
    };

    fetchData();
  }, []);
  useEffect(() => {
    const fetchData = async () => {
      try {
        let data: Category[] = await getCategories();
        if (data) {
          setCategories(data);
        }
      } catch (error) {
        console.error("Error fetching types:", error);
      }
    };

    fetchData();
  }, []);
  useEffect(() => {
    const timeout = setTimeout(() => {
      setLoading(false);
    }, 5000);
    return () => clearTimeout(timeout);
  }, []);
  const formik = useFormik<FilterModel>({
    initialValues: {
      search: "",
      ratingFilter: "",
      genreFilter: "",
    },
    onSubmit: async (values, { resetForm }) => {},
  });

  const filteredMovies = movies.filter((movie) => {
    return (
      (formik.values.search.length === 0 ||
        movie.title
          ?.toLowerCase()
          .includes(formik.values.search.toLowerCase())) &&
      (formik.values.ratingFilter.length === 0 ||
        formik.values.ratingFilter.toLowerCase() ===
          movie.category_name?.toLowerCase()) &&
      (formik.values.genreFilter.length === 0 ||
        formik.values.genreFilter.toLowerCase() === movie.genre?.toLowerCase())
    );
  });

  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
        maxHeight: "30%",
        minWidth: "50%",
        overflow: "auto",
        marginX: "10%",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          top: "5px",
          zIndex: 2,
        }}
      >
        <form onSubmit={formik.handleSubmit}>
          <Box
            sx={{
              marginLeft: "55px",
              position: "static",
              zIndex: 1,
            }}
          >
            <TextField
              value={formik.values.search}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              name="search"
              variant="standard"
              color="primary"
              placeholder="Search"
              sx={{
                bgcolor: "white",
                width: isMobile ? "100px" : "274px",
                height: "39px",
                padding: "7px",
                gap: "0px",
                marginRight: "10px",
                borderRadius: "15px",
                border: "1px",
                opacity: "0px",
              }}
              InputProps={{
                disableUnderline: true,

                startAdornment: (
                  <InputAdornment position="start">
                    <Box
                      aria-label="search"
                      // edge="start"
                      sx={{
                        color: "black",
                        opacity: "0.5",
                        marginLeft: "5px",
                      }}
                    >
                      <SearchIcon />
                    </Box>
                  </InputAdornment>
                ),
              }}
            />
            <IconButton
              aria-label="Filter"
              onClick={() => setShowFilter(!showFilter)}
              sx={{
                backgroundColor: "white",
                top: 5,
                right: 0,
                color: (theme) => theme.palette.grey[500],
              }}
            >
              <img alt={"close icon"} src={"filter.png"} />
            </IconButton>
          </Box>
          {showFilter ? (
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                backgroundColor: "aliceblue",
                position: "absolute",
                padding: "5px",
                alignContent: "center",
                alignItems: "center",
                top: "60px",
                borderRadius: "15px",
                width: "100%",
                height: "80px",
                zIndex: 10,
              }}
            >
              <FormControl>
                <Typography
                  sx={{
                    position: "absolute",
                    color: "black",
                    left: "50px",
                    zIndex: 2,
                  }}
                >
                  Rating
                </Typography>

                <TextField
                  sx={{
                    width: "150px",
                    height: "auto",
                    marginTop: "20px",
                    borderRadius: "15px",
                    paddingLeft: "10px",
                    paddingRight: "10px",
                  }}
                  id="id_ratingFilter"
                  select
                  name="ratingFilter"
                  variant="outlined"
                  color="primary"
                  InputProps={{
                    disableUnderline: true,
                  }}
                  SelectProps={{
                    IconComponent: ExpandMoreIcon,
                  }}
                  value={formik.values.ratingFilter}
                  onChange={formik.handleChange}
                >
                  {categories.map((category) => (
                    <MenuItem key={category.id} value={category.name}>
                      {category.name}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <FormControl>
                <Typography
                  sx={{
                    position: "absolute",
                    color: "black",
                    textAlign: "center",
                    left: "50px",
                    zIndex: 2,
                  }}
                >
                  Genre
                </Typography>
                <TextField
                  sx={{
                    width: "150px",
                    height: "auto",
                    marginTop: "20px",
                    borderRadius: "15px",
                    paddingLeft: "10px",
                    paddingRight: "10px",
                  }}
                  id="id_genre"
                  select
                  name="genreFilter"
                  SelectProps={{
                    IconComponent: ExpandMoreIcon,
                  }}
                  InputProps={{
                    disableUnderline: true,
                  }}
                  variant="outlined"
                  color="primary"
                  value={formik.values.genreFilter}
                  onChange={formik.handleChange}
                >
                  {genres.map((option) => (
                    <MenuItem key={option.id} value={option.name}>
                      {option.name}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
            </Box>
          ) : null}
        </form>
      </Box>

      {!loading && movies.length === 0 ? (
        <Typography
          variant="h6"
          align={"center"}
          component="div"
          sx={{ color: "black" }}
        >
          No Movies Found
        </Typography>
      ) : null}
      {loading ? (
        <div>
          <Backdrop
            sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={loading}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
        </div>
      ) : (
        <List style={{ maxHeight: "100%", overflow: "auto" }}>
          {filteredMovies.map((movie) => (
            <ListItem
              key={movie.id}
              sx={{
                marginTop: "10px",
                borderRadius: "15px",
              }}
            >
              <MovieItem {...movie} key={movie.id} />
            </ListItem>
          ))}
        </List>
      )}
    </Container>
  );
}
