import * as yup from "yup";

const MovieValidationsForm =yup.object().shape( {
    title: yup.string().required("Title is Required"),
    genre: yup.string().required("Genre is Required"),
    cover_file: yup.mixed().required("Cover Image is required"),
    avatar_file: yup.mixed().required("Avator Image is required"),
    category: yup.number().required('Rating is required'),
    release_date: yup.date().required("Release Date is required"),
    applicant: yup.string().required("Applicant is required"),
    permit_number: yup.string().required("Permit Number is required"),
    premier_date:yup.date().required("Premier Date is required"),


});

export default MovieValidationsForm;
