/* eslint-disable @typescript-eslint/no-unused-expressions */
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { LoadingButton } from "@mui/lab";
import {
  Avatar,
  Box,
  Button,
  Chip,
  FormControl,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  Paper,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import OutlinedInput from "@mui/material/OutlinedInput";
import { DatePicker } from "@mui/x-date-pickers";
import dayjs, { Dayjs } from "dayjs";
import { useFormik } from "formik";
import React, { ChangeEvent, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router";
import { v4 as uuidv4 } from "uuid";
import { DefaultPage } from "../../layouts/default-page";
import { Notify } from "../../layouts/notify";
import { BoardMember } from "../../models/BoardMember";
import { Movie } from "../../models/Movie";
import { getBoardMembers } from "../../services/ChannelService";
import { upload } from "../../services/Upload";
import {
  useAddMoveMutation,
  useGetCategoriesQuery,
  useGetGenresQuery,
  useUpdateMovieMutation,
} from "../../services/movies.query";
import MovieValidationsForm from "./Validation";

export default function AddNewMovie() {
  const navigate = useNavigate();

  const [addMovie, { isLoading: addMovieLoading }] = useAddMoveMutation();
  const [updateMovie, { isLoading: updateLoading }] = useUpdateMovieMutation();

  const location = useLocation();
  const movieData = location.state?.movie;

  const formik = useFormik({
    initialValues: {
      title: movieData ? movieData.title : "",
      category: movieData ? movieData.category : "",
      genre: movieData ? movieData.genre : "",
      applicant: movieData ? movieData.applicant : "",
      permit_number: movieData ? movieData.permit_number : "",
      reason: movieData ? movieData.reason : "",
      release_date: movieData ? movieData.release_date : null,
      premier_date: movieData ? movieData.premier_date : null,
      cover_file: movieData ? movieData.cover_img : null,
      avatar_file: movieData ? movieData.avatar_img : null,
      committees: movieData ? movieData.committees : [],
    },
    validationSchema: MovieValidationsForm,
    onSubmit: async (values, { resetForm }) => {
      console.log("values", values);
      let formData: Movie = {
        title: values.title,
        category: Number(values.category),
        genre: values.genre,
        applicant: values.applicant,
        permit_number: values.permit_number,
        reason: values.reason,
        release_date: values.release_date,
        premiere_date: values.premier_date,
        cover_img: "",
        avatar_img: "",
        rate: 0,
        committees: values.committees.map((id: string) => parseInt(id, 10)),
      };

      if (movieData) {
        formData.id = movieData.id;
      }

      if (avatarImage && typeof avatarImage !== "string") {
        const file = avatarImage;
        const fileExt = file?.name?.split(".").pop();
        const filePath = `${uuidv4()}-${Math.random()}.${fileExt}`;
        let data = await upload(file, "images", filePath);
        formData.avatar_img = data.url;
      } else if (typeof avatarImage === "string") {
        formData.avatar_img = avatarImage;
      } else if (movieData?.avatar_img) {
        formData.avatar_img = movieData.avatar_img;
      }

      if (coverImage && typeof coverImage !== "string") {
        const file = coverImage;
        const fileExt = file.name.split(".").pop();
        const filePath = `${uuidv4()}-${Math.random()}.${fileExt}`;
        let data = await upload(file, "images", filePath);
        formData.cover_img = data.url;
      } else if (typeof coverImage === "string") {
        formData.cover_img = coverImage;
      } else if (movieData?.cover_img) {
        formData.cover_img = movieData.cover_img;
      }

      try {
        if (!movieData) {
          await addMovie(formData);
          Notify("success", "Movie created successfully");
        } else {
          await updateMovie(formData);
          Notify("info", "Movie updated successfully");
        }
        setOpen(false);
        resetForm();
        navigate("/movies/movies");
      } catch (error) {
        console.error("Error during form submission:", error);
        Notify("error", "Error occurred while saving movie");
      }
    },
  });

  const [avatarImage, setAvatarImage] = useState<File | null>(
    movieData?.avatar_img ?? null
  );
  const [coverImage, setCoverImage] = useState<File | null>(
    movieData?.cover_img ?? null
  );
  const [release_date, setReleaseDate] = useState<Dayjs | null | undefined>(
    null
  );
  const [premiere_date, setPremiereDate] = useState<Dayjs | null | undefined>(
    null
  );
  const [open, setOpen] = useState<boolean>(false);
  const [isFocused, setIsFocused] = useState(false);
  const [genreIsFocused, setGenreIsFocused] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isPremiereDateOpen, setPremiereIsOpen] = useState(false);
  const [isCommitteeSelected, setIsCommitteeSelected] = useState(false);
  const [users, setBoardMembers] = useState<BoardMember[]>([]);

  const { data: categories, isLoading: categoriesLoading } =
    useGetCategoriesQuery("");
  const { data: genres, isLoading: genresLoading } = useGetGenresQuery("");

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        let data = await getBoardMembers();
        if (data) {
          setBoardMembers(data);
        }
      } catch (error) {
        console.error("Error fetching types:", error);
      }
    };

    fetchUserData();
  }, []);

  useEffect(() => {
    setReleaseDate(dayjs(movieData?.release_date));
    formik.setFieldValue("release_date", dayjs(movieData?.release_date));
    setPremiereDate(dayjs(movieData?.premier_date));
    formik.setFieldValue("premier_date", dayjs(movieData?.premier_date));
    formik.setFieldValue("avatar_file", movieData?.avatar_img);
    formik.setFieldValue("cover_file", movieData?.cover_img);
  }, [movieData]);

  const handleAvatarImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.currentTarget.files) {
      setAvatarImage(e.target.files[0]);
      formik.setFieldValue("avatar_file", e.currentTarget.files[0]);
      formik.setErrors({ avatar_file: "" });
    }
  };

  const handleCoverImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.currentTarget.files) {
      setCoverImage(e.currentTarget.files[0]);
      formik.setFieldValue("cover_file", e.currentTarget.files[0]);
      formik.setErrors({ cover_file: "" });
    }
  };

  const handleReleaseDateChange = (newValue: any) => {
    setReleaseDate(newValue);
    formik.setFieldValue("release_date", newValue);
    formik.setErrors({ release_date: "" });
  };

  const handlePremierDateChange = (newValue: any) => {
    setPremiereDate(newValue);
    formik.setFieldValue("premier_date", newValue);
    formik.setErrors({ premier_date: "" });
  };

  return (
    <DefaultPage
      title={movieData ? "Edit Movie" : "Add New Movie"}
      loading={categoriesLoading || genresLoading}
    >
      <Paper elevation={0}>
        <form autoComplete="off" onSubmit={formik.handleSubmit}>
          <Grid container spacing={6}>
            <Grid item xs={12} sm={4} md={4}>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  bgcolor: "#DEECFF",
                  width: "100%",
                  height: 300,
                  borderRadius: 5,
                }}
              >
                <Typography variant="subtitle1">Avatar Image</Typography>

                <input
                  accept="image/*"
                  style={{ display: "none" }}
                  id="avatar_contained-file"
                  type="file"
                  multiple
                  name="avatar_file"
                  onChange={handleAvatarImageChange}
                  onBlur={formik.handleBlur}
                />
                <label htmlFor="avatar_contained-file">
                  <Button
                    sx={{
                      width: "100%",
                      height: 200,
                      ":focus": {
                        border: "none",
                        outline: "none",
                        bgcolor: "transparent",
                      },
                      ":hover": {
                        border: "none",
                        outline: "none",
                        bgcolor: "transparent",
                      },
                    }}
                    variant="text"
                    component="span"
                    startIcon={<img alt="upload icon" src="/upload.png" />}
                  >
                    {avatarImage && typeof avatarImage === "string" ? (
                      <img
                        src={avatarImage}
                        style={{ width: 100, height: 100 }}
                        alt="avatar"
                      />
                    ) : (
                      avatarImage && (
                        <img
                          src={URL.createObjectURL(avatarImage)}
                          style={{ width: 100, height: 100 }}
                          alt="avatar"
                        />
                      )
                    )}
                  </Button>
                </label>
                {avatarImage && typeof avatarImage !== "string" && (
                  <span>
                    {avatarImage.name.length > 15
                      ? `${avatarImage.name.slice(0, 15)}...`
                      : avatarImage.name}
                  </span>
                )}
                {formik.errors.avatar_file && !movieData && (
                  <span style={{ color: "red", fontSize: "16px" }}>
                    {formik.errors.avatar_file as any}
                  </span>
                )}
              </Box>
            </Grid>

            <Grid
              item
              xs={12}
              sm={8}
              md={8}
              sx={{ marginTop: 2 }}
              direction="column"
              container
            >
              <FormControl>
                <Typography>Title</Typography>
                <TextField
                  sx={{
                    margin: 1,
                    borderRadius: 5,
                    padding: 0,
                  }}
                  InputProps={{
                    disableUnderline: true,
                  }}
                  name="title"
                  id="id_title"
                  variant="outlined"
                  value={formik.values.title}
                  onChange={formik.handleChange}
                  error={formik.touched.title && Boolean(formik.errors.title)}
                  helperText={
                    formik.touched.title && (formik.errors.title as any)
                  }
                  onBlur={formik.handleBlur}
                  type="text"
                  placeholder="e.g The Avengers"
                />
              </FormControl>
              <FormControl>
                <Typography
                  sx={{
                    backgroundColor: "white",
                    borderRadius: 5,
                    margin: 1,
                  }}
                >
                  Rating
                </Typography>
                <TextField
                  sx={{
                    padding: 0,
                    borderRadius: 5,
                    margin: 1,
                  }}
                  id="id_category"
                  select
                  name="category"
                  label={isFocused ? "" : "Rating"}
                  InputProps={{
                    disableUnderline: true,
                  }}
                  variant="outlined"
                  SelectProps={{
                    IconComponent: ExpandMoreIcon,
                  }}
                  value={formik.values.category}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.category && Boolean(formik.errors.category)
                  }
                  helperText={
                    formik.touched.category && (formik.errors.category as any)
                  }
                  onBlur={formik.handleBlur}
                  onFocus={() => setIsFocused(true)}
                >
                  {categories?.map((category: any) => (
                    <MenuItem key={category.id} value={category.id}>
                      {category.name}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <FormControl>
                <Typography
                  style={{
                    borderRadius: 5,
                    margin: 1,
                  }}
                >
                  Genre
                </Typography>
                <TextField
                  sx={{
                    padding: 0,
                    margin: 1,
                  }}
                  id="id_genre"
                  select
                  name="genre"
                  label={genreIsFocused ? "" : "Genre"}
                  InputProps={{
                    disableUnderline: true,
                  }}
                  SelectProps={{
                    IconComponent: ExpandMoreIcon,
                  }}
                  variant="outlined"
                  value={formik.values.genre}
                  onChange={formik.handleChange}
                  error={formik.touched.genre && Boolean(formik.errors.genre)}
                  helperText={
                    formik.touched.genre && (formik.errors.genre as any)
                  }
                  onBlur={formik.handleBlur}
                  onFocus={() => setGenreIsFocused(true)}
                >
                  {genres?.map((option: any) => (
                    <MenuItem key={option.id} value={option.name}>
                      {option.name}
                    </MenuItem>
                  ))}
                </TextField>
              </FormControl>
              <br />
            </Grid>
            <Grid item xs={12} sm={4} md={4} direction="column" container>
              <Box
                className="flex items-center"
                sx={{
                  bgcolor: "#DEECFF",
                  height: 300,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  borderRadius: 5,
                  justifyContent: "center",
                }}
              >
                <Typography> Cover Image</Typography>

                <input
                  accept="image/*"
                  style={{ display: "none" }}
                  id="cover_img"
                  type="file"
                  multiple
                  name="cover_file"
                  onChange={handleCoverImageChange}
                  onBlur={formik.handleBlur}
                />
                <label htmlFor="cover_img">
                  <Button
                    sx={{
                      width: "100%",
                      height: 200,
                      ":focus": {
                        border: "none",
                        outline: "none",
                        bgcolor: "transparent",
                      },
                      ":hover": {
                        border: "none",
                        outline: "none",
                        bgcolor: "transparent",
                      },
                    }}
                    variant="text"
                    component="span"
                    startIcon={<img alt="upload icon" src="/upload.png" />}
                  >
                    {coverImage && typeof coverImage === "string" ? (
                      <img
                        src={coverImage}
                        style={{ width: 100, height: 100 }}
                        alt="avatar"
                      />
                    ) : (
                      coverImage && (
                        <img
                          src={URL.createObjectURL(coverImage)}
                          style={{ width: 100, height: 100 }}
                          alt="avatar"
                        />
                      )
                    )}
                  </Button>
                </label>
                {coverImage && typeof coverImage !== "string" && (
                  <span>
                    {coverImage.name.length > 15
                      ? `${coverImage.name.slice(0, 15)}...`
                      : coverImage.name}
                  </span>
                )}
                {formik.errors.cover_file && !movieData && (
                  <span style={{ color: "red" }}>
                    {formik.errors.cover_file as any}
                  </span>
                )}
              </Box>
            </Grid>
            <Grid item xs={12} sm={8} md={8} direction="column" container>
              <FormControl fullWidth>
                <Typography
                  sx={{
                    borderRadius: 5,
                  }}
                >
                  Release Date
                </Typography>
                <DatePicker
                  slotProps={{
                    textField: {
                      variant: "outlined",
                      helperText: Boolean(formik.errors.release_date as any),
                      InputProps: { disableUnderline: true },
                    },
                  }}
                  label={isOpen ? "" : "Date"}
                  onOpen={() => setIsOpen(true)}
                  disablePast
                  name="release_date"
                  value={release_date}
                  onChange={handleReleaseDateChange}
                  format="DD.MM.YYYY"
                />
              </FormControl>
              <FormControl fullWidth>
                <Typography
                  sx={{
                    borderRadius: 5,
                  }}
                >
                  Premiere Date
                </Typography>
                <DatePicker
                  slotProps={{
                    textField: {
                      variant: "outlined",
                      helperText: formik.errors.premier_date as any,
                      InputProps: { disableUnderline: true },
                    },
                  }}
                  label={isPremiereDateOpen ? "" : "Date"}
                  disablePast
                  name="premier_date"
                  value={premiere_date}
                  onChange={handlePremierDateChange}
                  onOpen={() => setPremiereIsOpen(true)}
                  format="DD.MM.YYYY"
                />
              </FormControl>
              <FormControl fullWidth>
                <Typography
                  sx={{
                    borderRadius: 5,
                  }}
                >
                  Applicant Name
                </Typography>
                <TextField
                  InputProps={{
                    disableUnderline: true,
                  }}
                  id="id_applicant"
                  name="applicant"
                  variant="outlined"
                  type="text"
                  placeholder="e.g 20th Century Studios"
                  value={formik.values.applicant}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.applicant && Boolean(formik.errors.applicant)
                  }
                  helperText={
                    formik.touched.applicant && (formik.errors.applicant as any)
                  }
                  onBlur={formik.handleBlur}
                />
              </FormControl>
              <FormControl fullWidth>
                <Typography
                  sx={{
                    borderRadius: 5,
                  }}
                >
                  Permit Number
                </Typography>
                <TextField
                  sx={{ borderRadius: 5 }}
                  InputProps={{
                    disableUnderline: true,
                  }}
                  id="id_permit_number"
                  name="permit_number"
                  variant="outlined"
                  type="text"
                  placeholder="e.g WB123"
                  value={formik.values.permit_number}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.permit_number &&
                    Boolean(formik.errors.permit_number)
                  }
                  helperText={
                    formik.touched.permit_number &&
                    (formik.errors.permit_number as any)
                  }
                  onBlur={formik.handleBlur}
                />
              </FormControl>
              <FormControl fullWidth>
                <Typography
                  sx={{
                    borderRadius: 5,
                  }}
                >
                  Reason for Classification
                </Typography>
                <TextField
                  InputProps={{
                    disableUnderline: true,
                  }}
                  id="id_reason"
                  name="reason"
                  value={formik.values.reason}
                  onChange={formik.handleChange}
                  variant="outlined"
                  type="text"
                  placeholder="e.g reason ..."
                />
              </FormControl>
              <Typography
                sx={{
                  color: "#AFAFAF",
                  fontSize: 20,
                  fontWeight: "bold",
                  textAlign: "left",
                  borderRadius: 5,
                }}
              >
                Review Committee Members
              </Typography>
              <FormControl>
                <Typography sx={{ marginTop: 1 }}>Committee Members</Typography>
                <InputLabel
                  id="multiple_committee"
                  style={{
                    color: "#AFAFAF",
                    backgroundColor: "white",
                    marginTop: 10,
                    borderRadius: 6,
                  }}
                >
                  {isCommitteeSelected ? "" : "Select Committee"}
                </InputLabel>
                <Select
                  sx={{
                    marginTop: isCommitteeSelected ? 1 : 0,
                  }}
                  id="multiple_committee"
                  labelId="multiple_committee"
                  IconComponent={ExpandMoreIcon}
                  multiple
                  name="committees"
                  variant="outlined"
                  value={formik.values.committees}
                  onChange={formik.handleChange}
                  input={<OutlinedInput />}
                  renderValue={(selected) => (
                    <div style={{ display: "flex", flexWrap: "wrap" }}>
                      {selected.map((value: React.Key | null | undefined) => (
                        <Chip
                          avatar={
                            <Avatar
                              alt={users.find((u) => u.id === value)?.name}
                              src={
                                users.find((u) => u.id === value)?.profile_url
                              }
                            />
                          }
                          key={value}
                          label={users.find((u) => u.id === value)?.name}
                          style={{ margin: 2 }}
                        />
                      ))}
                    </div>
                  )}
                  MenuProps={{
                    PaperProps: {
                      style: {
                        maxHeight: 48 * 4.5 + 8,
                        width: 250,
                      },
                    },
                  }}
                  onOpen={() => setIsCommitteeSelected(true)}
                >
                  {users.map((user) => (
                    <MenuItem key={user.id} value={user.id}>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          gap: 2,
                        }}
                      >
                        <Avatar alt={user.name} src={user.profile_url} />
                        <ListItemText primary={user.name} />
                      </div>
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6} md={6} mx="auto" justifyContent="center">
              <LoadingButton
                type="submit"
                variant="contained"
                color="primary"
                className="hover:bg-green-700 text-white px-4 py-2 rounded-md"
                sx={{
                  width: "50%",
                  height: 40,
                  bgcolor: "#0054D3",
                  borderRadius: "55px",
                  textTransform: "none",
                }}
                loading={
                  formik.isSubmitting || addMovieLoading || updateLoading
                }
              >
                {movieData ? "Update Movie" : "Add New Movie"}
              </LoadingButton>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </DefaultPage>
  );
}
