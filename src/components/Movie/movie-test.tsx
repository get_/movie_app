import { Box, Button } from "@mui/material";
import {
  useDeleteMovieMutation,
  useGetMoviesQuery,
} from "../../services/movies.query";
import { DataTable } from "../../layouts/data-table";
import { format } from "date-fns";
import ImageCell from "./img-cell";
import { DefaultPage } from "../../layouts/default-page";
import { Add, Delete, Edit } from "@mui/icons-material";
import { useNavigate } from "react-router";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import React, { useMemo, useState } from "react";
export const MovieTest = () => {
  const [open, setOpen] = React.useState<boolean>(false);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);
  const [globalFilter, setGlobalFilter] = useState("");

  const [deleteMovie, { isLoading: deleteLoading }] = useDeleteMovieMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteMovie(itemTobeDeleted.id);
    setOpen(false);
  };
  const editMovie = (rowData: any) => {
    navigate("/movies/add-movie", { state: { movie: rowData } });
  };

  const columns = [
    {
      accessorKey: "avatar_img",
      header: "Cover Image",
      size: 40,
      Cell: ({ row }: any) => {
        return (
          <>
            <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
              <img
                src={row.original.avatar_img}
                style={{ width: 50, height: 50, cursor: "pointer" }}
                onClick={() => window.open(row.original.avatar_img, "_blank")}
              />
            </Box>{" "}
          </>
        );
      },
    },

    {
      accessorKey: "title",
      header: "Title",
      size: 40,
      globalFilter,
    },
    {
      accessorKey: "release_date",
      header: "Release Date",
      size: 40,

      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
    {
      accessorKey: "genre",
      size: 40,
      header: "Genre",
    },
    {
      accessorKey: "rate",
      size: 40,
      header: "Rate",
    },
    {
      accessorKey: "premiere_date",
      size: 40,

      header: "Premiere Date",
      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
    {
      accessorKey: "applicant",
      header: "Applicant",
      size: 40,
    },
  ];
  const {
    data: movies,
    isLoading,
    isSuccess,
    isFetching,
    isError,
  } = useGetMoviesQuery("");
  const navigate = useNavigate();
  // Filtered data logic updated
  const filteredData = useMemo(() => {
    if (globalFilter) {
      return movies?.filter((movie: any) =>
        movie.title.toLowerCase().includes(globalFilter.toLowerCase())
      );
    }
    return movies;
  }, [globalFilter, movies]);
  return (
    <DefaultPage
      title={"Movies"}
      primaryButtonProps={{
        startIcon: <Add />,
        children: "Add New Movie",
        onClick: () => {
          navigate("/movies/add-movie");
        },
      }}
    >
      <DataTable
        count={movies?.length}
        columns={columns}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        toolbar={true}
        toolbarActions={true}
        isFetching={isFetching}
        enableBottomToolbar={true}
        enablePinning={true}
        globalFilter={setGlobalFilter} // handle global filter changes
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button color="warning" onClick={() => editMovie(row?.original)}>
              <Edit />
            </Button>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleteLoading}
        item={itemTobeDeleted?.title}
        itemName={itemTobeDeleted?.title}
      />
    </DefaultPage>
  );
};
