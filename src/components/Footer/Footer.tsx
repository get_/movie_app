export default function Footer() {
    return (
        <>
            <p style={{
                textAlign: 'center',
                color: 'black',
                backgroundColor: '#F6F7FD',
                // padding: '10px',
                position: 'absolute',
                left: '0',
                bottom: '0',
                width: '100%',
            }}>© {new Date().getFullYear()} MTRCB. All rights reserved.</p>
        </>
    )
}
