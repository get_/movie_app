import * as React from 'react';
import Button from '@mui/material/Button';
import Snackbar, {SnackbarOrigin} from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import {State} from "../../models/State";
import {Message} from "../../models/Message";

export default function CustomizedSnackbars(mess:Message) {
    const [state, setState] = React.useState<State>({
        open: false,
        vertical: 'bottom',
        horizontal: 'right',
    });
    const handleClose = () => {
        setState({ ...state, open: false });
    };

    const { vertical, horizontal} = state;

    return (
        <div>
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={mess.open}
                onClose={handleClose}
                message={mess.message}
                key={vertical + horizontal}
                autoHideDuration={1000}
            >
                <Alert
                    onClose={handleClose}
                    severity="success"
                    variant="filled"
                    sx={{ width: '100%' }}
                >
                    {mess.message}
                </Alert>
            </Snackbar>
        </div>
    );
}
