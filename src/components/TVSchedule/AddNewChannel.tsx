import React, { ChangeEvent, useState } from "react";
import {
  Box,
  Button,
  FormControl,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import { useFormik } from "formik";
import LoadingButton from "@mui/lab/LoadingButton";
import CustomizedSnackbars from "../common/SnackBar";
import { v4 as uuidv4 } from "uuid";
import { upload } from "../../services/Upload";
import { Notify } from "../../layouts/notify";
import { useNavigate } from "react-router";
import { DefaultPage } from "../../layouts/default-page";
import { useAddChannelMutation } from "../../services/channel.query";
import { ChannelValidationsForm } from "./Validation";

export default function AddNewChannel() {
  const navigate = useNavigate();
  const [addChannel, { isLoading }] = useAddChannelMutation();
  const [open, setOpen] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");
  const [avatarImage, setAvatarImage] = useState<File | null>(null);

  const formik = useFormik({
    initialValues: {
      title: "",
      avatar_file: null,
    },
    validationSchema: ChannelValidationsForm,
    onSubmit: async (values, { resetForm }) => {
      let formData = {
        title: values.title,
        avatar_img: "",
      };

      try {
        if (values.avatar_file) {
          const file = avatarImage;
          const fileExt = file?.name.split(".").pop();
          const filePath = `${uuidv4()}-${Math.random()}.${fileExt}`;
          let data = await upload(file!, "images", filePath);
          formData.avatar_img = data.url;
        }

        console.log("formData: ", formData);
        await addChannel(formData);
        setOpen(true);
        setAvatarImage(null);
        setMessage("TV Channel created successfully.");
        Notify("info", "TV Channel created successfully.");
        navigate("/tv/channels");
        resetForm();
      } catch (error) {
        console.error("Error:", error);
        Notify("error", "Failed to create TV Channel.");
      }
    },
  });

  const handleAvatarImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.currentTarget.files) {
      setAvatarImage(e.target.files[0]);
      formik.setFieldValue("avatar_file", e.currentTarget.files[0]);
      formik.setErrors({ avatar_file: "" });
    }
  };

  return (
    <DefaultPage title="Add Channel">
      <form onSubmit={formik.handleSubmit}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <Box
              sx={{
                bgcolor: "#DEECFF",
                height: "auto",
                display: "flex",
                justifyContent: "left",
                alignItems: "center",
                marginTop: 5,
                borderRadius: 5,
              }}
            >
              <input
                accept="image/*"
                style={{ display: "none" }}
                id="contained-file"
                type="file"
                multiple
                name="avatar_file"
                onChange={handleAvatarImageChange}
                onBlur={formik.handleBlur}
              />
              <label htmlFor="contained-file">
                <Button
                  variant="text"
                  component="span"
                  sx={{
                    width: 400,
                    height: 150,
                    ":focus": {
                      border: "none",
                      outline: "none",
                      bgcolor: "transparent",
                    },
                    ":hover": {
                      border: "none",
                      outline: "none",
                      bgcolor: "transparent",
                    },
                  }}
                >
                  <CloudUploadIcon />
                </Button>
              </label>
            </Box>
            {avatarImage && <span>{avatarImage.name}</span>}
            {formik.errors.avatar_file && (
              <span style={{ color: "red" }}>{formik.errors.avatar_file}</span>
            )}
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl fullWidth sx={{ marginTop: 5 }}>
              <Typography style={{ color: "#AFAFAF" }}>Title</Typography>
              <TextField
                fullWidth
                InputProps={{ disableUnderline: true }}
                name="title"
                value={formik.values.title}
                onChange={formik.handleChange}
                error={formik.touched.title && Boolean(formik.errors.title)}
                helperText={formik.touched.title && formik.errors.title}
                onBlur={formik.handleBlur}
                variant="outlined"
                type="text"
              />
            </FormControl>
          </Grid>
        </Grid>
        <Box sx={{ mt: 2 }}>
          <LoadingButton
            loading={isLoading}
            type="submit"
            variant="contained"
            color="primary"
            disabled={isLoading}
            sx={{
              width: "70%",
              height: "auto",
              my: 2,
              borderRadius: 5,
            }}
          >
            Add New Channel
          </LoadingButton>
        </Box>
      </form>
      <CustomizedSnackbars message={message} open={open} />
    </DefaultPage>
  );
}
