import React, { useEffect, useState } from "react";
import { Avatar, Box } from "@mui/material";
import { createSignInUrl } from "../../services/Upload";

const ChannelImageCell = ({ row }: any) => {
  const [imageUrl, setImageUrl] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    const fetchImageUrl = async () => {
      try {
        const url: any = await createSignInUrl(
          row?.original?.avatar_img,
          "channel"
        );
        setImageUrl(url);
        setError(false);
      } catch (err) {
        console.error("Error fetching image URL:", err);
        setError(true);
      } finally {
        setLoading(false);
      }
    };

    fetchImageUrl();
  }, [row?.original?.avatar_img]);

  if (error) return <p>Error loading image</p>;
  if (loading) return <p>Loading...</p>;

  return (
    <Box
      display="flex"
      alignItems="center"
      sx={{ textTransform: "none" }}
      onClick={() => console.log(row.original)}
    >
      <Avatar
        src={imageUrl as any}
        alt={row?.original?.title}
        sx={{ width: 56, height: 56 }}
      />
    </Box>
  );
};

export default ChannelImageCell;
