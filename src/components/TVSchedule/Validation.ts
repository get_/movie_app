import * as yup from "yup";

export const ChannelValidationsForm =yup.object().shape( {
    title: yup.string().required("Title is Required"),
    avatar_file: yup.mixed().required("Avator Image is required"),


});


export const ProgramValidationsForm =yup.object().shape( {
    program_name: yup.string().required("Title is Required"),
    category_id: yup.mixed().required("Category is required"),
    start_time: yup.string().required("Start Time is required"),
    end_time: yup.string().required("End Time is required"),


});
