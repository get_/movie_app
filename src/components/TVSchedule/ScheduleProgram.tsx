import Box from "@mui/material/Box";
import {
  Button,
  FormControl,
  Grid,
  IconButton,
  MenuItem,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { Program, ProgramDate } from "../../models/Program";
import { getNext7DaysWithDayNames } from "../../utils/API";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { renderTimeViewClock, TimePicker } from "@mui/x-date-pickers";
import { useLocation } from "react-router";
import { useFormik } from "formik";
import { ProgramValidationsForm } from "./Validation";
import { LoadingButton } from "@mui/lab";
import { Notify } from "../../layouts/notify";
import {
  useAddProgramMutation,
  useDeleteProgramMutation,
  useLazyGetProgramsQuery,
} from "../../services/channel.query";
import { DefaultPage } from "../../layouts/default-page";
import { Add, Delete } from "@mui/icons-material";
import { format } from "date-fns";
import { DataTable } from "../../layouts/data-table";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { useGetCategoriesQuery } from "../../services/movies.query";

export default function ScheduleProgram() {
  let { state } = useLocation();
  console.log("state", state);
  const [programs, setProgram] = React.useState<Program[]>([]);
  const [selectedItem, setSelectedItem] = React.useState<any>("");
  const [dates, setDates] = useState<ProgramDate[]>([]);

  const [active, setActive] = React.useState<boolean>(false);
  const [deleteTVProgram, { isLoading: deleting }] = useDeleteProgramMutation();
  const [addProgram, { isLoading: adding }] = useAddProgramMutation();
  const [openDeleteModal, setOpenDeleteModal] = React.useState<boolean>(false);
  console.log("programs,programs", programs);
  console.log("dates", dates[0]?.date);
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);
  const handleOPenDelete = (row: any) => {
    setOpenDeleteModal(true);
    setItemTobeDelete(row);
  };
  const deleteItem = async () => {
    await deleteTVProgram(itemTobeDeleted.id);
    setOpenDeleteModal(false);
  };
  const [
    triggerProgram,
    {
      data: fetchedPrograms,
      isLoading: programsLoading,
      isFetching: programsFetching,
      isSuccess: programsSUccess,
      isError: programsError,
    },
  ] = useLazyGetProgramsQuery();

  useEffect(() => {
    if (state.channel?.id && dates) {
      triggerProgram({
        scheduleId: state?.channel?.id,
        programDate: selectedItem ?? dates[0]?.date,
      });
    }
  }, [dates, selectedItem, state.channel?.id]);
  const [open, setOpen] = React.useState<boolean>(false);

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    const fetchProgramDate = () => {
      const _programsDates: ProgramDate[] = getNext7DaysWithDayNames();
      setDates(_programsDates);
      setSelectedItem(_programsDates[0]?.date); // Set default selected date on initial load
    };
    fetchProgramDate();
  }, []);
  const { data: categories } = useGetCategoriesQuery("");

  const formik = useFormik({
    initialValues: {
      program_name: "",
      category_id: null,
      start_time: null,
      end_time: null,
      program_date: selectedItem as unknown as Date,
      tv_schedule_id: state?.channel?.id as number,
    },
    validationSchema: ProgramValidationsForm,
    onSubmit: async (values, { resetForm }) => {
      let data: Program = {
        program_name: values.program_name,
        category_id: values.category_id as unknown as number,
        start_time: values.start_time as unknown as string,
        end_time: values.end_time as unknown as string,
        program_date: selectedItem as unknown as Date,
        tv_schedule_id: values.tv_schedule_id,
      };
      if (
        data.tv_schedule_id === undefined ||
        data.tv_schedule_id === null ||
        data.tv_schedule_id === 0 ||
        data.program_date === null ||
        data.program_date === undefined
      ) {
        return;
      }

      await addProgram(data);
      Notify("info", "programme added Successfully");
      resetForm();
      setOpen(false);
    },
  });
  const columns = [
    {
      accessorKey: "program_name",
      header: "Program Name",
    },

    {
      accessorKey: "rating",
      header: "Rating",
      Cell: ({ row }: any) => {
        const rating = categories?.find(
          (id: { id: any }) => id?.id === row?.original?.category_id
        )?.name;
        return `${rating}`;
      },
    },
    {
      accessorKey: "season_episode",
      header: "Season Episode",
    },

    {
      accessorKey: "program_date",
      header: "Program Date",

      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
    {
      accessorKey: "start_time",
      header: "Time",
      Cell: ({ row }: any) => {
        // Assuming start_time and end_time are in 'HH:mm:ss' format
        const startTime = new Date(`1970-01-01T${row.original.start_time}`);
        const endTime = new Date(`1970-01-01T${row.original.end_time}`);

        const formattedStartTime = startTime.toLocaleTimeString("en-US", {
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit",
          hour12: true,
        });

        const formattedEndTime = endTime.toLocaleTimeString("en-US", {
          hour: "2-digit",
          minute: "2-digit",
          second: "2-digit",
          hour12: true,
        });

        return `${formattedStartTime} - ${formattedEndTime}`;
      },
    },
  ];
  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 1,
          padding: 1,
        }}
      >
        <Paper
          elevation={0}
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            width: "100%",
            height: "10%",
            padding: "15px 26px 16px 25px",
            gap: 4,
            borderRadius: "15px",
            opacity: "0px",
            background: "linear-gradient(180deg, #2891FF 0%, #63AFFF 100%)",
            alignContent: "center",
            alignItems: "center",
          }}
        >
          {dates.map((date, index) => (
            <Box
              key={index}
              sx={{
                padding: "10px 15px",
                cursor: "pointer",
                backgroundColor:
                  selectedItem === date.date ? "#0054D3" : "white",
                "&:hover": { backgroundColor: "#2891FF" },
                borderRadius: "20px",
              }}
              onClick={() => setSelectedItem(date.date)}
            >
              <Typography
                sx={{
                  color: selectedItem === date.date ? "white" : "black",
                  fontSize: 25,
                  fontWeight: 600,
                }}
              >
                {date.date?.split("-")[2]}
              </Typography>
              <Typography
                sx={{
                  color: selectedItem === date.date ? "#DADADA" : "#535353",
                  fontSize: 14,
                }}
              >
                {date.dayName}
              </Typography>
            </Box>
          ))}
        </Paper>

        <DefaultPage
          title="Schedule TV Program"
          primaryButtonProps={{
            startIcon: <Add />,
            disabled: !selectedItem,
            children: "Add New Program for this Date",
            onClick: () => {
              setOpen(true);
            },
          }}
        >
          <DataTable
            count={fetchedPrograms?.length}
            columns={columns}
            globalFilter={""}
            list={fetchedPrograms as any}
            getRowId={(row: { id: any }) => row.id}
            isLoading={programsLoading}
            isSuccess={programsSUccess}
            isFetching={programsFetching}
            enableBottomToolbar={true}
            isError={programsError}
            renderRowActions={({ row }: any) => (
              <Box sx={{ display: "flex" }}>
                <Button
                  color="error"
                  onClick={() => handleOPenDelete(row?.original)}
                >
                  <Delete />
                </Button>
              </Box>
            )}
          />
        </DefaultPage>

        <div>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <Paper
              elevation={0}
              sx={{
                backgroundColor: "white",
                height: "450px",
                width: "500px",
                borderRadius: "15px",
                padding: "15px",
              }}
            >
              <IconButton
                aria-label="close"
                onClick={() => handleClose()}
                sx={{
                  position: "absolute",
                  top: 0,
                  right: 0,
                  color: (theme) => theme.palette.grey[500],
                }}
              >
                <img alt={"close icon"} src={"/x.png"} />
              </IconButton>
              <form noValidate onSubmit={formik.handleSubmit}>
                <DialogContent>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "15px",
                    }}
                  >
                    <FormControl>
                      <label
                        htmlFor="id_title"
                        style={{
                          color: "#AFAFAF",
                          marginBottom: "7px",
                        }}
                      >
                        Title
                      </label>
                      <TextField
                        sx={{
                          width: "100%",
                          margin: "0px",
                          padding: "0px",
                        }}
                        name="program_name"
                        id={"id_title"}
                        variant="outlined"
                        value={formik.values.program_name}
                        onChange={formik.handleChange}
                        type="text"
                        onBlur={formik.handleBlur}
                        error={
                          formik.touched.program_name &&
                          Boolean(formik.errors.program_name)
                        }
                        helperText={
                          formik.touched.program_name &&
                          formik.errors.program_name
                        }
                      />
                    </FormControl>
                    <FormControl>
                      <label
                        style={{
                          color: "#AFAFAF",
                          marginBottom: "7px",
                        }}
                      >
                        Rating
                      </label>

                      <TextField
                        sx={{
                          width: "100%",
                          height: "auto",
                        }}
                        InputLabelProps={{
                          style: {
                            // fontSize: '15px',
                          },
                        }}
                        id="id_category"
                        select
                        name="category_id"
                        variant="outlined"
                        SelectProps={{
                          IconComponent: ExpandMoreIcon,
                        }}
                        value={formik.values.category_id}
                        onChange={formik.handleChange}
                        error={
                          formik.touched.category_id &&
                          Boolean(formik.errors.category_id)
                        }
                        helperText={
                          formik.touched.category_id &&
                          formik.errors.category_id
                        }
                        onBlur={formik.handleBlur}
                      >
                        {categories?.map((category: any) => (
                          <MenuItem key={category.id} value={category.id}>
                            {category.name}
                          </MenuItem>
                        ))}
                      </TextField>
                    </FormControl>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignContent: "center",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <FormControl>
                        <label
                          style={{
                            color: "#AFAFAF",
                          }}
                        >
                          Start Time
                        </label>

                        <DemoContainer components={["TimePicker"]}>
                          <TimePicker
                            sx={{
                              width: "60%",
                              margin: "0px",
                            }}
                            label="Time Clock"
                            onChange={(newValue) =>
                              formik.setFieldValue(
                                "start_time",
                                newValue?.format("HH:mm:ss") as string
                              )
                            }
                            viewRenderers={{
                              hours: renderTimeViewClock,
                              minutes: renderTimeViewClock,
                              seconds: renderTimeViewClock,
                            }}
                          />
                        </DemoContainer>
                        {formik.touched.start_time &&
                        Boolean(formik.errors.start_time) ? (
                          <span style={{ color: "red" }}>
                            {formik.errors.start_time}
                          </span>
                        ) : null}
                      </FormControl>
                      <FormControl>
                        <label
                          style={{
                            color: "#AFAFAF",
                          }}
                        >
                          End Time
                        </label>

                        <DemoContainer components={["TimePicker"]}>
                          <TimePicker
                            sx={{
                              width: "60%",
                              margin: "0px",
                            }}
                            label="Time Clock"
                            onChange={(newValue) =>
                              formik.setFieldValue(
                                "end_time",
                                newValue?.format("HH:mm:ss") as string
                              )
                            }
                            viewRenderers={{
                              hours: renderTimeViewClock,
                              minutes: renderTimeViewClock,
                              seconds: renderTimeViewClock,
                            }}
                          />
                        </DemoContainer>
                        {formik.touched.end_time &&
                        Boolean(formik.errors.end_time) ? (
                          <span style={{ color: "red" }}>
                            {formik.errors.end_time}
                          </span>
                        ) : null}
                      </FormControl>
                    </div>
                  </div>
                </DialogContent>
                <DialogActions>
                  <Grid container spacing={2}>
                    <Grid item xs={1} sm={1} md={1} />

                    <Grid item xs={10} sm={10} md={10}>
                      <LoadingButton
                        color="primary"
                        type={"submit"}
                        loading={adding}
                        sx={{
                          textTransform: "none",
                          color: "white",
                          width: "100%",
                          borderRadius: "20px",
                          "&:hover": {
                            backgroundColor: "#5489de",
                            color: "white",
                          },
                          margin: 1,
                          backgroundColor: "#0054D3",
                        }}
                      >
                        Add New Program
                      </LoadingButton>
                    </Grid>
                    <Grid item xs={1} sm={1} md={1} />
                  </Grid>
                </DialogActions>
              </form>
            </Paper>
          </Dialog>
        </div>
        <DeleteConfirmationDialog
          open={openDeleteModal}
          onClose={() => setOpenDeleteModal(false)}
          onConfirm={deleteItem}
          loading={deleting}
          item={itemTobeDeleted?.id}
          itemName={itemTobeDeleted?.program_name}
        />
      </Box>
    </>
  );
}
