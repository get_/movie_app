import { Button } from "@mui/material";
import React, { useMemo, useState } from "react";

import { useFormik } from "formik";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";
import {
  useDeleteChannelMutation,
  useGetChannelsQuery,
} from "../../services/channel.query";
import { DefaultPage } from "../../layouts/default-page";
import { Add, Delete, Tv } from "@mui/icons-material";
import { DataTable } from "../../layouts/data-table";
import DeleteConfirmationDialog from "../../layouts/delete-confirm";
import { format } from "date-fns";
import ChannelImageCell from "./img-cell";
export default function ChannelList() {
  const {
    data: channels,
    isLoading,
    isError,
    isSuccess,
    isFetching,
  } = useGetChannelsQuery("");
  const [itemTobeDeleted, setItemTobeDelete] = React.useState<any>(null);
  const [globalFilter, setGlobalFilter] = useState("");

  const [open, setOpen] = React.useState<boolean>(false);

  const [deleteChannel, { isLoading: deleting }] = useDeleteChannelMutation();
  const handleClose = () => {
    setOpen(false);
  };
  const openDeleteModal = (row: any) => {
    setOpen(true);
    setItemTobeDelete(row);
  };
  const routeToTVProgram = (row: any) => {
    navigate("/tv/tv-program", { state: { channel: row } });
  };

  const deleteItem = async () => {
    await deleteChannel(itemTobeDeleted.id);
    setOpen(false);
  };
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: async (values, { resetForm }) => {},
  });
  const columns = [
    {
      accessorKey: "avatar_img",
      header: "Cover Image",
      Cell: ({ row }: any) => {
        return (
          <>
            <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
              <img
                src={row.original.avatar_img}
                style={{ width: 50, height: 50, cursor: "pointer" }}
                onClick={() => window.open(row.original.avatar_img, "_blank")}
              />
            </Box>{" "}
          </>
        );
      },
    },

    {
      accessorKey: "title",
      header: "Title",
    },

    {
      accessorKey: "created_at",
      header: "Registration Date",

      Cell: ({ cell }: any) => format(new Date(cell.getValue()), "MM/dd/yyyy"),
    },
  ];
  const navigate = useNavigate();
  const filteredData = useMemo(() => {
    if (globalFilter) {
      return channels?.filter((channel: any) =>
        channel.title.toLowerCase().includes(globalFilter.toLowerCase())
      );
    }
    return channels;
  }, [globalFilter, channels]);

  return (
    <DefaultPage
      title={"Channels"}
      primaryButtonProps={{
        startIcon: <Add />,
        children: "Add New Channel",
        onClick: () => {
          navigate("/tv/add-channel");
        },
      }}
    >
      <DataTable
        count={channels?.length}
        columns={columns}
        globalFilter={setGlobalFilter}
        list={filteredData as any}
        getRowId={(row: { id: any }) => row.id}
        isLoading={isLoading}
        isSuccess={isSuccess}
        isFetching={isFetching}
        toolbar={true}
        toolbarActions={true}
        enableBottomToolbar={true}
        enablePinning={true}
        isError={isError}
        toolbarActionMargin="2"
        renderRowActions={({ row }: any) => (
          <Box sx={{ display: "flex" }}>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => routeToTVProgram(row?.original)}
            >
              <Tv fontSize="small" sx={{ ml: 2 }} /> Tv Program
            </Button>
            <Button
              color="error"
              onClick={() => openDeleteModal(row?.original)}
            >
              <Delete />
            </Button>
          </Box>
        )}
      />
      <DeleteConfirmationDialog
        open={open}
        onClose={handleClose}
        onConfirm={deleteItem}
        loading={deleting}
        item={itemTobeDeleted?.title}
        itemName={itemTobeDeleted?.title}
      />
    </DefaultPage>
  );
}
